        INST(0x00)  TRAP40CC1                  //Nonexistant Instruction
        INST(0x01)  TRAP40CC1                  //Nonexistant Instruction
        INST(0x02)  NOIND                      //LCFI
                    if (inst and 0x200000) then S6CC=(inst >> 4) and 0xf;
                    if (inst and 0x100000) then begin
                        s6.fs=(inst>>2) and 1;
                        s6.fz=(inst>>1) and 1;
                        s6.fn=inst and 1;
                        endif
                    BREAK;
        INST(0x03)  TRAP40CC1                  //Nonexistant Instruction    
        INST(0x04)  calinst=inst; s6.curloc=0x48; goto trapint;  //CAL1
        INST(0x05)  calinst=inst; s6.curloc=0x49; goto trapint;  //CAL2
        INST(0x06)  calinst=inst; s6.curloc=0x4A; goto trapint;  //CAL3
        INST(0x07)  calinst=inst; s6.curloc=0x4B; goto trapint;  //CAL4
        INST(0x0C)  TRAP40CCA        //Nonexistant Instruction   &CC3 if slave
        INST(0x0D)  TRAP40CCA        //Nonexistant Instruction   &CC3 if slave
        INST(0x0E)  PRIV BRANCHTR LAA {psd *pd=(psd *)(&(*pIEAXd(2))); GPSD(s6psd,s6);   //LPSD
                    s6psd.w.w0=pd->w.w0;
                    s6psd.n.wk=pd->n.wk; s6psd.n.ci=pd->n.ci;
                    s6psd.n.ii=pd->n.ii; s6psd.n.ei=pd->n.ei;
                    s6psd.n.ma=pd->n.ma;
                    if (inst and 0x00800000) then s6psd.n.rp=pd->n.rp;
                    if (inst and 0x00200000) then begin
                        word g,lev;                        //find highest priority active
//                      for (g=0;g<16;g++) {if (intr[g].active) break;}
//                      if (g lt 16) then begin
                        if (active.h.h0) then begin
                            g=intr[16-BPRIO(active.h.h0)].grp;
            //              lev=0x10000>>(17-BPRIO(intr[g].active));
                            lev=0x10000>>(17-BPRIO(active.h.h1));
                            if ((lev and intr[g].active) eq 0) then { printt("oops lev= ")
    printxv(lev) printt(" active = ") printxv(intr[g].active) printt(" g = ")
    printd(g) NEWLINE}
//                            outputxnl(s6.curloc); outputxnl(active.w); outputxnl(lev);NEWLINE
                            CLEAR(intr[g].triggered,lev)  //clear
                            if (inst and 0x00100000) then begin
//                              SET(intr[g].armed,lev)         //arm it
//                              STS(intr[g].triggered,intr[g].signal,lev)  //retrigger if signal level is high
                                ARM(g,lev)
                                endif
                            ACTIVECHG(intr[g])
                            endif

                        endif
                    BPSD(s6,s6psd) } BREAK;
/*XPSD*/INST(0x0F)  PRIV BRANCHTR LAA{word rem; //psd *pd=(psd *)(pIEAXd(0));
                    word ieaxd=IEAX(inst,dword,2);
                    psd *pd=(psd*) pRxMEM0(ieaxd,dword);
                    rem=REM(pd,((ieaxd and 0xfff8)? 2048:64),dword);
                    GPSD(s6psd,s6);   //XPSD
                    *pd--=s6psd;   //Decrement pointer to increment sigadr!!!
                    if (rem le 1) pd=(psd*) pRxMEM0(++ieaxd,dword); //if crossing pages etc
                    s6psd.w.w0=pd->w.w0 and 0xf7f1ffff;
                    s6psd.n.wk=pd->n.wk; s6psd.n.ci|=pd->n.ci;
                    s6psd.n.ii|=pd->n.ii; s6psd.n.ei|=pd->n.ei;
                    s6psd.n.ma=pd->n.ma;
                    if (inst and 0x00800000) then s6psd.n.rp=pd->n.rp;
                    BPSD(s6,s6psd);
                    } BREAK;
        INST9(0x10) NODDR;
/*AD*/  INST(0x10)  {dword dw=RRu1; dw+=*pIEAXd(2); CC12d;
                    CCd34(LD(REG(inst),dw),dword);
                    if ((S6CC>>2) and s6.am) TRAP43;} BREAK;
        INST(0x11)  CMPD(RRu1,*pIEAXd(2)); BREAK;    //CD
        INST(0x12)  CCd34(LD(REG(inst),*pIEAXd(2)),dword);BREAK;  //LD
        INST(0x14)  TRAP40CC1                  //Nonexistant Instruction    
        INST(0x15)  *pIEAXd(0)=RRu1;BREAK;           //STD
        INST(0x16)  TRAP40CC1                  //Nonexistant Instruction    
        INST(0x17)  TRAP40CC1                  //Nonexistant Instruction
        INST9(0x18) NODDR;
/*SD*/  INST(0x18)  {dword dw=RRu1; dw-=*pIEAXd(2); CC12Sd;
                    CCd34(LD(REG(inst),dw),dword);
                    if ((S6CC>>2) and s6.am) TRAP43;} BREAK;
/*CLM*/ INST(0x19)  {udw temp; word r=R; temp.dw=*pIEAXd(2);
                     S6CC=(r lt temp.w.w0) or ((r gt temp.w.w0)<<1) or
                           ((r lt temp.w.w1)<<2) or ((r gt temp.w.w1)<<3);} BREAK;    
        INST(0x1A) {dword temp=*pIEAXd(2);       //LCD
                    LD(REG(inst),-temp);
                    S6CC &=8;
                    if (temp ne 0) then begin
                        if (temp gt 0) then S6CC|=1; else begin
                            if (temp eq 0x8000000000000000LL) then begin
                                S6CC|=5; if (s6.am) then TRAP43;
                                endif else S6CC|=2;
                            endelse
                        endif} BREAK;
/*LAD*/ INST(0x1B)  {dword temp=*pIEAXd(2); dword atemp=(temp ge 0) ? temp: -temp;
//                   printdw(temp) printt(" abs= ") printdw(atemp) NEWLINE
                     LD(REG(inst),atemp);      //LAD
                    S6CC &=8;
                    if (temp ne 0) then begin
                        if (temp eq 0x8000000000000000LL) then begin
             //               R=temp;
                            S6CC|=5; if (s6.am) then TRAP43;
                            endif else S6CC|=2;
                        endif} BREAK;

        INST(0x20)  NOIND;
//                    (R+=IMMED(inst));CC1234;
                    CC1234A(word, &R, IMMED(inst))
                    BREAK;   //AI
        INST(0x21)  NOIND;CMP(R,IMMED(inst));  BREAK;       //CI
        INST(0x22)  NOIND;CCt34(R=IMMED(inst),word);  BREAK;       //LI
#define SHIFTEA     word iea=IEA(inst,2);              \
                    word irx=field(inst,rx);         \
                    byte typ;                        \
                    word ci=iea;                     \
                    if (irx) ci+=SLOC(reg,irx,word); \
                    ci=(ci<<25)>>25;
#define SHIFTF(rp,normm,scv)               \
                        word neg,chara;    \
                        typeof(val) fmask=(((typeof(val))1)<<scv)-1;  \
                        switch(0) {        \
                case 0: if (rp lt 0) then begin neg=1; val=-(rp);      \
                            endif else begin neg=0; val=rp; endelse    \
                        frac=val and fmask;                            \
                        if (frac eq 0 || frac eq fmask) then begin     \
                            rp=0; S6CC= (ci ge 0)<<3; break; endif    \
                        chara=val>>scv;                                \
                        if (ci ge 0) then begin                                \
                            while (!(frac and normm) && ci && (chara ge 0))    \
                                {frac<<=4;ci--;chara--;}                       \
                            if (frac and normm) then S6CC|=8;                 \
                            if (chara lt 0) then S6CC|=4;                     \
                            endif                                              \
                        if (ci lt 0) then begin                                \
                            word sc=128-chara;                                 \
                            word nci=-ci;                                      \
                            while (frac && nci && sc )                         \
                                {frac>>=4;nci--;sc--;chara++;}                 \
                            if (nci lt sc || (nci eq 0)) then                  \
                                if (frac eq 0) then begin rp=0; break; endif   \
                            if (nci ge sc) then  S6CC=4;                      \
                            endif                                              \
                        val=frac or (((typeof(val))(chara and 0x7f))<<scv);    \
                        if (neg) then begin rp=-val; S6CC|=1; endif else begin\
                                rp=val; S6CC|=2; endelse                      \
                        }
		    
        INST(0x24)  { SHIFTEA                      //Shift floating
                    typ=(iea>>8) and 1;
                    S6CC=0;
                    if (typ eq 0) then begin       //single
			word *rp=RP;		    	    
                        word frac,val;
                        SHIFTF(*rp,0xf00000,24)
                        endif else begin           //double
                        dword rdw=RRu1;
                        dword val,frac;
                        SHIFTF(rdw,0xf0000000000000LL,56)
                        LD(REG(inst),rdw)
                        endelse
                    }BREAK;
        INST(0x25)  { byte occ=S6CC; SHIFTEA                      //Shift
                    typ=iea>>9;
                    S6CC &=3;
                    if (iea and 0x100) then begin
                        udw dr;dr.dw=RRu1;
                        if (ci gt 0) then begin
                            udw masked;
                            dword masked2=(((dword) 0x8000000000000000LL)>>ci) and dr.dw;
                            dword nzmo=masked2>>((ci lt 63) ? 63-ci : 0);
                            masked.dw=(((dword) 0x8000000000000000LL)>>(ci-1)) and dr.dw;
                            S6CC |= (NZMO(nzmo)<<2) or (PARITY(masked.w.w0 eor masked.w.w1));
                            endif
                        switch (typ & 3) {
 /*SLD*/                  case  0: if (ci ge 0) then if (ci lt 64) then dr.ud<<=ci; else dr.ud=0LL;
                                else if (ci gt -64) then dr.ud>>=-ci; else dr.ud=0LL;  break;
 /*SCD*/                  case 1: ci &= 63;    // force positive;
                                  dr.ud= (dr.ud<<ci) or (dr.ud>> (64-ci)); break;
 /*SSD*/                  case 3: if (xerox eq 9) {   //sigma 9 only
                                      int ssc=0,r1;
                                      uword ow0,nw0;
                                      S6CC=occ and 0xa; ow0=dr.w.w0;
                                      if ((dr.w.w0 and 0x80000000) eq 0) begin
                                         if (ci gt 0) then begin
                                            if (dr.w.w0 ne 0) ssc=32-BPRIO(dr.w.w0);
                                            else if(dr.w.w1 ne 0) ssc=64-BPRIO(dr.w.w1);
                                            else ssc=64;
                                            if (ci lt ssc) ssc=ci;
                                            endif
                                         if (ci lt 0) then begin
                                            if (dr.w.w1 ne 0) ssc=-BFPRIO(dr.w.w1);
                                            else if(dr.w.w0 ne 0) ssc=-32-BFPRIO(dr.w.w0);
                                            else ssc=-64;
                                            if (ci gt ssc) ssc=ci;
                                            endif
                                         endif
                                      r1=ci-ssc; ssc &=63;
                                      dr.ud= (dr.ud<<ssc) or (dr.ud>> (64-ssc));
                                      nw0=dr.w.w0;
                                      S6CC|=nw0>>31;
                                      if (ci gt 0) S6CC|=(((ow0 ^ nw0)>>29)&4);
                                      break;}
                          //others fall through to SAD (7 diagnostic wants it to...
                          //even though ref manual says undefined)
 /*SAD*/                  case  2: if (ci ge 0) then if (ci lt 64) then dr.dw<<=ci; else dr.dw=0LL;
                                else if (ci gt -64) then dr.dw>>=-ci; else dr.dw>>=63; break;
                          }
                        LD(field(inst,r),dr.dw);
                        endif else begin
                        word *pR=&R; unsigned long * upR=(unsigned long *)pR;
                        if (ci gt 0) then begin
                            word masked,masked2,nzmo;
                            if (ci lt 32) then begin
                                masked=(((word) 0x80000000)>>(ci-1)) and *pR;
                                masked2=(((word) 0x80000000)>>ci) and *pR;
                                nzmo=masked2>>(31-ci);
                                S6CC |= (PARITY(masked)) | (NZMO(nzmo)<<2 );
                                endif else begin
                                S6CC |= (PARITY(*pR)) | (NZMO(*pR)<<2);
                                if (typ eq 1 && ci gt 32) then begin
                                    masked=(((word) 0x80000000)>>(ci-33)) and *pR;
                                    S6CC ^= PARITY(masked);
                                    endif
                                endelse
                            endif

                        switch (typ & 3) {
 /*SLS*/                  case  0: if (ci ge 0) then if (ci lt 32) then *upR<<=ci; else *upR=0;
                                else if (ci gt -32) then *upR>>=-ci; else *upR=0;  break;
 /*SCS*/                  case  1: SCS(*pR,ci) ;break;   //circular single ...was SCS(*upR,ci)
 /*SSS*/                  case  3: if (xerox eq 9) {   //sigma 9 only
                                      int ssc=0,r1;
                                      uword nw,ow=*upR;
                                      if ((ow and 0x80000000) eq 0) begin
                                        if (ci gt 0) then begin
                                            if (ow ne 0) ssc=32-BPRIO(ow);
                                            else ssc=32;
                                            if (ci lt ssc) ssc=ci;
                                            endif
                                        if (ci lt 0) then begin
                                            if (ow ne 0) ssc=-BFPRIO(ow);
                                            else ssc=-32;
                                            if (ci gt ssc) ssc=ci;
                                            endif
                                        endif
                                      r1=ci-ssc;
                                      SCS(*upR,ssc);
                                      nw=*upR;
                                      S6CC=(occ & 0xa) |  (nw>>31);
                                      if (ci gt 0) S6CC|=((ow ^ nw)>>29)&4;
                                      break;}
                          //others fall through to SAD (7 diagnostic wants it to...
                          //even though ref manual says undefined)

                                break;   // undefined on s7   //sigma 9 only
 /*SAS*/                  case  2: if (ci ge 0) then if (ci lt 32) then *pR<<=ci; else *pR=0;
                                else if (ci gt -32) then *pR>>=-ci; else *pR>>=31; break;
                        } endelse;BREAK;}

#define SIGMA6 1
#define NEWCORE 1
        INST(0x26)  {word *temp=(pSLOCM(IEAX(inst,word,2),word,0)); CCt34(R=*temp,word);
                    if (NEWCORE & 1) then *temp |= 0x80000000;} BREAK; 
        INST(0x27)  TRAP40CC1                  //Nonexistant Instruction    
        INST(0x2A)  {word count=((S6CC) ? S6CC:16);   //LM
                     word eva=EVA;
                     word * r0=&(REGWA(0));
                     word ri=field(inst,r);
                     while (count) do begin
                        word *isadd=pRxMEM2(eva,word);
                        word rem=MIN(count,RXMREMw(isadd));
                        word j;
                        for (j=rem;j;j--) r0[-((ri++)&15)]=*isadd--;
                        eva=(eva+rem) and M17vx;
                        count-=rem;
                        endwhile
                    } BREAK;
        INST(0x2B)   {word count=((S6CC) ? S6CC:16);   //STM
                     word eva=EVA;
                     word * r0=&(REGWA(0));
                     word ri=field(inst,r);
                     while (count) do begin
                        word *idadd=pRxMEM0(eva,word);
                        word rem=MIN(count,RXMREMw(idadd));
                        word j;
                        for (j=rem;j;j--) *idadd--=r0[-((ri++)&15)];
                        eva=(eva+rem) and M17vx;
                        count-=rem;
                        endwhile
                    } BREAK;

        INST(0x2C)  TRAP40CCA        //Nonexistant Instruction   &CC3 if slave
        INST9(0x2C)  PRIV {word ix; word ainst=*pIEAXw(2);    //LRA
                     word *rp=RP,vp,pp;
                     word occ=S6CC;
                     S6CC =0;
                     switch (occ and 0xc) {
                        case 0: *rp= IEAX(ainst,byte,2) & M19vx;
                                if (!INMEM(*rp,byte)) {S6CC=0xc; BREAK;}
                                vp=*rp>>11; *rp^=(pp=s6.memmap[vp])<<2;
                                break;
                        case 4: *rp= IEAX(ainst,hword,2) & M18vx;
                                if (!INMEM(*rp,hword)) {S6CC=0xc; BREAK;}
                                vp=*rp>>10; *rp^=(pp=s6.memmap[vp])<<1;
                                break;
                        case 8: *rp= IEAX(ainst,word,2) & M17vx;
                                if (!INMEM(*rp,word)) {S6CC=0xc; BREAK;}
                                vp=*rp>>9; *rp^=(pp=s6.memmap[vp]);
                                break;
                        case 12: *rp= IEAX(ainst,dword,2) & M16vx;
                                if (!INMEM(*rp,dword)) {S6CC=0xc; BREAK;}
                                vp=*rp>>8; *rp^=(pp=s6.memmap[vp])>>1;
                                break;
                        endswitch
                        S6CC|=s6.memac[vp];
                        if ((vp<<9) ge msz) S6CC|=0xc;
                        pp=(pp>>9) eor vp;
//                      output(pp); output (vp);
                        *rp|=s6.wl[pp]<<24;
                        BREAK;}
        INST(0x2D) PRIV switch (S6CC) {  //LMS Sigma 9    and CC3 if slave
                        case 0: {word *temp=(pSLOCM(IEAX(inst,word,2),word,0));
                                R=*temp;
                                if (NEWCORE & 1) then *temp |= 0x80000000;}
                                break;
                        default: S6CC=0; break;
                        endswitch
                        BREAK;
        INST(0x2E)  PRIV
                    if (pcpwait && ! (intr[0].armed & intr[0].enabled))
                         then goto wait; else begin //WAIT
                        SET_WAIT
                        if ((event and pcp_evt && pcp_flag and cntrl_c_flag)) goto wait;
                        pthread_yield();
                        if (!event) s6.ia--; else {SET_RUN}
                        LAA; BREAK;
                        endelse
                  //   goto wait;  // nothing important is going on...
        INST9(0x2F) PRIV SETRP9(s6,*pIEAXw(2)>>4);BREAK;
        INST(0x2F)  PRIV SETRP(s6,*pIEAXw(2)>>4);BREAK;      //LRP
        INST(0x30)  R+=*pIEAXw(2);CC1234; BREAK;      //AW
        INST(0x31)  CMP(R,*pIEAXw(2));    BREAK;      //CW
        INST(0x32)  CCt34(R=*pIEAXw(2),word); BREAK;      //LW
        INST(0x33)  {word Rs=ifield(inst,r);// word *temp=&(*pIEAXw);     //MTW
                     (Rs ne 0) ? ({*pIEAXw(0)+=Rs;CC1234;}) : (CCt34(*pIEAXw(2),word));
                    };BREAK;
        INST(0x34)  TRAP40CC1            //Nonexistant Instruction    
        INST(0x35)  *pIEAXw(0)=R; BREAK;             //STW

        INST(0x38)  R-=*pIEAXw(2);CC1234S;BREAK;      //SW
/*CLR*/ INST(0x39)  {word r=R; word ru1=Ru1; word iea=*pIEAXw(2);
                     S6CC=(r lt iea) or ((r gt iea)<<1) or
                           ((ru1 lt iea)<<2) or ((ru1 gt iea)<<3);} BREAK;    
        INST(0x3A)  {word temp=*pIEAXw(2); R=-temp;  //LCW
                    S6CC &=8;
                    if (temp ne 0) then begin
                        if (temp gt 0) then S6CC |= 1; else
                        if (temp eq 0x80000000) then begin
                            S6CC|=5; if(s6.am) then TRAP43;
                            endif else S6CC|=2;
                        endif} BREAK;
        INST(0x3B)  {word temp=*pIEAXw(2); R=abs(temp);  //LAW
                    S6CC &=8;
                    if (temp ne 0) then begin
                        if (temp eq 0x80000000) then begin
                            S6CC|=5; if (s6.am) then TRAP43;
                            endif else S6CC|=2;
                        endif} BREAK;
        INST(0x42)  TRAP40CC1            //Nonexistant Instruction
        INST(0x43)  TRAP40CC1            //Nonexistant Instruction    
        INST(0x44)  {word ix; word ainst=*pIEAXw(2);    //ANLZ
                     word *rp=RP;
                     ix=S6CC=anlz[OPCODE(ainst)];
                     S6CC |= (ainst>>30) and 2;
                     switch (ix) {
                        case 0: *rp= IEAX(ainst,byte,2) & M19vx; break;
//                        case 1: *rp=ainst and M20; break;//Does this set R?
                        case 4: *rp= IEAX(ainst,hword,2) & M18vx; break;
                        case 8: *rp= IEAX(ainst,word,2) & M17vx; break;
                        case 9: break;
                        case 12: *rp= IEAX(ainst,dword,2) & M16vx; break;
                        default: break;
                        endswitch} BREAK;
        INST(0x45)  {word mask=Ru1; uword r=(R) & mask;    //CS
                    uword e=(*pIEAXw(2) & mask); S6CC&=0xc;
                    if (r lt e) then S6CC|=1; else
                        if (r gt e) then S6CC|=2;} BREAK;
        INST(0x46)  {word val,*temp=pIEAXw(0);val=*temp;              //XW
                    *temp=R; CCt34(R=val,word);} BREAK;
        INST(0x47)  {udw dtemp; word *temp=pIEAXw(0); dtemp.dw=RRu1; //STS
                    *temp=(dtemp.w.w0 & dtemp.w.w1) | (*temp & not(dtemp.w.w1));} 
                    BREAK;
        INST(0x48)  R^=*pIEAXw(2);CC34;  BREAK;      //EOR
        INST(0x49)  R|=*pIEAXw(2);CC34;  BREAK;      //OR
        INST(0x4A)  {word mask=Ru1;word *rp=RP;   //LS
                    CCt34(*rp=(not(mask) and *rp) or (mask and *pIEAXw(2)),word);BREAK;}
        INST(0x4B)  R&=*pIEAXw(2);CC34;  BREAK;      //AND
        INST(0x4C)  IOINST            BREAK;      //SIO
        INST(0x4D)  IOINST            BREAK;      //TIO
        INST(0x4E)  IOINST            BREAK;      //TDV
        INST(0x4F)  IOINST            BREAK;      //HIO

        INST(0x50)  //R+=*pIEAXh(2);CC1234;BREAK;      //AH
                    {word hw=*pIEAXh(2);
                    CC1234A(word,&R,hw )} ; BREAK;
        INST(0x51)  CMP(R,*pIEAXh(2)); BREAK;        //CH
        INST(0x52)  CCt34(R=*pIEAXh(2),word); BREAK; //LH
        INST(0x53)  {hword Rs=ifield(inst,r); //hword temp=(*pIEAXh(0));   //MTH
                    if (Rs eq 0) {CCt34(*pIEAXh(2),hword);BREAK;}
                      CC1234A(hword, pIEAXh(0), Rs)
//                    (Rs ne 0) ? ({*pIEAXh(0)+=Rs;CC1234}) : (CCt34(*pIEAXh(2),hword));
                    };BREAK;
        INST(0x54)  TRAP40CC1            //Nonexistant Instruction    
        INST(0x55)  {word temp=R; *pIEAXh(0)=temp; temp=temp>>15;      //STH
                    if (temp eq 0 || temp eq -1) then S6CC&=0xb; else
                        S6CC|=4;} BREAK;
        INST(0x58)  R-=*pIEAXh(2);CC1234S;  BREAK;    //SH
        INST(0x59)  TRAP40CC1            //Nonexistant Instruction
        INST(0x5A)  CCt34(R=-*pIEAXh(2),word); BREAK; //LCH
        INST(0x5B)  CCt34(R=abs(*pIEAXh(2)),word); BREAK;    //LAH
        INST(0x5C)  TRAP40CC1            //Nonexistant Instruction
        INST(0x5D)  TRAP40CC1            //Nonexistant Instruction    
        INST(0x5E)  TRAP40CC1            //Nonexistant Instruction    
        INST(0x5F)  TRAP40CC1            //Nonexistant Instruction    

        INST(0x62)  TRAP40CC1            //Nonexistant Instruction
#define BRLAA  fbr=temp-s6.ia; iirem=iirem-fbr; iiad=iiad-fbr; \
               if (iirem gt iiremx || iirem lt 0) LAA;
        INST(0x64)  {word fbr,temp=IEAX(inst,word,2); if (--(R) gt 0) then begin  //BDR
//                      if(s6.ia-1 eq temp) then {R=0; BREAK;}  //super optimizer
                        BRLAA
                        BRANCHTR s6.ia=temp and M17;endif} BREAK;
        INST(0x65)  {word fbr,temp=IEAX(inst,word,2); if (++(R) lt 0) then begin  //BIR
//                      if(s6.ia-1 eq temp) then {R=0; BREAK;}  //super optimizer
                        BRLAA
                        BRANCHTR s6.ia=temp and M17;endif} BREAK;
        INST(0x66)  //*pIEAXw(0)+=R;CC1234;BREAK;      //AWM
                    CC1234A( word, pIEAXw(0), R) ;BREAK;
        INST(0x67)  inst=*pIEAXw(1); goto fetched;     //EXU  int check s/b after fetched
        INST(0x68)  {word fbr,temp=IEAX(inst,word,2);
                    if ((REG(inst) and S6CC) eq 0) then begin    //BCR
                    BRLAA
                    BRANCHTR s6.ia=temp and M17; endif;} BREAK;
        INST(0x69)  {word fbr,temp=IEAX(inst,word,2);
                    if ((REG(inst) and S6CC) ne 0) then begin    //BCS
                    BRLAA
                    BRANCHTR s6.ia=temp and M17; endif;} BREAK;
        INST(0x6A)  BRANCHTR LAA{word temp=IEAX(inst,word,2); R=s6.ia; s6.ia=temp and M17;}BREAK; //BAL
        INST(0x6B)  {word temp=*pIEAXw(2); S6CC=(temp>>28) and 0xf; //INT
                     R=(temp>>16) and 0xfff;*Ru1P=temp and 0xffff;}BREAK;
/*RD*/  INST(0x6C)  PRIV {word d,r=REG(inst),ieax=EVA;
                //    if (!REG(inst)) BREAK;
                    switch ((ieax >>12) and 0xf) {
                        case 0: S6CC=cp.sw.sense;
                               if (r) *RP=0; BREAK;
     /*COC*/            case 3: if (ieax and 0xf00) goto watchdog;
                                S6CC=0; d=coc_rd(ieax and 0xff);
                                if (r) *RP=d;
                                BREAK;
                        case 0xF:
                            if (r && ((ieax and 0xfc0) eq 0)) then begin
//                             outp(0x70,(ieax and 0x3f)); *RP=inp(0x71); break;
                               char buff[30]="000000000000000000000000";
                               struct timeval tv;
                               time_t curtime;
                         *RP=0; //break;
                              gettimeofday(&tv, NULL); 
                               curtime=tv.tv_sec;
                                 strftime(buff,30,"%m-%d-%Y  %T.",localtime(&curtime));
                               switch(ieax and 0x3f) {
#define pdec(x) ({char * _x=x; int v=((((* _x) & 0x0f)<<4) or( (*++_x) and 0x0f));v;})
                                 case 0:  buff[20]=0; *RP= pdec(&buff[18]); break;  //sec
                                 case 2:  buff[17]=0; *RP= pdec(&buff[15]); break;  //min
                                 case 4:  buff[14]=0; *RP= pdec(&buff[12]); break;  //hour
                                 case 7:  buff[5] =0; *RP= pdec(&buff[3]); break;  //dd
                                 case 8:  buff[3] =0; *RP= pdec(&buff[0]); break;  //mm
                                 case 9:  buff[10]=0; *RP= pdec(&buff[8]); break;  //yy
                                 case 32: buff[8] =0; *RP= pdec(&buff[6]); break;  //cc
                                 default:  *RP=0; break;
                                }
                           break;
                            endif 
                            else goto watchdog;
                        default: goto watchdog;} } BREAK;
//                    outp(0x70,0x00); sec=inp(0x71);
//                    outp(0x70,0x02); min=inp(0x71);
//                    outp(0x70,0x04); our=inp(0x71);
//                    outp(0x70,0x07); dd=inp(0x71);
//                    outp(0x70,0x08); mm=inp(0x71);
//                    outp(0x70,0x09); yy=inp(0x71);
//                    outp(0x70,0x32); cc=inp(0x71);
        INST(0x6D)  PRIV {word ieax=EVA;        //WD
                    word r=REG(inst); word d= r ? SLOC(reg,r,word) : 0;
                    word mode=(ieax >> 12 ) and 0xf;
                    switch (mode) {
                        case 0: switch ((ieax and 0xff8)>>3){    //internal computer control
                            case 6: s6.ci|=(ieax>>2) and 1;      //set cie in psd
                                    s6.ii|=(ieax>>1) and 1;
                                    s6.ei|=(ieax and 1); break;
                            case 4: {byte nb=not(ieax);          //reset cie in psd
                                    s6.ci&=(nb>>2) and 1;
                                    s6.ii&=(nb>>1) and 1;
                                    s6.ei&=nb and 1;} break;
                                    SET(event,intr_evt)
                            case 8: {byte lsb=ieax and 7;        //set/reset/toggle alarm
                                    switch (lsb){
                                        case 0: cp.alarm=0; break;
                                        case 1: cp.alarm=1; break;
                                        case 2: //{byte i; i=inp(0x61) and 0xfe;
                                              //   i^=0x02; outp(0x61,i);}break;  //make it sing 
                                        default: break;
                                        } break;
                            default: goto watchdog;}
                            S6CC=cp.sw.sense;
                            }
                        case 1:                   //interrupt control
                            {word g=ieax and 0xf;
                             word code=(ieax>>8) and 7;
                             i_states * pg=&(intr[g]);
                               d &= 0xffff;                    // gsp 10/5/09
                             switch (code) {
                                case 1: CLEAR(pg->armed, d);
                                        CLEAR(pg->triggered, d);break;
                                case 2: CLEAR(pg->triggered, d);
                                 //     SET(pg->armed, pg->exists and d);
                                        SET(pg->enabled, pg->exists and d);
                                        ARM(g,d);
                                        break;
                                case 3: //SET(pg->armed, pg->exists and d);
                                        CLEAR(pg->enabled, d);
                 //gsp 10/5-8/09        CLEAR(pg->triggered, d);
                                        ARM(g,d);
                                        break;
                                case 4: SET(pg->enabled, pg->exists and d);break;
                                case 5: CLEAR(pg->enabled, d);break;
                                case 6: pg->enabled =pg->exists and d;break;
                                case 7: SET(pg->triggered, pg->armed and d);break;
                                }
                                ACTIVECHG((*pg))
                                S6CC&=3;  //what about cc3,cc4 here ???
                            } SET(event,intr_evt) BREAK;
    /* COC */           case 3: if (ieax and 0xf00) goto watchdog;
                                else S6CC=coc_wd(ieax and 0xff,d);
                                break;
                        default: //external dio here
                            goto watchdog;
                        endswitch
                    }BREAK;
        INST(0x6E)  PRIV STS(S6CC,iop(AIO,0),0xc)
	            if (REG(inst) && !(S6CC and 8)) *RP=MEMW(0x20);  BREAK;      //AIO

        INST(0x70)  {byte temp=*pIEAXb(2);       //LCF
                    if (inst and 0x200000) then S6CC=(temp >> 4) and 0xf;
                    if (inst and 0x100000) then begin
                        s6.fs=(temp>>2) and 1;
                        s6.fz=(temp>>1) and 1;
                        s6.fn=temp and 1;
                        endif}
                    BREAK;
        INST(0x71)  CMP(R and 0xff,(long) *pIEAXb(2)); BREAK;  //CB
        INST(0x72)  CCt34b(R=(word)*pIEAXb(2)); BREAK;      //LB
        INST(0x73)  {signed char Rs=ifield(inst,r);// byte *temp=&(*pIEAXb);     //MTB
                    if (Rs eq 0) {CCt1234b(*pIEAXb(2));BREAK;}
                    {signed char * cp=pIEAXb(0);
                      signed char k=*cp;
                      word res= (unsigned char) k + (unsigned char) Rs;
                      if (res and 0x100) s6.cc =8; else s6.cc=0;
                      if ((res and 0xff) ne  0) s6.cc|=2;
                     *cp=res;
                    }
 //                    (Rs ne 0) ? ({*pIEAXb(0)+=Rs;CC1234b;}) : (CCt1234b(*pIEAXb(2)));
                    } BREAK;
        INST(0x74)  *pIEAXb(0)=(((((S6CC<<2) or s6.fs) <<1) or s6.fz) <<1) or s6.fn;BREAK; //STCF
        INST(0x75)  *pIEAXb(0)=R;        BREAK;      //STB
#define MULTIPLY32(m)   {word wprod;                 \
                /*   long long prod=mult(Ru1,m); */ \
                     long long prod=((long long)Ru1) * m;         \
                     CCd34(prod,dword);              \
                     wprod=prod;                     \
                     if (wprod ne prod) S6CC|=4; else S6CC&=0xb;   \
                     LDl(REG(inst),prod);}
        INST(0x37)  MULTIPLY32(*pIEAXw(2)) BREAK;           //MW
        INST(0x23)  NOIND; MULTIPLY32(IMMED(inst)) BREAK;     //MI
        INST(0x57)  {word rh=SLOC(reg,((REG(inst)<<1)+1),hword);  //MH
                     CCt34(Ru1=rh * *pIEAXh(2),word);} BREAK;
        INST(0x36)  {dword dividend;                              //DW
                    word divisor=*pIEAXw(2);
                    word remainder,quotient;
                    word dovfl=0;
                    word r=REG(inst);
                    if (r and 1) then dividend=R; else dividend=RRu1;
                    if (divisor eq 0) then dovfl=1; else begin
                        quotient=dividend / divisor;
                        remainder=dividend mod divisor;
//                      if (mult(quotient,divisor)+remainder ne dividend) then dovfl=1;
                        if ((((long long) quotient) * divisor)+ remainder ne dividend) then dovfl=1;
                        if (quotient eq 0x80000000) then dovfl=1;
                        endelse
                    if (dovfl) then begin
                        S6CC|=4;
                        if (s6.am) then TRAP43;
                        BREAK; endif
                    S6CC&=8; CCt34(quotient,word);
                    Ru1=quotient;
                    if ((r and 1) eq 0) then R=remainder; 
                    } BREAK;
        INST(0x56)  { word dividend;                              //DH
                    hword divisor=*pIEAXh(2);
                    word quotient;
                    dividend=R;
                    if (divisor ne 0) then begin
                        quotient=dividend / divisor;
                        endif else begin
                        S6CC|=4;
                        if (s6.am) then TRAP43;
                        BREAK; endelse
                    S6CC&=8; CCt34(quotient,word);
                    R=quotient;
                    } BREAK;
