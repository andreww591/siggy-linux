#include "coccom.h"
extern unsigned char ring[256];
extern ringii,ringor;
extern com_modemstat[NP],com_linestat[NP];
extern com_flag,com_in_flag;
extern int comx[8], comxl[NP], ptype[NP], baud[NP];
extern defined_coms;
typedef struct {
    uword run[2];        //internal coc state enables xmit interrupts
    uword xscanhold[2];  //xmit reg empty anded with run
    byte dsr[64];        //dataset ready (dtr with null mod
    byte rcv[64];        //internal coc state- receiver on or off
    byte rds[64];        //xmit data set state
    byte xlsp[64];
    byte test[64];       //test mode loopback state
    byte ltype[64];      //0 for virtual terminal, 1,2,3,4 for com1234
                         //actually now index into type table
    hword baud[64];      //if com port, baud rate to set
    byte usr[64];        //virtual term fkey number
    byte fd[64];         //socket fd for this line  
    devdata * dev;       //pointer to device data (iop address, etc)
    word iloc0;          //e.g. 60
    word iloc1;          //e.g. 61
//  word ig0;            //iloc0 group number
//  uhword ib0;          //iloc0 bit
    word ig1;            //iloc1 group number
    uhword ib1;          //iloc1 bit
    byte scan;           //current coc internal scanner xmit line pointer
    byte filler;
} cocdata;

