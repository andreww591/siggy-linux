#if 0
//button locations for image/pcp.xpm
#define RCFILE  "siggy.rc0"
#define MAINSZ   640,472
#define TSWSZ    11,19
#define PBSZ     29,22
#define BTSWSZ   25,25
typedef struct {int x,y,xsz,ysz;} xysz;

xysz xyzt[] = {
[SENSE] =    { 324,120,40 + 4* TSWSZ},
[WATCHDOG] = {  51,122,8 + TSWSZ},
[PWRL] =     {  44,209,PBSZ},
[CPURL] =    {  95,209,PBSZ},
[IORL] =     { 151,208,PBSZ},
[LOADL] =    { 206,207,PBSZ},
[SYSRL] =    { 336,206,PBSZ},
[NORMALL] =  { 389,206,PBSZ},
[RUNL]    =  { 445,206,PBSZ},
[WAITL]   =  { 498,204,PBSZ},
[INTERRUPTL]={ 552,203,PBSZ},
[ADDSTOP] =  { 162,333,8 + TSWSZ},
[SELADD]  =  { 209,333,10 + 17 * TSWSZ},
[DATA] =     {  44,399,10 + 32 * TSWSZ},
[INSERTPSD]= { 474,278,BTSWSZ},
[INSTADD] =  { 555,278,BTSWSZ},
[STORESW] =  { 474,335,BTSWSZ},
[DISPLAYSW]= { 555,334,BTSWSZ},
[DATASW] =   { 474,391,BTSWSZ},
[COMPUTESW]= { 555,391,BTSWSZ}
             };
#else
//buttons for 8810 pictures
#define RCFILE  "siggy.rc"
#define MAINSZ   640,472
#define TSWSZ    11,19
#define PBSZ     29,22
#define BTSWSZ   25,29
typedef struct {int x,y,xsz,ysz;} xysz;
#define XLC 555-8
#define XTC1 474-7
#define YP1 266
#define YP2 297
#define YRB 207
#define YR4 276
#define YRA 330
#define YR5 333
#define YR6 387
#define YRD 393


xysz xyzt[] = {
[SENSE] =    { 324,124,36 + 4* TSWSZ},
[WATCHDOG] = {  52,123,6 + TSWSZ},
[PWRL] =     {  44,YRB+2,PBSZ},
[CPURL] =    {  98,YRB+2,PBSZ},
[IORL] =     { 151,YRB+1,PBSZ},
[LOADL] =    { 206,YRB,PBSZ},
[SYSRL] =    { 330,YRB,PBSZ},
[NORMALL] =  { 385,YRB,PBSZ},
[RUNL]    =  { 438,YRB,PBSZ},
[WAITL]   =  { 490,YRB,PBSZ},
[INTERRUPTL]={ 545,YRB,PBSZ},
[PSD1] =     {  46,YP1,-2+32*TSWSZ},
[PSD2] =     {  46,YP2,-2+32*TSWSZ},
[ADDSTOP] =  { 164,YRA,8 + TSWSZ},
[SELADD]  =  { 209,YRA,2 + 17 * TSWSZ},
[DATA] =     {  46,YRD,1+ 32 * TSWSZ},
[DATAL] =    {  46,360,-2+32*TSWSZ},
[INSERTPSD]= { XTC1,YR4,BTSWSZ},
[INSTADD] =  { XLC,YR4,BTSWSZ},
[STORESW] =  { XTC1,YR5,BTSWSZ},
[DISPLAYSW]= { XLC,YR5-1,BTSWSZ},
[DATASW] =   { XTC1,YR6,BTSWSZ},
[COMPUTESW]= { XLC,YR6,BTSWSZ}
             };
#endif