#include "sigma.h"
#include "sigmaio.h"
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include <stdio.h>   /* for fprintf(stderr,... */
#include <termios.h>
#include <sched.h>
#include "rvolatiles.h"
#include "srglobals.h"
#include <pthread.h>
#include <string.h>

#define FORKSYS(cmd) ({pid_t cpid; \
    cpid = fork();                    \
    if (cpid == -1) { perror("fork"); exit(EXIT_FAILURE); }   \
    if (cpid == 0) {                                      \
        system(cmd);                  \
        _exit(EXIT_SUCCESS);}      \
     cpid;})

#define CLONE(stksize,func,arg)  ( {int *stack_bot=(int*)malloc(stksize) + stksize; \
          clone(func,stack_bot,CLONE_VM | CLONE_FILES,arg);})
//#include "thread.h"



void * console(void * );
void set_tty_cooked();

extern i_states intr[];
extern ibits active,waiting,ewaiting;
extern event;
extern limit;

//volatile int screen,pipei,pipeo;
//devdata * TTYdev=0;
int count=0;

void inp_handler(devdata * dev, char k)
{
       if((k eq 0x03) or (k eq 80))  TRIGGER(0,0x10) else  //cnt-c or F1 for cpu interrupt
       dev_TY_charin(dev,k);
}



console_init( devdata * dev)
{
        pthread_t console_pid=0;
//        TTYdev=dev;
        int rc = pthread_create(&console_pid, NULL, console, dev);
//        outputs(dev->fid);
        id_kid[nkids++]=console_pid;
//        output(console_pid); NEWLINE // fflush(stdout);
        while (dev->pipeo eq 0) usleep(10000);
}

int concnt=0;

void console_write(devdata * dev,char  buf)
{   int rc=write(dev->pipeo,&buf,1);
    concnt++;
   if (limit gt 0) {
     if (concnt gt limit ) {printt("\n\rconsole output limit exceeded ")
              output(concnt);NEWLINE;rc=0;}}
   if(rc != 1) {printf("\rerror writing to pipeo for console\r\n");
                exit(1);}
}



void *  console(void * vdev)
{
      int pipei,pipeo;
      pid_t pidc;
      int rc,noexisting;
      char inchar;
      devdata * dev = vdev;
      char * fnamei = dev->fid;
//      char * fnamei =  fid;
      char fnameo[50];
      char buf[120];
//        outputs(fnamei);
      strcpy(fnameo,fnamei);
      strcat(fnameo,"o");
      mkfifo(fnamei,0660);
      mkfifo(fnameo,0660);
      sprintf(buf,"ps -f -C console2|grep console2|grep %s",dev->fid);
      noexisting = system(buf);
      if (noexisting) begin
          sprintf(buf,"konsole -T %s -e ./console2 %s",dev->name,dev->fid);
//          V_CHK(outputs(buf);NEWLINE)
          pidc=FORKSYS(buf); 
          id_kid[nkids++]=pidc;
          endif

      pipei   = open(fnamei,O_RDONLY);
      assert(pipei>=0);
      dev->pipeo = open(fnameo,O_WRONLY  );
      assert(pipeo>=0);
//      set_led(1);
      while(1) {
	    rc=read(pipei,&inchar,1);
	    if(rc==1) {
		inp_handler(dev,inchar);}
      }
}

