        case 0x0F:  ALABEL(trap_xpsd)
                    { psd *pd=(psd *)(IEAXdU); GPSD(s6psd,s6);   //XPSD
                    SET_RUN
        //            printt("trap location ") printxv(s6.curloc)printt(" X P S D executing!!!\n")
                    *pd--=s6psd;    //Decrement pointer to increment sigadr!!!
                    s6psd.w.w0=pd->w.w0;
                    s6psd.n.wk=pd->n.wk; s6psd.n.ci|=pd->n.ci;
                    s6psd.n.ii|=pd->n.ii; s6psd.n.ei|=pd->n.ei;
                    s6psd.n.ma=pd->n.ma;
                    if (inst and 0x00800000) then s6psd.n.rp=pd->n.rp;
  /*Trap 40*/       if (s6.curloc eq 0x40) then begin
                        s6psd.n.cc|=s6.t40cc;
                        if (inst and 0x00400000) then s6psd.n.ia+=s6.t40cc;
                        endif
  /*CAL1-4*/        if ((s6.curloc ge 0x48) and (s6.curloc le 0x4b)) then begin
                        s6psd.n.cc|=field(calinst,r);
                        if (inst and 0x00400000) then s6psd.n.ia+=field(calinst,r);
                        endif
                    s6psd.w.w0&=0xf7f1ffff;
                    BPSD(s6,s6psd);
                    if (s6.curloc ge 0x50) then begin  //How do we make 50,51 active?
                        group=(s6.curloc ge 0x60) ? s6.curloc/16-4 : 0;
                        level=0x8000 >> (group ? (s6.curloc and 0xf) : ((s6.curloc-2)and 0xf));
                        CLEAR((intr[group]).armed,level)   //becomes active
                        ACTIVECHG(intr[group])
                        endif
                    } LAA; goto *nexti[event];  //assume no crossover change between doublewords

#define COUNTER(t) {word _z; if (s6.curloc eq 0x55) then (_z=(*IEAX##t##M+=ifield(inst,r))); \
                    else (_z=(*IEAX##t##U+=ifield(inst,r)));  \
                    if (!_z) then goto countzero;} break;

        case 0x33:  COUNTER(w)  //MTW
        case 0x53:  COUNTER(h)  //MTH
        case 0x73:  COUNTER(b)  //MTB

        countzero:  if ((s6.curloc ge 0x52) and (s6.curloc le 0x55))  // check for Counter 1-4 count pulse
                    SET((intr[0]).triggered,(0x200>>(s6.curloc-0x52)) and // and trigger Counter 1-4 zero
                        ((intr[0]).armed)) break;

        default:
//          should be trap 4C (instruction exception) if Sigma 9
            printt("Unimplemented Instruction in Trap/Interrupt Location ")
            printxv(s6.curloc) printt(" = ") printx(inst) printt("  ")
            goto halt;
