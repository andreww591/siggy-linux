#define clock_evt 1
#define kbd_evt 2
#define com_evt 4
#define endwait_evt 8
//#define com_vect 0x0b    //IRQ3
//#define com1 0x02e8       //com4

       //   com1 3f8   com2 2f8   com3 3e8   com4 2e8
         //     IRQ4       IRQ3      IRQ4        IRQ3
         // int 0x0C       0x0B      0x0C        0x0B
// following is also defined in sigma.h
#define SET(x,b) {int _b=(b); asm("orl %1,%0; " : "=m" (x) : "ir" (_b) : "cc");}

