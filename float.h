typedef struct {  unsigned long long frac; short int exp;} fltt;
typedef union {
    float f;
    long l;
    double d;
    long long ll;
    unsigned char c[10];
    fltt ft;
} ufl;
#define ABS(x) ({typeof(x) _x=(x); (_x >=0) ? _x: -(_x);})
#define BPRIO2(x) ({typeof(x) _x=(x); \
    int _h,_p=BPRIO(_x and 0xffffffff); \
    if (sizeof(_x) gt 4 ) if(_h=_x>>32) _p=BPRIO(_h)+32;_p;})
#define SFTT(s,t,otp,esv,bp) { int d1; \
   typeof(s) _s =(s); unsigned long long __s,__sf;  \
   __s=((long long)ABS(_s))<<(64-sizeof(_s)*8); \
   esv=(__s >> 56)-64; \
   __sf=__s and 0xffffffffffffffLL; \
   if (__sf) { \
   bp=64-BPRIO2(__sf); \
   t.frac= __sf << bp; \
   t.exp= (esv*4+16383-bp+7) | ((_s>>(sizeof(_s)*8-16)) & 0x8000);}\
   else {t.frac=0; t.exp=0; bp=0;}}
#define SFSTT(s,t,otp,esv,bp) { \
   int _s =(s); int _sa; unsigned int __sf;  \
   _sa=ABS(_s); \
   esv=(_sa >>24) -64; \
   __sf=_sa and 0xffffff; \
   if (__sf) {  \
   bp=32-BPRIO(__sf); \
   t.frac= __sf << bp; \
   t.frac<<=32; \
   t.exp= (esv*4+16383-bp+7) | ((_s>>16) & 0x8000); \
   bp=bp+32; } \
   else {t.frac=0; t.exp=0; bp=0;}}

#define OVERFLOWas(t) if (exph ge 64) {S6CC=6+(t.exp>>15); TRAP44;}
#define OVERFLOWm(t) OVERFLOWas(t)
#define OVERFLOWd(t) OVERFLOWm(t)

#define UNDERFLOWas(t) \
     {  if (s6.fn) begin   \
            maxexp=MAX3(exph,t1exp,t2exp); \
            t.frac>>=4*(maxexp-exph); \
            exph=maxexp; \
            endif else begin   \
            if (t.frac eq 0) if (s6.fs) {S6CC=8; TRAP44;} else {S6CC=8; goto tzero;} \
            if (exph lt -64) begin  \
                if (s6.fz) {S6CC=0x0e + (t.exp>>15); TRAP44;} else begin \
                        if (s6.fs && MAX(t1exp,t2exp)-exph gt 2) \
                             {S6CC=0xa + (t.exp>>15); TRAP44;} \
                             else  {S6CC=0xc;goto tzero;} \
                        endelse \
                endif else begin \
                if (MAX(t1exp,t2exp)-exph gt 2) {if (s6.fs) {S6CC=0xa+(t.exp>>15);TRAP44} else {\
            /*    PCP_BREAK; printd(t1exp) printd(t2exp) printd(exph) NEWLINE */ \
                S6CC=8;}} \
                endelse \
            endelse  }
#define UNDERFLOWm(t)    \
    if (t.frac && (exph lt -64)) begin    \
        if (s6.fz) {S6CC=0x0e + (t.exp>>15); TRAP44;} else {S6CC=0xc;goto tzero;} \
        endif
#define UNDERFLOWd(t) UNDERFLOWm(t)
#define TZCCas {if (!(s6.fn)) {S6CC=8; if (s6.fs) TRAP44;}}
#define TZCCm
#define TZCCd

#define TTSF(t,s,otyp) ({ __label__ tzero; int exp,exph;\
   S6CC=0; \
   if (t.exp || t.frac) {\
        exp=(t.exp and 0x7fff) -16383+4; \
        exph=exp>>2; \
        OVERFLOW##otyp(t)  \
        UNDERFLOW##otyp(t)    \
        s=(t.frac >> ( 75- sizeof(s) *8- (exp and 3))); \
        if (s) begin s |= ((64LL+(exph))<<(sizeof(s)*8-8));     \
               if(t.exp lt 0) {s=-s;S6CC|=1;} else S6CC|=2; \
               endif ; \
        endif else {TZCC##otyp  tzero: s=0;}});

#define FLOP(op,a,b,c) {asm volatile ("fldt %2; fldt %1; f" #op "p %%st(1) ; fstpt %0;" : \
    "=m" (a) : "m" (b), "m" (c) : "cc");}
#define FMUL(a,b,c) FLOP(mul,a,b,c)
#define FDIV(a,b,c) FLOP(div,a,b,c)
#define FADD(a,b,c) FLOP(add,a,b,c)
#define FSUB(a,b,c) FLOP(sub,a,b,c)
#define printft(var)  {int i;for (i=9;i>=0;i--) printf("%.2hX ",var.c[i]); NEWLINE }

