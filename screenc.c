#include <stdio.h>
//#include "screen.h"
#include "idlop.h"

#include "mv.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <sched.h>


#include "sigma.h"
#include "sigmaio.h"
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include <stdio.h>   /* for fprintf(stderr,... */
#include <termios.h>
#include <sched.h>
#include "cpu.h"
#include "coc.h"

#include "rglobals.h"
#include "rvolatiles.h"

#include "hostint.h"
#include "thread.h"

int keyboardin(void (*));                 //func)(char));
int set_tty_raw();
#undef V_CHK
#define V_CHK(x) {}

int showscreen(int usr)
{
user=usr;                   //would be nice to set focus here
}


void kbd_init()                 //former name was screen_init
{
int rc;
pid_t pidk;
V_CHK(printf("kbd_init called\n");)
rc = pthread_create(&pidk, NULL, keyboardin, NULL);
id_kid[nkids++]=pidk;
set_tty_raw();                 //why is this here and also at siggy.c 190?
}

unsigned char mkb_getc_w(void)
{
  unsigned char ch;
  size_t size;
  while (1)
  {
    usleep(20000);        /* 1/50th second */
    size = read (STDIN_FILENO, &ch, 1);
    if (size > 0) {break;}
  }
  return ch;
}

//  runs as a thread waiting for input
//  when input is received, causes kbhit to return 1 and sets kbd_evt

volatile char inchar=0;

int keyboardin(void *func)   //keyboard input thread main loop
{
//   S_LOCK(KB);
   while (1) {
     inchar=mkb_getc_w();
     event|=kbd_evt;
   }
}


int kbhit()
{
    if (inchar == 0) return 0;
    return 1;
}

int getkey()
{
int k=inchar;
inchar=0;
return k;
}


extern int errno;                 
int kbhit_init=0;
static struct termios termattr, save_termattr;
static int ttysavefd = -1;
static enum 
{ 
  RESET, RAW, CBREAK 
} ttystate = RESET;


int
set_tty_raw(void) 
{
  int i;
  if (kbhit_init eq 0) {
    i = tcgetattr (STDIN_FILENO, &termattr);
    if (i < 0) 
    {
      printf("tcgetattr() returned %d for fildes=%d\n",i,STDIN_FILENO); 
      perror ("");
      return -1;
    }
    save_termattr = termattr;

    termattr.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
    termattr.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    termattr.c_cflag &= ~(CSIZE | PARENB);
    termattr.c_cflag |= CS8;
    termattr.c_oflag &= ~(OPOST);
   
    termattr.c_cc[VMIN] = 1;  /* or 0 for some Unices;  see note 1 */
    termattr.c_cc[VTIME] = 0;
    V_CHK(printf("tty_raw init complete\n");)
    kbhit_init++;
    }
  i = tcsetattr (STDIN_FILENO, TCSANOW, &termattr);
  if (i < 0) 
  {
    printf("tcsetattr() returned %d for fildes=%d\n",i,STDIN_FILENO); 
    perror("");
    return -1;
  }
   
  ttystate = RAW;
  ttysavefd = STDIN_FILENO;
  V_CHK(printf("tty set raw\r\n");)
  return 0;
}

/* ***************************************************************************
 *
 * set_tty_cbreak(), put the user's TTY in cbreak mode.
 * returns 0 on success, -1 on failure.
 *
 *************************************************************************** */
int 
set_tty_cbreak() 
{
  int i;

  i = tcgetattr (STDIN_FILENO, &termattr);
  if (i < 0) 
  {
    printf("tcgetattr() returned %d for fildes=%d\n",i,STDIN_FILENO); 
    perror ("");
    return -1;
  }

  save_termattr = termattr;

  termattr.c_lflag &= ~(ECHO | ICANON);
  termattr.c_cc[VMIN] = 1;
  termattr.c_cc[VTIME] = 0;
      
  i = tcsetattr (STDIN_FILENO, TCSANOW, &termattr);
  if (i < 0) 
  {
    printf("tcsetattr() returned %d for fildes=%d\n",i,STDIN_FILENO); 
    perror ("");
    return -1;
  }
  ttystate = CBREAK;
  ttysavefd = STDIN_FILENO;

  return 0;
}

/* ***************************************************************************
 *
 * set_tty_cooked(), restore normal TTY mode. Very important to call
 *   the function before exiting else the TTY won't be too usable.
 * returns 0 on success, -1 on failure.
 *
 *************************************************************************** */
int
set_tty_cooked() 
{
  int i;
  if (ttystate != CBREAK && ttystate != RAW) 
  {
     printf("set_tty_cook: not raw\r\n");
     return 0;
  }
  i = tcsetattr (STDIN_FILENO, TCSAFLUSH, &save_termattr);
  if (i < 0) 
  {
    return -1;
  }
  ttystate = RESET;
  V_CHK(printf("tty cooked\n\r");)
  return 0;
}

