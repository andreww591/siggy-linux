
//#include <stdio.h>
//#include <sys/socket.h>
//#include <netinet/in.h>
//#include <arpa/inet.h>
#include <netdb.h>           //pin AF_INET etc
#include "idlop.h"
//#include <termios.h>
//#include <sys/time.h>
//#include <sys/types.h>
//#include <sys/stat.h>
#include <fcntl.h>       //O_RDONLY etc
//#include <unistd.h>
#include <assert.h>    // assert
#include <stdlib.h>    //(exit)
#include <string.h>    //(strlen)
#include <pthread.h>
#include "mv.h"
#include "argcheck.h"
//#include <linux/kd.h>
//#include <linux/keyboard.h>

#define errif(x,y)   if (x) {set_tty_cooked();perror(y);exit(1);}

char * host_name = "127.0.0.1"; // local host
int port = 51664;
int keyboard,vscreen,pipei,pipeo;
int cntc=0;
int socket_id;
char * ttab[]={"","DTMD","WYSE","VT100","Z-29"};
enum ttype{ NONE, DTMD, WYSE, VT100, Z_29};
int term=DTMD;       //default terminal type
void vterm_kbd(int k);
void vterm_init();
void vterm_write(char d);


int sendchar(char k)
{
      int rc;
      rc=send(socket_id, &k, 1, 0);
//      outputx(k);
      if (!cntc) if(k eq 3) {set_tty_cooked(); exit(0);}
      errif((rc eq -1),"Error in send\n")
}

int sendline( char * str)
{
//      char c;
//      char * p =str;
//      while (c=*p++) sendchar(c);
//      sendchar('\n');
        send(socket_id,str,strlen(str),0);

}


void * keyboardin(void *func)
{
      int rc;
      unsigned char kchar;
      vterm_init();
      set_tty_raw();
//      rc=ioctl ( keyboard, KDSKBMODE, K_MEDIUMRAW );
//      perror("iocntl");
      while (1) {
	    rc=read(keyboard,&kchar,1);
	    if(rc==1) vterm_kbd(kchar);
            else if(rc==0) exit(0);
       }
}

void putch(char k)
{write(vscreen,&k,1);}

argtab table[]={
     {'h',&host_name,String,"Host name or number                          default 127.0.0.1"},
     {'p',&port,Int,        "Host port number                             default 51664"},
     {'t',&term,Int,        "Term type (1-4 DTMD,WYSE,VT100,Z-29)         default 1 (DTMD)"},
     {'c',&cntc,Switch,     "Control-C transmitted (doesn't exit program) default off"},
     {0,0,Last}};


int main(int argc, char *argv[]) {
  char buf[8192];
  int i,rc;
  struct sockaddr_in pin;
  struct hostent *server_host_name;
  char c;
  struct termios;   // tio;  
  int osz;
  char outbound_char,inbound_char;
  pthread_t pidk;
//  if (argc > 1) host_name = argv[1];
      if (argc lt 2) then begin
        argusage(argv[0],table);
        exit (0);
        endif
  argcheck(argc,argv,table);

  server_host_name = gethostbyname(host_name);
  errif( (server_host_name == 0),"Error resolving host name\n");


  bzero(&pin, sizeof(pin));
  pin.sin_family = AF_INET;
  pin.sin_addr.s_addr = htonl(INADDR_ANY);
  pin.sin_addr.s_addr = ((struct in_addr *)(server_host_name->h_addr))->s_addr;
  pin.sin_port = htons(port);

  socket_id = socket(AF_INET, SOCK_STREAM, 0);
  errif((socket_id eq -1),"Error creating socket\n");

  rc=connect(socket_id, (void *)&pin, sizeof(pin));
  errif((rc eq -1),"Error connecting to socket\n");
   vterm_init();
   keyboard = open("/dev/tty",O_RDONLY);
   assert(keyboard>=0);
   vscreen   = open("/dev/tty",O_WRONLY| O_NONBLOCK);
   assert(vscreen>=0);
   pipeo=socket_id;
   pipei=socket_id;
   sendline(ttab[term]);
//   printf("terminal type = %s\n",ttab[term]);
   pthread_create(&pidk, NULL, keyboardin, NULL);
   while(1) {
            rc=recv(socket_id, buf, sizeof(buf), 0);
            errif((rc eq -1),"Error in receiving response from server\n");
            osz=rc;
            if(osz eq 0) exit(0);
            for (i=0;i<osz;i++) { //  buf[i]&=0x7f;}
//	    rc=write(vscreen,&buf,osz);
            vterm_write(buf[i]);
//             putch(buf[i] & 0x7f);
            }
   }
  close(socket_id);
}



#include "gkeys.h"
#include "asc_cnt.h"
char *** pttkxl;//=ttkxl;

#define c(x,y) case OCT(x) : goto y;
#define OCT(x) 0##x
#define l(x,y) case x: goto y;

#define VGAADDR 0xb8000UL
#define VIDADDR(r,c) vmem+(2*((((r)-1) * m->info.screenwidth) + ((c-1))))
#define ROW (m->info.cury)
#define COL (m->info.curx)
#define MXCOL (m->info.winright)
#define MXROW (m->info.winbottom)
#define LEFT (m->info.winleft)
#define TOP (m->info.wintop)
#define ATR (m->attr<<8)

struct text_info {
    unsigned char winleft;
    unsigned char wintop;
    unsigned char winright;
    unsigned char winbottom;
    unsigned char attribute;
    unsigned char normattr;
    unsigned char currmode;
    unsigned char screenheight;
    unsigned char screenwidth;
    unsigned char curx;
    unsigned char cury;
};

typedef struct tag_screen {
    struct text_info info;
    char buf[132*50*2];
    int state;          //parse state
    char wrapped;
    char attr;
    char tstate;        //term state (roll, protect on, ins char, etc)
    char leds;
} screen;

    screen *m;

int vidport;
#define LIGHTGRAY 0
#define BROWN  0
#define BLACK  0
#define GREEN  0
#define C80  0
#define C4350  0

struct text_info tinfo[]={
          /*null*/          {1,1,80,25,7,LIGHTGRAY,C80,24,80,1,1},
          /*dtmd*/          {1,1,80,25,7,LIGHTGRAY,C80,24,80,1,1},
          /*wyse*/          {1,1,80,25,7,BROWN,C80,24,80,1,1},
          /*vt100*/         {1,1,132,25,6,BLACK+(LIGHTGRAY<<4),C80,24,80,1,1},
          /*Z_29*/          {1,1,132,49,6,BROWN,C4350,49,132,1,1},
          /*tekt*/          {1,1,132,25,6,GREEN+(BLACK<<4),C80,24,80,1,1}
                          };


void vterm_write(char d)
{   int i,vmem,ds,_dos_ds;
//    int term=altf_ttype[usr+1];
    short int k=d and 0x7f;

//    m=screens[usr];
//    if (usr eq user) {ds=_dos_ds; vmem=VGAADDR;}    //on screen
//        else {ds=my_ds; vmem=(int) (&(m->buf));}       //memory only

//      putch(k);fflush(stdout);
//      return;
      switch (term) {
        case 0:
        case DTMD: goto dm_state_chk;
        case WYSE: goto wyse_state_chk;
        case VT100: goto vt_state_chk;
        case Z_29:  goto z_29_state_chk;
        default:  putch(k);fflush(stdout);
      endswitch         // term switch
    return;
wyse_state_chk:
    switch (m->state) {
        l(0,wyse_state0) l(3,wyse_escseq) l(1,wyse_y) l(2,wyse_x)
        default: m->state=0;}
    goto done;
wyse_escseq: m->state=0;
  switch (k) begin              //wyse esc seq
     l('B',delch) l('C',fs) l('G',readcur) l('H',home)
     l('L',xmit) l('M',reset) l('N',normi) l('O',dimi)
     l('P',instoggle) l('U',kbdon)
     l('V',rollon) l('W',rolloff)  l(']',printpage)
     l('^',proton) l('_',protoff)
     l('=',rs) l('+',ff) l('E',insln) l('R',delln) l('T',eeol) l('Y',eeos)
     default: goto print;  //invalid esc, char is displayed
     endswitch
   goto done;
vt_state_chk:
    switch (m->state) {
        l(0,vt_state0)
        default: m->state=0;}
    goto done;
z_29_state_chk:
    switch (m->state) {
        l(0,z_29_state0)
        default: m->state=0;}
    goto done;
wyse_state0:
   switch (k) {
     c(NUL,nop) c(BS,bs) c(HT,ht) c(LF,lf) c(FF,fs) c(CR,cr) c(ESC,esc)
     c(FS,fs) c(GS,eeol) c(RS,rs) c(VT,us) c(RUBOUT,nop)
     default:   goto print;}
   goto done;
vt_state0:
   switch (k) {
     c(NUL,nop) c(BS,bs) c(HT,ht) c(LF,lf) c(FF,ff) c(CR,cr) c(ESC,esc)
     c(FS,fs) c(GS,eeol) c(RS,rs) c(US,us) c(RUBOUT,nop)
     default:   goto print;}
   goto done;
z_29_state0:
   switch (k) {
     c(NUL,nop) c(BS,bs) c(HT,ht) c(LF,lf) c(FF,ff) c(CR,cr) c(ESC,esc)
     c(FS,fs) c(GS,eeol) c(RS,rs) c(US,us) c(RUBOUT,nop)
     default:   goto print;}
   goto done;
dm_state_chk:
    switch (m->state) {
        l(0,dm_state0) l(1,dm_cursex) l(2,dm_cursey) l(3,dm_escseq)
        default: m->state=0; }
    goto done;
dm_state0:
   switch (k) {
     c(NUL,nop) c(BS,bs) c(HT,ht) c(LF,lf) c(FF,ff) c(CR,cr) c(ESC,esc)
     c(FS,fs) c(GS,eeol) c(RS,rs) c(US,us) c(RUBOUT,nop)
     default:   goto print;}
   goto done;
dm_escseq: m->state=0;
  switch (k) begin              //datamedia esc seq
     l('A',us) l('B',delch) l('C',fs) l('G',readcur) l('H',home) l('J',eeos)
     l('K',eeol) l('L',xmit) l('M',reset) l('N',normi) l('O',dimi)
     l('P',instoggle) l('R',insch) l('T',xmitline) l('U',kbdon)
     l('V',rollon) l('W',rolloff) l('Y',rs) l(']',printpage)
     l('^',proton) l('_',protoff)
     default: goto print;  //invalid esc, char is displayed
     endswitch
   goto done;
wyse_x: m->state=0; k-=31;
         if (k gt MXCOL) k=MXCOL; if(k lt LEFT) k=LEFT; COL=k;
         goto setcrs;           // cursor y,x x char
dm_cursex: k-=31; m->state++;
         if (k gt MXCOL) k=MXCOL; if(k lt LEFT) k=LEFT; COL=k;
         goto done;        // cursor x,y x char
wyse_y: k-=31; if (k gt MXROW) k=MXROW; if(k lt TOP) k=TOP; ROW=k;
        m->state++; goto done;   // cursor y,x y char
dm_cursey: k-=31; if (k gt MXROW) k=MXROW; if(k lt TOP) k=TOP; ROW=k;
        m->state=0; goto setcrs;      // cursor x,y y char
readcur:
eeos:  write(vscreen,&"\e[J",3); goto done;
xmit:
reset:
instoggle:
xmitline:
kbdon:
rollon:
rolloff:
printpage:
proton:write(vscreen,&"\e[22m",5); goto done; 
protoff:write(vscreen,&"\e[1m",4); goto done; 
normi:write(vscreen,&"\e[0m",4); goto done; 
dimi: write(vscreen,&"\e[2m",4); goto done; 
    goto done;
insln:  write(vscreen,&"\e[L",3); goto done; //fsmovwr(ds,VIDADDR(ROW,LEFT),VIDADDR(ROW+1,LEFT),  //insert line with downscroll below
     //       m->info.screenwidth*(MXROW-ROW-1));
        //fsstosw(ATR,ds,VIDADDR(ROW,LEFT),m->info.screenwidth);
        goto done;
delln:  write(vscreen,&"\e[M",3); goto done;  //fsmovw(ds,VIDADDR(ROW+1,LEFT),VIDADDR(ROW,LEFT),  //delete line with upscroll below
        //      (MXROW-ROW-1)*m->info.screenwidth);
        //fsstosw(ATR,ds,VIDADDR(MXROW-1,LEFT),m->info.screenwidth);
        goto done;
 //insch and delch need to quit at end of unprotected fields    **fix me**
insch:  //fsmovwr(ds,VIDADDR(ROW,COL),VIDADDR(ROW,COL+1),MXCOL-COL);
        //_farpokeb(ds,VIDADDR(ROW,COL),0); 
        goto done;
delch:  write(vscreen,&"\e[P",3); goto done; //fsmovw(ds,VIDADDR(ROW,COL+1),VIDADDR(ROW,COL),MXCOL-COL);
        //_farpokeb(ds,VIDADDR(ROW,MXCOL),0); 
        goto done;
nop:    goto done;
bs:     //if (--COL lt LEFT) COL=LEFT; goto setcrs;
cr:     //COL=LEFT;  goto setcrs;
        putch(k);return;
eeol:   //fsstosw(ATR,ds,VIDADDR(ROW,COL),(MXCOL-COL));
        write(vscreen,&"\e[K",3);
        goto done;
esc:    m->state=3; goto done;
fs:     COL++; goto setcrs;
ff:     write(vscreen,&"\e[2J",4);    //fsstosw(ATR,ds,VIDADDR(TOP,LEFT),m->info.screenwidth*(MXROW-TOP));
home:   // ff falls into here!
        ROW=TOP; COL=LEFT; goto setcrs;
ht:     COL=1+((COL-1+8)/8)*8; goto chkrc;
lf:     //if (m->wrapped eq 0) ROW++; m->wrapped=0; goto chkrc;
        putch(k);return;
rs:     m->state=1; m->wrapped=0; goto done;       //RS (datamedia cursor x,y)
print:  m->wrapped=0;
        //_farpokew(ds,VIDADDR(ROW,(COL++)),ATR|k); 
        putch(k);return;
        goto chkrc;
us:     m->wrapped=0; ROW--; if (ROW lt TOP) begin
            ROW=TOP; goto insln;
            endif
        goto setcrs;
chkrc:  if (COL gt MXCOL) {COL=LEFT; ROW++; m->wrapped=1;}
        if (ROW ge MXROW) begin       //scroll_screen(usr);
     //       fsmovw(ds,VIDADDR(TOP+1,LEFT),VIDADDR(TOP,LEFT),
     //           m->info.screenwidth*(MXROW-TOP-1));
     //       fsstosw(ATR,ds,VIDADDR(MXROW-1,LEFT),m->info.screenwidth);
            ROW=MXROW-1;
            endif
setcrs: if (1) begin   // was ScreenSetCursor(ROW-1,COL-1);
     //       int cur=(ROW-1)*m->info.screenwidth+COL-1;
     //       _farpokew(_dos_ds,0x450,((ROW-1)<<8)|(COL-1));
     //       outp(vidport,0x0f); outp(vidport+1,cur and 0xff);
     //       outp(vidport,0x0e); outp(vidport+1,cur >> 8);
            char buf[20];
            int len;
            sprintf(buf,"\e[%d;%dH",ROW,COL);
            len=strlen(buf);
            write(vscreen,&buf,len);
            endif
done:   return;
}

//#define k(x,y) [K_##x]= #y
#define k(x,y) {K_##x, QF(y)}
#define ka(x,y) {K_Alt_##x, #y}
#define ks(x,y) {K_Shift_##x, #y}
#define kc(x,y) {K_Control_##x, #y}
#define QF(x) #x


typedef struct {unsigned short int sc; void * x;} keymape;
keymape  keystr[]={
    {0,0},                                //end of null terminal   begin DTMD
    k(BackSpace,\RUBOUT), kc(Backspace,\b), ka(Backspace,\b),
    k(F9,\SI), k(F10,\SO),
    k(Up,\US), k(Left,\BS), k(Right,\FS), k(Down,\LF), k(Home,\EM),
    k(Insert,\en), k(Delete,\RUBOUT),
//    eeol       eeos         clear
// {0x??,0x1d}, {0x??,0x0b}, {0x??,0x0c},
    {0,0},                                //end DTMD               begin WYSE
    k(BackSpace,\RUBOUT), kc(Backspace,\b), ka(Backspace,\b),
    k(F9,\SI), k(F10,\SO),
    k(Up,\VT), k(Left,\BS), k(Right,\FF), k(Down,\LF), k(Home,\RS),
    k(Insert,\eQ), k(Delete,\RUBOUT), k(End,\eT), k(PageDown,\eK), k(PageUp,\eL),
    {0,0},                                //end WYSE               begin VT100
    k(BackSpace,\RUBOUT), kc(Backspace,\b), ka(Backspace,\b),
    k(F9,\SI), k(F10,\SO),
    k(Up,\eA), k(Left,\eB), k(Right,\eC), k(Down,\eD),
    {0,0},                                //end VT100              begin Z_29
    k(BackSpace,\RUBOUT), kc(Backspace,\b), ka(Backspace,\b),
    k(F9,\SI), k(F10,\SO),
    k(Up,\eA), k(Left,\eB), k(Right,\eC), k(Down,\eD),
                                          //end Z_29
    };


void vterm_init()
{   int i,mxss=51*132;
//    hword * cc;
//    my_ds=_go32_my_ds();
//    vidport=_farpeekw(_dos_ds,0x0463);
    pttkxl=(char ***) malloc(4*nel(ttab));   //allocate pointers to each keytable
    foreach(ttab,i)
        pttkxl[i]=(char **)malloc(660*4);   //allocate a key xlate table for each terminal type
        memset(pttkxl[i],0,660*4);
        endforeach
    i=0;
    foreach (keystr,j)
        int k=keystr[j].sc;
        if(k eq 0 && keystr[j].x eq 0)
            {i++; if (i ge nel(ttab)) {printt("malformed keystr\n");break;}}
            else pttkxl[i][k]=keystr[j].x;
        endforeach
//    screen_init();

            m=malloc(sizeof(screen));
//          outputx(usr); //outputx(altf_ttype[usr+1]);
            memcpy(&(m->info),&tinfo[term],sizeof(struct text_info));
            m->state=0;
            m->wrapped=0;
            m->attr=m->info.normattr;
            m->tstate=0;
            m->leds=0;
            stosw(m->attr<<8,(int) (&(m->buf)),sizeof(m->buf)>>1);


}

void vterm_kbd(int k)
{   char * p;
//    int line=altf_cocln[usr+1];
//    int term=altf_ttype[usr+1];
//     output (k);
    if ( p= pttkxl[term][k] ) begin
        while (*p)
            {sendchar(*p++);}
        endif else if (k le 0x80) sendchar(k);
}
