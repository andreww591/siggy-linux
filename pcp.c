#include "opts.h"
#include "sigma.h"
#include "cpu.h"
#include "sigmaio.h"
#include <stdlib.h>
//#include "screen.h"
#include <stdio.h>
#include <sys/stat.h>
#include "parse.h"
#include "coc.h"
#include "vterm.h"
//#include <process.h>
#include <string.h>
//#include <stdlib.h>

//s6pcp cp;
#undef V_USER
#define V_USER pcpuser
#define printb(v,nb) {int i; for (i=0;i<nb;i++){if(((nb-i)&3)==0 ) printf(" ");printxv((((v)>>(nb-1-i)) & 1))} }
#define display(n,v,nb) printf(n); printb(v,nb); NEWLINE

#include "rglobals.h"
#include "rvolatiles.h"

#undef RETURN
#define RETURN {set_tty_raw(); start_clock();} return

int str2num(char * str, word * num,word radix)
{   char *pstr=str;
    word i,val,res=0;
    word isgn=1;
//    int radix=10;
    if (*pstr eq '-') then begin
        isgn=-1;
        pstr++;
        endif
    if (*pstr eq '.') then begin
        radix=16;
        pstr++;
        endif else if (*pstr eq '0') then begin
            pstr++;
            if ((*pstr eq 'x') || (*pstr eq 'X')) then begin
                radix=16;
                pstr++;
                endif
            endif
    for (i=0; *pstr != 0 ; i++, pstr++) do begin
        switch (*pstr) begin
            case '0' ... '9': val=(*pstr)-'0';break;
            case 'A' ... 'F': val=(*pstr)-'A'+10;break;
            case 'a' ... 'f': val=(*pstr)-'a'+10;break;
            default: return -1;
            endswitch
        if (val ge radix) then begin
            if (i le 1) then radix=16;
                else return -1;
            endif
        res=res*radix+val;
        endfor
    *num=isgn*res;
    return 0;
}
str2dev(char * str, devdata ** pdev)
{   word i,iopaddr,devaddr,dcaddr;
    DCdata *DC;
    iopdata *iop;
    devdata *dev;
    DEVNAMES
    int ndev=sizeof(devname)/sizeof(char *);
    if (strlen(str) ne 5) begin
        if (str2num(str,&devaddr,16)) return -1;
        dev=(devdata *) find_ua(devaddr);
        goto devv;
        endif
    for (i=0;i<ndev;i++) do begin
        if (strncmp(devname[i],str,2) eq 0) then goto found;
        endfor
    return -1;  //invalid type, not in table
found:
    iopaddr=(*(str+2))-'A';
    if(iopaddr lt 0) then return -1; // invalid iop letter
    devaddr=bin(*(str+4)) and 0xf;
    dcaddr=bin(*(str+3)) and 7;
    if (!(bin(*(str+3)) and 8)) then begin
        dcaddr=16*dcaddr+devaddr;
        devaddr=dcaddr;
        endif
    iop=find_iop(iopaddr);
    if (iop eq NULL) then return -1;
    DC=find_DC(iop,dcaddr);
    if (DC eq NULL) then return -1;
    dev=find_dev(DC,devaddr);
devv:
    if (dev eq NULL) then return -1;
    *pdev=dev;
    return 0;   //found device
}




pcp(s6pcp *cp,sigma6 *s6arg)
{
#define s6 (*s6arg)
    char *dargs[]={"command",0};
//  byte *reg; //local register pointer (SIGGY knows to care for it)
    ACCESSES_REGISTERS
    psd s6psd; //local temp copy only for psw1/psw2!
    int olduser,num[20],ii,radix,__x ;
    struct stat statbuf;
    ACCESSES_MEMORY
    int steps=0;
//    enum routines {Eq,Dump};
    enum parmtyp {Hex,Int,String,Device,Fid};
    enum parmtyp argt;
    typedef struct {
        int num;
        enum parmtyp arg[10];
        } parmlist;
    typedef struct {
        char * cmd;
        void * rout;
        parmlist args;
        void * varpnt;
        } command;
    
    parse_table ptab={ " \t", "\n+-*/,=<>",  "'\"",  '\\'};

    command ctab[]={
        {"?",&&help},
        {"ac",&&access,{2,Hex,Hex}},
        {"addstop",&&addstop},
        {"audio",&&audio},
        {"b",&&brk,{1,Hex}},
        {"boot",&&boot,{1,Hex}},
        {"branch",&&branch},
        {"clock",&&clock},
        {"coc",&&coc},
        {"coms",&&coms},
        {"cpu",&&cpu},
        {"cpureset",&&cpureset},
        {"dev",&&device,{1,Device}},
        {"device",&&device,{1,Device}},
        {"devices",&&devices},
//        {"dos",&&dos},
        {"d",&&dump,{2,Hex,Hex}},
        {"dump",&&dump,{2,Hex,Hex}},
        {"dumpba",&&dumpba,{2,Hex,Hex}},
        {"dumpda",&&dumpda,{2,Hex,Hex}},
        {"dv",&&dv,{1,Device}},
        {"end",&&quit},
        {"event",&&evnt},
        {"fpos",&&fpos,{1,Device}},
        {"iai",&&iai},
        {"iexist",&&iexist,{2,Int,Hex}},
        {"interrupts",&&interrupts},
        {"ioreset",&&ioreset},
        {"iostop",&&iostop,{2,Device,Int}},
        {"help",&&help},
        {"load",&&load},
        {"map",&&map,{2,Hex,Hex}},	
        {"memclear",&&memclear},
        {"moun",&&mount,{2,Device,Fid}},
        {"n",&&step},
        {"noclock",&&noclock},
        {"nopcp",&&nopcp},
        {"pcp",&&pcp},
        {"power",&&power},
        {"psd",&&psd},
        {"psw1",&&psw1,{1,Hex}},          // must solve reg (reg pointer) problem first
        {"psw2",&&psw2,{1,Hex}},
        {"quit",&&quit},
        {"q",&&quit},
        {"regs",&&regs},
        {"r",&&run},
        {"reboot",&&reboot},
        {"rew",&&rew,{1,Hex}},
        {"rewind",&&rewind,{1,Device}},
        {"run",&&run},
//        {"scroll",&&scroll},
        {"search",&&search,{2,Hex}},
        {"seladd",&&seladd,{1,Hex}},
        {"sense",&&sense,{1,Hex}},
        {"show",&&show},
        {"s",&&step},
        {"ss",&&ss},              //smart step
        {"step",&&stepm,{1,Int}},
        {"store",&&store,{2,Hex,Hex}},
        {"sysreset",&&sysreset},
        {"wtask",&&wtask},
        {"tics",&&tics},
        {"user",&&user,{1,Int}},
        {"verbose",&&verbose,{1,Hex}},
        {"watchdog",&&watchdog},
        {"wl",&&writelocks,{2,Hex,Hex}}
        };        //previous line gets no trailing comma
    int ncmds=sizeof(ctab)/sizeof(command);
    char cmdbuf[120],token[120];
    int i,next;
    char delim,quoted;
    int k;
    olduser=user;
    if (showcpu eq 0) showscreen(pcpuser);
    set_tty_cooked();
    stop_clock();
//    set_tty_cooked();
next:
    if (pcp_flag and step_flag) begin
        if (cp->nopcp) pcp_show(s6arg); else pcp_display(cp,s6arg);
        endif
getkbd:
    CLEAR(pcp_flag,cntrl_c_flag or step_flag or rec_flag)
    if(cp->idle) begin
        vterm_printf(user," \n\rpcpw-");
        fgets(cmdbuf,120,stdin);
        usleep(10000);  // .01 sec  to allow other thread to process character
        if(kbhit()) begin   //if other thread caught first character
             int ii;
             int k1=getkey(); 
//             printf("getkey caught %c\n",k1);
             for (ii=120-1;ii>0;ii--) cmdbuf[ii]=cmdbuf[ii-1];
             cmdbuf[0]=k1;
             endif
     endif else 
         if (cbl) strcpy(cmdbuf,pcmdbuf);
         else {vterm_printf(user," \rpcp-"); /* clreol(); */ RETURN 0;}
    cbii=cbl=0;
    i=next=0;
    parse(0,token,120,cmdbuf,&ptab, &delim,&next,&quoted);
    if (strlen(token) eq 0) goto getkbd;
    vterm_printf(user,"\n");      //compensate for fgets line
    for (k=0; k<ncmds; k++) do begin
//      printt("comparing ") printt((ctab[k]).cmd) printt(" and ") printt(token) NEWLINE
        if (strcasecmp((ctab[k]).cmd,token) eq 0) then begin  
            for (ii=0; ii <(ctab[k]).args.num; ii++) begin
//              printt("checking parameter ii ") printd(ii) NEWLINE
                if (delim eq '\r') then begin
                    printt("Parameter missing: ") printd(ii+1) NEWLINE
                    goto getkbd;
                    endif
                if (parse(0,token,120,cmdbuf,&ptab,&delim,&next,&quoted) eq 0)
                    begin 
                switch (argt=(ctab[k]).args.arg[ii]) {
                    case Hex:
                    case Int:
                        radix=(argt eq Hex) ? 16:10;
                        if (str2num(token,&num[ii],radix)) then begin
                            printt("Invalid parameter value: ") printt(token) NEWLINE
                            goto getkbd;
                            endif break;
                    case Device:
//                        strupr(token);
                        if (str2dev(token,(devdata**) (&num[ii]))) then begin
                            printt("Parameter ") printd(ii) printt(": invalid device name") NEWLINE
                            goto getkbd;
                            endif break;
                    case Fid:
//                      if (stat(token,&statbuf)) then begin
//                          printt("File not found") NEWLINE goto getkbd; endif
                        num[ii]=(int) strdup(token); break;
                    default:
                        printt("Parse routine not implemented for requested arg type") NEWLINE
                        goto getkbd;

                    endswitch
                  endif else begin
                            printt("Parameter missing: ") printd(ii+1) NEWLINE
                            goto getkbd;
                            endelse;
                endfor
            goto *((ctab[k]).rout);
            endif
        endfor
    if (strlen(token) gt 0) printt("command not implemented: ") printt(token) NEWLINE
    goto getkbd;
access:  {word i; for (i=0;i<num[1];i++) {printxv( s6.memac[i])} NEWLINE} goto getkbd;
audio:   cp->sw.audio^=1; goto next;        // toggle audio switch
addstop: cp->sw.address_stop^=1; printt("addstop set to") printo(cp->sw.address_stop) NEWLINE goto next; // toggle address stop switch
boot:    cp->unit_address=num[0] and 0x7ff; goto next;
branch:  br_count &=0xf; {for (k=0; k<16; k++) do begin 
         printx(br_stack[(br_count+1+k)&0xf]) NEWLINE endfor;} goto getkbd;
brk:   cp->sw.address_stop=1;cp->sw.select_address=num[0]&M17;goto getkbd;
clock:   activate_clock(); goto getkbd;
coc:     {word i,L; for (i=0;i<16;i++) {if (cocs[i]) {cocdata * c=cocs[i];
         devdata * dev=c->dev; printt(dev->name) NEWLINE
         printt(" run = ") printx(c->run[0]) printx(c->run[1])
         printt(" xscanhold = ") printx(c->xscanhold[0]) printx(c->xscanhold[1])
         NEWLINE printt(" dsr lines =") for (L=0;L<64;L++) {if (c->dsr[L]) {
         printd(L)}} NEWLINE
         }}} goto getkbd;
coms:    {word i,j,k,m,p,nn;
             for (i=0;i<8;i++) begin
                 j=comx[i];
                 if(j gt 0) {printf("irq%ld:   count=%ld",i,irqs[i+1]);
                    while(j) {NEWLINE
                       p=comport[j];
                       switch (ptype[j]) {
                        case 3:
                         printf("  lpt%ld(%lx) count=%ld regs=",j-4,p,icoms[j]);
                         for(k=0;k<4;k++){printxv(inp(p+k)) printt(";")}
                         printf(" strobe phase = %d",sphase[j]);break;
                        case 1:
                         printf("  com%ld(%lx) count=%ld regs=",j,p,icoms[j]);
                         for(k=0;k<8;k++){printxv(inp(p+k)) printt(";")}
                         //add mcr,lcr,com_modemstat,com_linestat
                         break;
                        case 2:
                         for (nn=0;nn<16;nn++) {
                          if(ptype[j+nn] ne 2) break;
                          printf("\n  boca%ld(%lx) count=%ld regs=",nn,p+8*nn,icoms[j+nn]);
                          for(k=0;k<8;k++){printxv(inp(p+k+8*nn)) printt(";")}
                         }
                        }
                       j=comxl[j];}
                    NEWLINE}
                 endfor
             goto getkbd;}
cpu:     showcpu=1;showscreen(olduser); goto getkbd;
cpureset: CPURESET(s6,s6psd); goto getkbd;
device:  {devdata * dev=(devdata*)num[0]; outputvxnl(dev->TDV_status);
         outputvxnl(dev->busy); printt(" fid = ") printt(dev->fid)
         NEWLINE outputvxnl(dev->nextblock); outputvxnl(dev->lastblock);
         outputvxnl(dev->fpos); outputnl(dev->ars);
         printt(" orp = ") printd(dev->orp) printt(" rec# = ") printd(dev->recno)
         if (dev->HOSTtype) then begin printt(" file# = ") printd(dev->fileno) endif
         NEWLINE if (dev->ssize) {printt(" sense = ") printxv(*((word*)(dev->sense)))}
         {DCdata * dc=dev->parent;iopdata * iop=dc->parent;int chan=dc->address;
         printt(" dev/DC: TSH_status = ")
         printxv(dev->TSH_status) printt("/") printxv(dc->TSH_status)
         printt(" IOPstatus = ") printxv(iop->IOPstatus[chan])
         printt(" order = ") printxv(dc->command) printt(" multi = ") printd(dc->multi) printt(" busy = ")
         printxv(dc->busy) NEWLINE
         printt(" AIOstatus = ") printx(dev->AIOstatus)
         printt(" dev pbit = ") printxv(dev->pbit) printt(" DC pbit = ") printxv(dc->pbit)
         printt(" DC ic = ") printxv(dc->ic) printt(" iop ic = ") printxv(iop->ic)
         NEWLINE outputvx( iop->cda[chan]*2);
          }NEWLINE goto getkbd;}
dv:     {devdata *dev=(devdata*)num[0]; dev->verbose^=1; printt(dev->name)
        printt(" verbose set to ") printo(dev->verbose) NEWLINE} goto getkbd;
devices: showiocfg(); goto getkbd;
//dos:     showscreen(0); spawnvp(P_WAIT,"command.com",dargs); showscreen(pcpuser); goto getkbd;
dump:    dump(num[0] ,(num[0]+num[1]>sz) ? sz-num[0] : num[1]); goto getkbd;
dumpba:  dump(num[0]/4 ,num[1]/4+2); goto getkbd;
dumpda:  dump(num[0]<<1 ,num[1]*2); goto getkbd;
evnt:    printt("Event = ") printx(event) NEWLINE goto getkbd;
fpos:    {devdata * dev=(devdata*)num[0]; if (num[0]) {
         printt("File position = ") printxv(ftell((FILE *) dev->HOST)) NEWLINE }} goto getkbd;
help:    for (k=0; k<ncmds; k++) do begin   // dump table of commands
         printt((ctab[k]).cmd) printt("\t") endfor; NEWLINE goto getkbd;
iai:     s6.ia++;s6.ia &= M17; goto show;
iexist:  intr[num[0] and 0xf].exists=num[1]; goto getkbd;
interrupts: {word i; printt("\nGroup\texists\tarmed\tenabled\ttriggered\twaiting\tactive\tpbit\tgrp") NEWLINE
            for (i=0;i<16;i++) do begin
         /* if (intr[i].exists) then */begin
                printf("%d\t%.4X\t%.4X\t%.4X\t%.4X\t\t%.4X\t%.4X\t%.4X\t%.4X",i,intr[i].exists,
                intr[i].armed,intr[i].enabled,intr[i].triggered,
                intr[i].waiting,intr[i].active,intr[i].pbit,intr[i].grp);NEWLINE endif
            endfor
            outputxnl(ewaiting.w); outputxnl(active.w); outputvx(ic);} goto getkbd;
ioreset: IORESET NEWLINE goto next;
iostop:  ((devdata *)num[0])->stop=num[1]; goto getkbd;
load:    LOAD(cp->unit_address,s6); goto next;
map:     {word i; for (i=0;i<num[1];i++) {printxv(((i+num[0])<<9) eor s6.memmap[i+num[0]])} NEWLINE} goto getkbd;
memclear: MEMCLEAR goto next;
mount:   {devdata * dev=(devdata*)num[0]; dev->fid=(char *)malloc(strlen((char *)num[1]));
         strcpy(dev->fid,(char*)num[1]);
         if (dev->HOST ne 0) then fclose((FILE *)dev->HOST); dev->HOST=0;
         if(HOSTdevinit(dev)) then begin printt("Open failed") NEWLINE endif}
         goto getkbd;
noclock: release_clock(); goto getkbd;
nopcp:   cp->nopcp=1; showscreen(pcpuser);goto next;
pcp:     cp->nopcp=0; showscreen(pcpuser);pcp_display(cp,s6arg); goto next;
power:   printt("why? didn't you pay the electric & A/C bill?") NEWLINE; goto getkbd;
psd:     SHOWPSD(s6); goto getkbd;
psw1:    GPSD(s6psd,s6); s6psd.w.w0=num[0]; BPSD(s6,s6psd); goto next;
psw2:    GPSD(s6psd,s6); s6psd.w.w1=num[0]; BPSD(s6,s6psd); goto next; 
quit:    showscreen(0);gotoxy(1,25);NEWLINE;exit(0);
reboot:  HOSTdevrewind((find_ua(cp->unit_address))); MEMCLEAR IORESET CPURESET(s6,s6psd) LOAD(cp->unit_address,s6) goto run;
rew:     HOSTdevrewind((find_ua(num[0]))); goto getkbd;
rewind:  HOSTdevrewind((devdata*)num[0]); goto getkbd;
run:     showscreen(ttyuser);RETURN 0;
//scroll:  scroll=1; goto getkbd;
seladd:  cp->sw.select_address=num[0]&M17; goto getkbd;
sense:   cp->sw.sense=num[0] and 0xf; goto next;
show:    pcp_show(s6arg);  goto getkbd;
ss:      showscreen(olduser);stepuntil=-((int)s6.ia); goto step_return; //smart step
step:    showscreen(olduser);stepuntil=1; goto step_return;   //single step
stepm:   showscreen(olduser);stepuntil=num[0];goto step_return; //single step multiple
regs:    SHOWREGS(s6); NEWLINE goto getkbd;
search:  {word ix=0; while (ix++ lt sz) do begin
                         if ((MEMW(ix) and num[1]) eq num[0]) do begin
                             printxv(ix) printt(" ") printx(MEMW(ix)) NEWLINE
                             endif;
                         endwhile; ix=0;  
                     while (ix++ lt 16) do begin
                         if ((SLOC(reg,ix,word) and num[1]) eq num[0]) do begin
                             printt("R") printxv(ix) printt(" ") printx(SLOC(reg,ix,word)) NEWLINE
                             endif;
                         endwhile;} goto getkbd;
store:    *pRxMEM0(num[0],word)=num[1]; goto getkbd;
sysreset: IORESET CPURESET(s6,s6psd) /*printt("sys reset hit, but switch is broken") NEWLINE;*/ goto getkbd;
tics:    output(tics); goto getkbd;
user:    showscreen(num[0]); goto getkbd;
verbose:  verbose=num[0]; goto getkbd;
watchdog: cp->sw.watchdog^=1; goto next;
writelocks:  {word i; for (i=0;i<num[1];i++) {printxv( s6.wl[i])} NEWLINE} goto getkbd;
wtask:    w_task=-w_task-1; output(w_task); goto getkbd;
step_return: SET(pcp_flag,step_flag) ;RETURN pcp_evt;
trapint: output(s6.t40cc); goto getkbd;
//trap40cc4: printt("t40cc4?"); NEWLINE; goto getkbd;
//trap40cc2: printt("t40cc2?");NEWLINE; goto getkbd;
}
#undef s6

pcp_show(sigma6 *s6a)
{ SHOWREGS((*s6a)); dumpb((*s6a).ia);}

pcp_display(s6pcp *cp,sigma6 *s6a)
#define s6 (*s6a)
{
    ACCESSES_MEMORY
    ACCESSES_REGISTERS
    psd s6psd;
    GPSD(s6psd,s6);
    (*cp).data=*pRxMEM3(s6.ia,word);     //get data lights
trapint:    //ignore a trap while fetching data lights
    NEWLINE printt("     XEROX  SIGMA  6   ") NEWLINE

    printt ("            Watchdog Timer                                     __sense__") NEWLINE
    printt ("                 ") printb(cp->sw.watchdog,1)
    display("                                           ",cp->sw.sense,4)
    printt("                              Unit Address") NEWLINE
    printf("                                   %.3lX\n",(long)cp->unit_address);
    printt("          _w_key_ _i_inh_                                  __pointer__") NEWLINE
    printt("PSW2:  ") printt("     ") printb(s6.wk,2) printt("   ")
            printb(s6.ci,1) printb(s6.ii,1) printb(s6.ei,1)
            printt("                                  ")
            printb(s6.rp,5) NEWLINE
    printt ("         ___cc__  _float_ _s_m_d_a_   ____________INSTRUCTION ADDRESS__________") NEWLINE
    printt("PSW1:  ") printb(s6.cc,4) printt("   ")
            printb(s6.fs,1) printb(s6.fz,1) printb(s6.fn,1) printt(" ")
            printb(s6.ms,1) printb(s6.mm,1) printb(s6.dm,1) printb(s6.am,1)
            printt("       ") printb(s6.ia,17) NEWLINE
    printt ("                           ADDR STOP  ________________SELECT ADDRESS___________") NEWLINE
    printt ("                              ") printb(cp->sw.address_stop,1)
    display("         ",cp->sw.select_address,17)
    printt ("         _______________________________DISPLAY________________________________") NEWLINE
    display("       ",cp->data,32)
#undef s6
}

//note: pcpuser defaults to a dtmd, we use dtmd screen sequences to edit line here
#include "asc_cnt.h"   //control character name definitions
void pcp_charin(k)     //build a command line with local editing
{  int savecmd;
   switch (k) {
     case 0x20 ... 0x7f: if(insmode) begin
                            int i;
                            if(cbl ge SCB-1) cbl--;
                            for (i=cbl;i>cbii;i--)
                                pcmdbuf[i]=pcmdbuf[i-1];
                                vterm_write(1,ESC);vterm_write(1,'R');
                                cbl++;
                            endif
                         pcmdbuf[cbii++]=k;
                         vterm_write(1,k);
                         if(cbii ge SCB) cbii--;
                         if(cbii gt cbl) cbl=cbii;
                         edited++;
                         return;
     case 0x147:              //home key
        {int i; for (i=cbii;i>0;i--) vterm_write(1,BS);}
        cbii=0;
        return;
     case 0x14F:              //end key
        for (;cbii<cbl;cbii++) vterm_write(1,FS);
        return;
     case 0x152:              //insert key
        insmode=1-insmode; return;
     case 0x14b:              //left arrow
        if(cbii) {cbii--; vterm_write(1,BS);} return;  //don't fall through
     case 0x08:               //backspace
        if(cbii) {cbii--; vterm_write(1,BS);} else return;  //fall through
     case 0x153:              //delete key
        {int i; for (i=cbii;i<cbl;i++) pcmdbuf[i]=pcmdbuf[i+1];
            cbl=i;} vterm_write(1,ESC); vterm_write(1,'B');
            edited++; return;
     case 0x14d:              //right arrow
        cbii++; if(cbii gt cbl) cbii=cbl; else vterm_write(1,FS); return;
     case 0x148:              //up arrow  - previous history buffer
        hcmdor= (--hcmdor lt 0) ? ((cbc ge NH) ? NH-1 : 0) : hcmdor;
        if(hcmdor ne hcmdii) goto hist;
        hcmdor++; if(hcmdor ge NH) hcmdor=0;
        return;
     case 0x150:              //down arrow
        if(hcmdor ne hcmdii) {hcmdor++; if(hcmdor ge NH) hcmdor=0;}
hist:   if(hcmdor ne hcmdii) strcpy(pcmdbuf,hcmdbuf+hcmdor*SCB);
            else pcmdbuf[0]=0;
        vterm_printf(1,"\rpcp-%c%s",GS,pcmdbuf);  //dtmd GS is erase to eol
        cbl=cbii=strlen(pcmdbuf);
        edited=0;
        return;
     case 0x0d:               //carriage return
        pcmdbuf[cbl]=0;
     //   if(hcmdii ne hcmdor)
        if (edited) begin
            strcpy(hcmdbuf+hcmdii*SCB,pcmdbuf);
            cbc++;
            hcmdii++; if(hcmdii ge NH) hcmdii=0;
            endif
        hcmdor=hcmdii;
        pcp(&cp,&s6);
//        S_UNLOCK(KB);
//        vterm_printf(2,"\rpcpr-");
//        S_LOCK(KB);
        edited=insmode=0;
        return;
     default: outputx(k);
    }
}


void charin(int usr, int k)
{
    if(usr eq 2) pcp_charin(k);
     else output(usr);
}
