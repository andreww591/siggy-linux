//#define FLTINST(op,typ) TRAP41
#define PREOPm
#define PREOPd if (t2.ft.frac eq 0) {S6CC=4; TRAP44;}
#define PREOPas {word dexp=t2exp-t1exp,guard=(inst>>29) and 1; \
        if (dexp) {if (dexp-guard gt 0) \
                { t1.ft.frac &= -1LL << (t1bp+4*(dexp-guard));} \
                else if (dexp+guard lt 0) \
                {t2.ft.frac &= -1LL << (t2bp-4*(dexp+guard));} }}
#define FLTINST(op,cctyp) {hword maxexp,t1exp,t2exp,t1bp,t2bp; ufl t1,t2,res; \
    SFSTT(R,t1.ft,cctyp,t1exp,t1bp)    \
    SFSTT(*pIEAXw(2),t2.ft,cctyp,t2exp,t2bp)      \
    PREOP##cctyp \
    F##op(res.f,t1.f,t2.f)    \
    TTSF(res.ft,R,cctyp)    \
    BREAK; }
#define FLTINSTL(op,cctyp) {hword maxexp,t1exp,t2exp,t1bp,t2bp; ufl t1,t2,res; \
    SFTT(RRu1,t1.ft,cctyp,t1exp,t1bp)    \
    SFTT(*pIEAXd(2),t2.ft,cctyp,t2exp,t2bp)      \
    PREOP##cctyp \
    F##op(res.f,t1.f,t2.f)    \
    TTSF(res.ft,Rd,cctyp)    \
    BREAK; }
        INST9(0x1C) NODDR;
        INST(0x1C)  FLTINSTL(SUB,as)                  //FSL
        INST9(0x1D) NODDR;
        INST(0x1D)  FLTINSTL(ADD,as)                  //FAL
        INST9(0x1E) NODDR;
        INST(0x1E)  FLTINSTL(DIV,d)                   //FDL
        INST9(0x1F) NODDR;
        INST(0x1F)  FLTINSTL(MUL,m)                   //FML
        INST(0x3C)  FLTINST(SUB,as)                   //FSS
        INST(0x3D)  FLTINST(ADD,as)                   //FAS
        INST(0x3E)  FLTINST(DIV,d)                    //FDS
        INST(0x3F)  FLTINST(MUL,m)                    //FMS
