#include "idlop.h"
#include <stdio.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdlib.h>
#include <X11/Xos.h>
#include <X11/Xfuncs.h>
#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <X11/Xproto.h>
#include <X11/Xutil.h>
#include <X11/Xmu/Error.h>
#include <assert.h>



Display *dpy = NULL;

void set_led( int led_mode)
{
    int led=3;
    XKeyboardControl values;
//    output (dpy);
//    output(led); output(led_mode);

    if (dpy eq NULL) { dpy = XOpenDisplay(NULL);   //  Open display  
           assert(dpy ne NULL);  }
    values.led_mode = led_mode;
    if (led != -1) {
        values.led = led;
        XChangeKeyboardControl(dpy, KBLed | KBLedMode, &values);
    } else {
        XChangeKeyboardControl(dpy, KBLedMode, &values);
    }

    {XKeyboardState val;
    XGetKeyboardControl(dpy, &val);   //dont know why, but this is necessary
//     printf ("LED mask:  %08lx\n", val.led_mask);
    }

    return;
}
#if 0
main()
{   int i=0;
    int led=3;       //scroll lock led
    while(1){
    set_led(i);
    i=1-i;
    sleep(1);
    }    
}

#endif
