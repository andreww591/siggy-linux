#define cmask 0x7fff7fff
#define wcoun 0x00007fff
#define scoun 0x7fff0000
#define pullx 0x00017fff
#define pushx 0x7fff0001
#define wcovf 0x00008000
#define scovf 0x80000000
#define tstw  0x80008000
#define spdm  0x1ffffffffffffLL
#define STKINIT  dword * pd=pIEAXd(0); udw spd; uword ovun,swc; spd.dw=*pd;
#define PUTSPD STS(spd.w.w1,swc,cmask) *pd=spd.dw and spdm;

#define CHKSTKABORT if (ovun=(swc and tstw)) then begin          \
                       if (ovun and not(spd.w.w1)) then TRAP42   \
                       S6CC=((spd.w.w1 and wcoun) eq 0)         \
                          or (((spd.w.w1 and scoun) eq 0)<<2)    \
                          or ((ovun and wcovf)>>14)              \
                          or ((ovun and scovf)>>28);             \
                       BREAK; endif

/*PLW*/  INST(0x08)  { STKINIT
                      swc=(pullx+(spd.w.w1 and cmask)) eor wcovf;
                      CHKSTKABORT
                          else begin
                          R=*pRxMEM2(spd.w.w0,word);
                          spd.w.w0--; spd.w.w0 &= M17;
                          PUTSPD
                          S6CC=((swc and wcoun) eq 0);
                          endelse} BREAK;

/*PSW*/  INST(0x09)  { STKINIT
                     swc=(pushx+(spd.w.w1 and cmask)) eor scovf;
                     CHKSTKABORT
                         else begin
                         *pRxMEM0(++spd.w.w0,word)=R;
                         PUTSPD
                         S6CC=((swc and scoun) eq 0)<<2;
                         endelse} BREAK;

#define MSTKINIT  uword sm; word count=S6CC ? S6CC : 16; word ri=field(inst,r); word * r0=&REGWA(0); STKINIT

/*PLM*/  INST(0x0A)  {MSTKINIT
                     sm= (count<<16)+wcovf-count;
                     swc=(sm+(spd.w.w1 and cmask)) eor wcovf;
                     CHKSTKABORT
                     else begin
                        while (count) do begin
                             word j, *isadd=pRxMEM2(spd.w.w0,word);
//                           word rem=MIN(count,RXMREMw(isadd));
                             word rem=MIN(count,(spd.w.w0 & 15)+1); //this ones backwards
                             for (j=rem;j;j--) r0[-((ri+(--count))&15)]=*isadd++;
                             spd.w.w0-=rem;
                             endwhile
                     PUTSPD
                     S6CC=((swc and wcoun) eq 0);
                     endelse} BREAK;
/*PSM*/  INST(0x0B)  {MSTKINIT
                     sm=((wcovf-count)<<16)+count;
                     swc=(sm+(spd.w.w1 and cmask)) eor scovf;
                     CHKSTKABORT
                     else begin
                     word wc=0;
                     while (count) do begin
                         word j, *idadd=pRxMEM0(spd.w.w0+1,word);
                         word rem=MIN(count,RXMREMw(idadd));
                         for (j=rem;j;j--) *idadd--=r0[-((ri+wc++)&15)];
                         spd.w.w0+=rem; count-=rem;
                         endwhile
                     PUTSPD S6CC=((swc and scoun) eq 0)<<2;
                     endelse} BREAK;
/*MSP*/  INST(0x13)  {uword modifier=R and 0xffff;uword eorx; MSTKINIT
                     sm=((-modifier)<<16) or modifier;
                     eorx=sm and tstw;
                     sm &= cmask;
                     swc=(sm+(spd.w.w1 and cmask)) eor eorx;
                     CHKSTKABORT
                       else begin
                       spd.w.w0+=(hword) modifier;
                       PUTSPD
                       S6CC=(((swc and scoun) eq 0)<<2) | ((swc and wcoun) eq 0);
                       endelse} BREAK;

#define ZR1 ({udw _d;_d.w.w0=0; _d.w.w1=SLOC(reg,1,word);_d.dw;})
#define RRu1z  (field(inst,r) ? RRu1 : ZR1)

           INST5(0x28) TRAP40CC1;
  /*CVS*/  INST(0x28)  {word trem,i,n=0; uword a=R;word b=0;
                        uword *itbl;word tbl=EVA;
                        uword bit=0x80000000; word rem=32;
                        udw r;
                 //       printt("Ru1 = ") printx(ru1) NEWLINE
                        while (rem) do begin
                            itbl=pRxMEM2(tbl,word);
                            n=MIN(rem,REMwRXM(itbl,tbl));
                            for (i=n;i;i--) begin
                                if (*itbl le a) {a-=*itbl; b|=bit;}
                                bit>>=1;
                                itbl--;
                                endfor
                            rem-=n; tbl+=n;
                            endwhile
                        CCt34(b,word);
                        r.w.w0=a; r.w.w1=b;
                        LD(REG(inst),r.dw);
                        }BREAK;

           INST5(0x29) TRAP40CC1;
  /*CVA*/  INST(0x29)  {word trem,i,n=0; word ru1=Ru1;
                        uword *itbl;word tbl=EVA;
                        uword bit=0x80000000; word rem=32;
                        udw r; r.dw=0;
                 //       printt("Ru1 = ") printx(ru1) NEWLINE
                        while (rem) do begin
                            itbl=pRxMEM2(tbl,word);
                            n=MIN(rem,REMwRXM(itbl,tbl));
                            for (i=n;i;i--) begin
                                if (ru1 and bit)  {r.ud+=*itbl;} //printx(*itbl) printt(" ")}
                                bit>>=1;
                                itbl--;
                                endfor
                            rem-=n; tbl+=n;
                            endwhile
                        R=r.w.w1;
                        CCt34(r.w.w1,word);
                        if (r.w.w0) S6CC|=8; else S6CC&=7;
                        }BREAK;

#define BYTESTRINIT(NULLCC) word count,sadd,dadd,fill,rem,i,j,srem,drem;       \
        byte * isadd,* idadd, kk;             \
        word disp=IMMED(inst); word r=REG(inst); udw bsr; bsr.dw=RRu1z; \
        rem=count=((uword) bsr.w.w1)>>24;                      \
        NULLCC                           \
        if (count eq 0) BREAK;                                         \
        sadd=(bsr.w.w0 and M19vx)+disp; dadd=(bsr.w.w1&= M19vx);           \
        if (r) fill=((uword) bsr.w.w0)>>24; else fill=0xff;

#define PUTBSD {bsr.w.w0+=count-rem; bsr.w.w1=dadd;  bsr.w.w0 &= M19vx; LDl(r,bsr.dw);}

           INST5(0x40) TRAP40CC1;
           INST9(0x40) NODDR;
  /*TTBS*/ INST(0x40) NOIND; { byte table[256], *tbl; BYTESTRINIT(;)
                          isadd=pRxMEM2(sadd,byte);
                          srem=RXMREMb(isadd);
                          if (srem lt 256) then begin
                              word trem=256; tbl=table+255;
                              while (trem) do begin
                                  i=MIN(trem,srem);
                                  for (j=i;j;j--) {*tbl--=*isadd--;}
                                  trem-=i;
                                  if (trem) then begin
                                        sadd+=i; isadd=pRxMEM2(sadd,byte);
                                        srem=RXMREMb(isadd); endif
                                  endwhile
                              isadd=table+255;
                              endif
                          S6CC &= 0xE;
                          while(rem) do begin
                              idadd=pRxMEM2(dadd,byte);
                              drem=RXMREMb(idadd);
                              i=MIN(drem,rem);
                              dadd+=i; rem-=i;
                              for (j=i;j;j--,idadd--) begin
                                  if (fill and isadd[-(*idadd)]) then begin
                                      fill &= isadd[-(*idadd)]; S6CC |=1;
                                      dadd-=j; rem=0; break;
                                      endif
                                  endfor
                              endwhile
                          rem=(bsr.w.w1+count-dadd);
                          dadd &=M19vx;  dadd|=rem<<24;
              //          if (r && !(r and 1)) then         // 4/6/98 9 sufx 1d2
                                SLOC(reg,r*4,byte)=fill;
                          SLOC(reg,r|1,word)=dadd;
                          } BREAK;


           INST5(0x41) TRAP40CC1;
           INST9(0x41) NODDR;
  /*TBS*/  INST(0x41) NOIND; { byte table[256], *tbl; BYTESTRINIT(;)
                          isadd=pRxMEM2(sadd,byte);
                          srem=RXMREMb(isadd);
                          if (srem lt 256) then begin
                              word trem=256; tbl=table+255;
                              while (trem) do begin
                                  i=MIN(trem,srem);
                                  for (j=i;j;j--) {*tbl--=*isadd--;}
                                  trem-=i;
                                  if (trem) then begin
                                        sadd+=i; isadd=pRxMEM2(sadd,byte);
                                        srem=RXMREMb(isadd); endif
                                  endwhile
                              isadd=table+255;
                              endif
                          while(rem) do begin
                              idadd=pRxMEM0(dadd,byte);
                              drem=RXMREMb(idadd);
                              i=MIN(drem,rem);
                              for (j=i;j;j--,idadd--) {*idadd=isadd[-(*idadd)];}
                              dadd+=i; rem-=i;
                              endwhile
                          SLOC(reg,r|1,word)=dadd and M19vx;
                          } BREAK;


           INST5(0x60) TRAP40CC1;
  /*CBS*/  INST(0x60) NOIND; { BYTESTRINIT(S6CC &=0xc;)
                      if (r) then begin
                          while(rem) do begin
                              isadd=pRxMEM2(sadd,byte); idadd=pRxMEM2(dadd,byte);
                              srem=RXMREMb(isadd); drem=RXMREMb(idadd);
                              i=MIN3(srem,drem,rem);
                              dadd+=i; sadd+=i; rem-=i;
                              for (j=i;j;j--,idadd--,isadd--) begin
                                  if (*idadd ne *isadd) then begin
                                       S6CC|= (*isadd gt *idadd)+1;
                                       dadd-=j; sadd-=j; rem=0; break;
                                       endif
                                  endfor
                              endwhile
                          rem=(bsr.w.w1+count-dadd);
                          dadd|=rem<<24;
                          PUTBSD
                          endif else begin
                          kk=*pRxMEM2(sadd,byte);
                          while(rem) do begin
                              idadd=pRxMEM2(dadd,byte);
                              drem=RXMREMb(idadd);
                              i=MIN(drem,rem);
                              dadd+=i; rem-=i;
                              for (j=i;j;j--,idadd--) do begin
                                  if (*idadd ne kk) then begin
                                       S6CC|= (kk gt *idadd)+1;
                                       dadd-=j; rem=0; break;
                                       endif
                                  endfor
                              endwhile
                          rem=count+bsr.w.w1-dadd;
                          SLOC(reg,1,word)=(dadd and M19vx) or (rem<<24);
                          endelse  BREAK;
                          } BREAK;

  
           INST5(0x61) TRAP40CC1;
  /*MBS*/  INST(0x61) NOIND; { BYTESTRINIT(;)
                      if (r) then begin
                          while(rem) do begin
                              isadd=pRxMEM2(sadd,byte); idadd=pRxMEM0(dadd,byte);
                              srem=RXMREMb(isadd); drem=RXMREMb(idadd);
                              i=MIN3(srem,drem,rem);
#ifdef NOSTRMOV
                              for (j=i;j;j--) {*idadd--=*isadd--;}
#else
                              xmovbr(isadd,idadd,i);
#endif
                              dadd+=i; sadd+=i; rem-=i;
                              endwhile
                          PUTBSD
                          endif else begin
                          kk=*pRxMEM2(sadd,byte);
                          while(rem) do begin
                              idadd=pRxMEM0(dadd,byte);
                              drem=RXMREMb(idadd);
                              i=MIN(drem,rem);
#ifdef NOSTRMOV
                              for (j=i;j;j--) {*idadd--=kk;}
#else
                              xstosbr(kk,idadd,i);
#endif
                              dadd+=i; rem-=i;
                              endwhile
                          SLOC(reg,1,word)=dadd and M19vx;
                          endelse
                          } BREAK;
           INST9(0x6F) PRIV NODDR;
  /*MMC*/                  {udw mmc; word i,ix=field(inst,rx); uword count,strt,val;
                      mmc.dw=RRu1; count=((uword) mmc.w.w1)>>24; count=count ? count:256;
                      strt=(mmc.w.w1>>9) and 0xff;
//                    printt("           mmc = ") printdw(mmc.dw) printt(" ix = ") printd(ix) printt(" ")
                      switch (ix) {
                        case 1:  while (count--) do begin               //locks  (physical mem)
                                  val=*pRxMEM2(mmc.w.w0++,word);
                                  for (i=15;i>=0;i--) do begin
                                    s6.wl[strt]=(val>>(2*i)) and 3;
                                    strt++; strt&=0x1fff; endfor              //was 0xff BIG 0x1ff
                                  endwhile;
                                  break;			
                        case 2: while (count--) do begin               //access (virtual men)
                                  val=*pRxMEM2(mmc.w.w0++,word);
                                  for (i=15;i>=0;i--) do begin
                                    s6.memac[strt]=(val>>(2*i)) and 3;
                                    strt++; strt&=0xff; endfor
                                  endwhile;
                                  break;
                        case 4: while (count--) do begin
                                  val=*pRxMEM2(mmc.w.w0++,word);
                                  for (i=3;i>=0;i--) do begin
                                    s6.memmap[strt]=(strt eor ((val>>(8*i)) and 0xff))<<9;
                                    strt++; strt&=0xff; endfor
                                  endwhile;
                                  break;
                        case 5: while (count--) do begin
                                  val=*pRxMEM2(mmc.w.w0++,word);
//                                printt(" val = ") printx(val) NEWLINE
                                  for (i=1;i>=0;i--) do begin              // expand ff, for sz > 128k
                                    s6.memmap[strt]=(strt eor (((val>>(16*i)) and 0x1fff)))<<9;
                                    strt++; strt&=0xff; endfor
                                  endwhile;
//                                PCP_BREAK;
                                  break;
                        default: if (xerox eq 9) TRAP4DCC3;
                        endswitch;
                      STS(mmc.w.w1,strt<<9,0xff01fe00);
                      LD(REG(inst),mmc.dw)
                      } BREAK;
  /*MMC*/  INST(0x6F) PRIV {udw mmc; word i,ix=field(inst,rx); uword count,strt,val;
                      mmc.dw=RRu1; count=((uword) mmc.w.w1)>>24; count=count ? count:256;
                      strt=(mmc.w.w1>>9) and M8B;              //gsp 10/4/09  was 0xff
//                      printt("\n\r    mmc = ") printdw(mmc.dw) printt(" ix = ") printd(ix) printt(" ")
                      switch (ix) {
                        case 1:  while (count--) do begin               //locks  (physical mem)
                                  val=*pRxMEM2(mmc.w.w0++,word);
//                                  printx(val); NEWLINE
                                  for (i=15;i>=0;i--) do begin
                                    s6.wl[strt]=(val>>(2*i)) and 3;
                                    strt++; strt&=M8B; endfor              //was 0xff BIG 0x1ff
                                  endwhile;
                                  break;			
                        case 2:   strt&=0xff;                //gsp 10/4/09
                                  while (count--) do begin               //access (virtual men)
                                  val=*pRxMEM2(mmc.w.w0++,word);
                                  for (i=15;i>=0;i--) do begin
                                    s6.memac[strt]=(val>>(2*i)) and 3;
                                    strt++; strt&=0xff; endfor
                                  endwhile;
                                  break;
                        case 4:   strt&=0xff;                //gsp 10/4/09
                                  while (count--) do begin
                                  val=*pRxMEM2(mmc.w.w0++,word);
                                  for (i=3;i>=0;i--) do begin
                                    s6.memmap[strt]=(strt eor ((val>>(8*i)) and 0xff))<<9;
                                    strt++; strt&=0xff; endfor
                                  endwhile;
                                  break;
                        case 5:   strt&=0xff;                //gsp 10/4/09
                                  while (count--) do begin
                                  val=*pRxMEM2(mmc.w.w0++,word);
//                                printt(" val = ") printx(val) NEWLINE
                                  for (i=1;i>=0;i--) do begin              // expand ff, for sz > 128k
                                    s6.memmap[strt]=(strt eor (((val>>(16*i)) and 0x1ff)))<<9;
                                    strt++; strt&=0xff; endfor
                                  endwhile;
//                                PCP_BREAK;
                                  break;
                        default: if (xerox eq 9) TRAP4DCC3;
                        endswitch;
                      STS(mmc.w.w1,strt<<9,0xff01fe00);
                      LD(REG(inst),mmc.dw)
                      } BREAK;

  /*MMC*/  INST5(0x6F) PRIV {udw mmc; word i,ix=field(inst,rx); uword count,strt,val;
                      mmc.dw=RRu1; count=((uword) mmc.w.w1)>>24; count=count ? count:256;
                      strt=(mmc.w.w1>>9) and 0xff;
//                    printt("           mmc = ") printdw(mmc.dw) printt(" ix = ") printd(ix) printt(" ")
                      switch (ix) {
                        case 1:  while (count--) do begin               //locks  (physical mem)
                                  val=*pRxMEM2(mmc.w.w0++,word);
                                  for (i=15;i>=0;i--) do begin
                                    s6.wl[strt]=(val>>(2*i)) and 3;
                                    strt++; strt&=0x0ff; endfor              //was 0xff BIG 0x1ff
                                  endwhile;
                                  break;			
                        default: TRAP40CC1;
                        endswitch;
                      STS(mmc.w.w1,strt<<9,0xff01fe00);
                      LD(REG(inst),mmc.dw)
                      } BREAK;

