#include <stdio.h>
#include "sigma.h"
#include "sigmaio.h"
#include "coc.h"
#include <string.h>
#include <stdlib.h>
#include "lights.h"

extern i_states intr[];
extern ibits active,waiting,ewaiting;
extern word event,tics;
extern verbose;
extern vidcoc,light_on,light_off;
extern SEMAPHORE_DATA(DE);
#define LOCGROUP(loc) ((loc ge 0x60) ? loc/16-4 : 0)
#define LOCLEVEL(loc) (0x8000 >> (loc ge 0x60 ? (loc and 0xf) : ((loc-2)and 0xf)))
#define COC_O_INT TRIGGER(coc->ig1,coc->ib1);intr[coc->ig1].signal|=coc->ib1;

cocdata * cocs[16]={[0 ... 15] = 0};      //  Sigmas can have 16 cocs

          //leading zero for 1 indexing (there is no com0)
//int comlines[5] = {0, 1,  15,    7,   5};   //  coc:line for com1-com4
int comlines[NP] = {[0 ... NP-1] = -1};
//int comports[5] = {0, 0x3f8, 0x2f8, 0x3e8, 0x2e8};
extern int comport[NP];
int strobe_flag;
byte sphase[NP]={0};
//see link table to set irqs in dosint
#define VERB_LEVEL V_CONFIG


void add_vterm(char * buf)
{ 
printf("Config for virtual terminals is obsolete\n");
    return;

}
void add_boca(char * buf)
{   
printf("Config for boca is obsolete\n");
}

unsigned char pkt_dest[6];

void add_netcom(char * buf)
{   
printf("config for packet driver is obsolete\n");

}

void add_com(char * buf)
{ 
printf("config for hardware com ports is obsolete\n");
}

void add_lpt(char * buf)
{
printf("config for lpt not available\n");
}
typedef struct tag_term_info {
   struct tag_term_info * tinfo_flink;
   struct tag_term_info * tinfo_blink;
   char * type;         //DTMD WYSE, etc
   int  masksz;
   int * mask;
} term_info;

 term_info * tinfo_head=0;
 term_info * tinfo_tail=0;

void set_termlines( char * buf)   //called from config for TERM type masks
{
char *junk,*ttyp,*mask;
term_info * new;
int masklen,masksz,i,x,wd,bit;
   junk=strtok(buf,", \t\n\r");
   ttyp=strtok(NULL,", ;\t\n\r");
   mask=strtok(NULL,"; ,\t\n\r");
   V_CHK(printf("mask for %s set to %s\n",ttyp,mask);)
   new=(term_info*) malloc(sizeof(term_info));
   LINK(new,tinfo_,);
   new->type=(char *)malloc(strlen(ttyp)+1);
   masklen=strlen(mask);
   new->masksz=masksz=(31+4*masklen)/32;
   new->mask=(int *) malloc(masksz *4);
   strcpy(new->type,ttyp);
   for (i=0;i<masklen;i++) begin
       unsigned char j=mask[i];
       if ((j ge '0') and (j le '9')) x=j-'0';
       else if ((j ge 'A') and (j le 'F')) x=j-'A'+10;
       else if ((j ge 'a') and (j le 'f')) x=j-'a'+10;
       else {printf("invalid hex in mask\n"); x=0;break;}
       wd=i/8;
       bit=x<<(28-(i & 7)*4);
       *(new->mask+wd)|=bit;
       endfor
    V_CHK(for(i=0;i<masksz;i++) outputx(new->mask[i]);)


}

extern int port;
void set_netport(char * buf)    //called from config to set listening tcp port
{
  char *junk,*portnum;
   junk=strtok(buf,", \t\n\r");
   portnum=strtok(NULL,", ;\t\n\r");
   port=atoi(portnum);
   output(port);
}

#define NMAX (16*64)
devdata * cocdev[16];
int ncocs=0;
int cocln[NMAX];
int assign_cocln(int fd,char * ttype)
{
   int i,j;
   term_info * p= tinfo_head;
   int masksz=0;
   int *bits =0;
   if (fd ge NMAX) return -1;
   while (p) {
      if (strcmp(ttype,p->type) eq 0) {
           V_CHK(printf("matched terminal type %s\n\r",p->type);)
           masksz=p->masksz;
           bits=p->mask;
           break;
           }
      p=p->tinfo_flink;
      }

   for (i=0; i < ncocs ; i++) {
       cocdata * c = cocs[i];
     for (j=2; j < 64; j++) {                                //fix me (avoid xp 1 line)
     int kk=64*i+j;
     int wd=kk/32;
     int bit=1<<(31-(kk and 31));
     int found=0;
     if (wd < masksz) if((bits[wd] and bit) ne 0) found=1;
     if((masksz eq 0) or (found))
      if (c->fd[j] eq 0) {
           devdata * dev=c->dev;
           c->fd[j]=fd;
           cocln[fd]=(i<<16) + j;
           V_CHK(printf("\r\nsocket %d assigned coc # %d line %d\r\n",fd,i,j);)
           c->dsr[j]=1;
           V_CHK(printf("set dsr on %s line %d\n\r",dev->name,j);)
           return cocln[fd];
           }
      }     
    }
    V_CHK(printf("can't assign cocline ncocs = %d\n\r",ncocs);)
    return -1;
}


void cocinit(devdata * dev)       //map screen users to coc lines
{   int i,parm[6]={0,0,0,0,0,0},cocn;                // and init coc data
    cocdata * c;
    char * pstr, *pend;  
    V_CHK(printt(dev->name) printt(" ") printt(dev->fid); NEWLINE)
    cocn=strtol(strtok(dev->fid,", \t\n\r"),&pend,16);
    cocn=0;
    dev->fpos=cocn;                   //a tacky place to keep it, i know
    cocdev[ncocs++]=dev;
    for (i=1;i<6;i++) begin
        pstr=strtok(NULL,", \t");
        if (pstr) parm[i]=strtol(pstr,&pend,16); else break;
        endfor
    if ((c=cocs[cocn]) eq 0) begin
        c=cocs[cocn]=(cocdata *) malloc(sizeof(cocdata));
        c->dev=dev;
        endif else {printt("Duplicate COC number\n")}
    c->iloc0=parm[1];
    dev->ig0=LOCGROUP(c->iloc0);
    dev->ib0=LOCLEVEL(c->iloc0);
    c->iloc1=parm[2];
    c->ig1=LOCGROUP(c->iloc1);
    c->ib1=LOCLEVEL(c->iloc1);
    V_CHK(printt(dev->name) printt(" COC init called") NEWLINE
    printxv(c->iloc0) printxv(dev->ig0) printxv(dev->ib0) NEWLINE
    printxv(c->iloc1) printxv(c->ig1) printxv(c->ib1) NEWLINE)
    for (i=0;i<64;i++) { c->dsr[i]=c->rcv[i]=c->rds[i]=
        c->xlsp[i]=c->test[i]=c->ltype[i]=c->fd[i]=0;}
//    for (i=0;i<13;i++) begin
//        if (altf_cocln[i]>>6 eq cocn) begin
//            c->dsr[altf_cocln[i] and 0x3f]=1;
//            c->usr[altf_cocln[i] and 0x3f]=i-1;
//            userdev[i-1]=dev;
//            endif
//        endfor
    for (i=1;i<NP;i++) begin
        if (ptype[i]) begin  //avoid clobbering ln 0 from unused entries
          if (comlines[i]>>6 eq cocn) begin
             c->ltype[comlines[i] and 0x3f]=i;
             if (ptype[i] eq 4) c->dsr[comlines[i] and 0x3f]=1; //fake net dsr ready
             endif
          endif
        endfor
    c->run[0]=c->run[1]=c->xscanhold[0]=c->xscanhold[1]=c->scan=0;
}
#undef VERB_LEVEL
byte coc_rd(word ccc)
{   //returns scanner position (line number) that needs transmitter service
    word wi;
    word cocn=(ccc and 0xf0)>>4;
    word func=ccc and 0xf;
    cocdata * coc = cocs[cocn];
    if (coc eq 0) return 0;
    wi=((coc->scan+1) and 63)>>5;
    if (!(coc->xscanhold[wi])) wi^=1;
    if ((coc->xscanhold[wi])) coc->scan=BPRIO(coc->xscanhold[wi])-1+(wi<<5);
        else coc->scan=tics and 63;      //show some action - pretend its scanning
    return coc->scan;
}
byte coc_wd(word ccc,word d)
{    //returns cc dependant on line state of addressed line and function code
    word ltyp;
    word cocn=(ccc and 0xf0)>>4;
    word func=ccc and 0xf;
    byte cc;
    word L=d and 0x3f;
    word lwi=L>>5;
    uword lbit=1<<(L and 31);
    word d8;
#define SET_COCBIT(x) coc->x[lwi]|=lbit;
#define CLEAR_COCBIT(x) coc->x[lwi]&=not(lbit);
#define COCBYT(x) coc->x[L]
#define XMTSTAT 3
#define PON(stat) ({byte _s=stat; _s^=0x80; _s&=0xf8; \
    _s|=_s>>4;_s|=_s>>2;_s|=_s>>1;_s&=1;_s;})

//#define RCVSTAT (COCBYT(rcvcc))
#define RCVSTAT (COCBYT(dsr))*(2-COCBYT(rcv))  //needs "long space received" here
    cocdata * coc = cocs[cocn];
    if (coc eq 0) return 3;
    ltyp=coc->ltype[L];
    switch (func) {
        case 0: if ISLPT(ltyp) {
                    COCBYT(dsr)=PON(inp(comport[ltyp]+1));
                    if (COCBYT(dsr) eq 0) sphase[ltyp]=0;}
                return RCVSTAT;                   //sense receiver L status
        case 1: COCBYT(rcv)=1; return RCVSTAT;    //turn receiver L on
        case 2: COCBYT(rcv)=0; return RCVSTAT;    //turn receiver L off
        case 3: COCBYT(rds)=0; return RCVSTAT;    //turn receiver L dataset off and turn back-back test off
        case 4: return XMTSTAT;                   //sense transmitter L status
        case 5: SET_COCBIT(run);                  // send the character
                d8=d>>8;
//                LINE
//                printf("line %d fd %d ch %d\n\r",L,coc->fd[L],d8);
                socketout(coc->fd[L],d8);
                SET_COCBIT(xscanhold)
                COC_O_INT
                return XMTSTAT;

#if 0
                if (ltyp) begin
/*phys term*/     switch(ptype[ltyp]) {
                   case 1:      //uart
                   case 2:      //boca uart
                        outp(comport[ltyp]+0,d8);     //uart data out
                        break;
                   case 3:      //lpt
                        outp(comport[ltyp]+0,d8);     //lpt data out
                        if (COCBYT(dsr)) begin
                            sphase[ltyp]=1;   //start lpt strobe phase counter
                            SET(event,strobe_evt);
                            SET(strobe_flag,1L<<ltyp);
                            endif else begin      //if printer is off, fake it
                            SET_COCBIT(xscanhold)  //because handshake is dead
                            COC_O_INT              //and we'll never finish
                            sphase[ltyp]=0;
                            return XMTSTAT;   //gsp 6/99
                            endelse
                        break;
                   case 4:      //net
//                        pkt_out(d8);
                        SET_COCBIT(xscanhold)
                        COC_O_INT
                        return XMTSTAT;
                        break;
                   endswitch
                  CLEAR_COCBIT(xscanhold);
                  if (!(coc->xscanhold[0] or coc->xscanhold[1]))
                    {CLEAR(intr[coc->ig1].signal,coc->ib1);}
                  endif else begin
/*virtual term*/  if (COCBYT(dsr)) then begin   //skip inactive virtual screens
                        int u=COCBYT(usr);
                        SET_COC_ON      //update lights
                        vterm_write(u,d8);  //transmit on L
                        SET_COC_OFF
                //      showscreen(u);putch((d8) and 0x7f); //transmit on L
                        endif
                    SET_COCBIT(xscanhold)
                    COC_O_INT
                    endelse
                return XMTSTAT;
#endif
        case 6: SET_COCBIT(run);  //send long space on L
                if (ltyp) begin
                     //xmit long space to physical line
                    endif else begin
                    SET_COCBIT(xscanhold)
          //          TRIGGER(2,0x4000);intr[2].signal|=0x4000;
                    COC_O_INT
                    endelse
                return XMTSTAT;
        case 7: COCBYT(rds)=1; return XMTSTAT;  //turn transmit data set off or back-back test on
        case 0xE: CLEAR_COCBIT(run);            //stop transmit on L
                coc->xscanhold[0]&=coc->run[0];
                coc->xscanhold[1]&=coc->run[1];
                if (!(coc->xscanhold[0] or coc->xscanhold[1]))
           //       {CLEAR(intr[2].signal,0x4000);}
                    {CLEAR(intr[coc->ig1].signal,coc->ib1);}
                return XMTSTAT;
        default: printt("coc wd func = ") printxv(func)   //could set baud here
                printt(" char/Line = ") printxv(d) NEWLINE
                break;
      endswitch
    return 0;
}

#if 0
void coc_com()
{   word x,pline,cocn,L,lwi;
    byte cls,cms;
    uword lbit;
    cocdata *coc;
    void * dev;
  while (com_flag) begin
    int rdy;
    DISABLE
    x=BPRIO(com_flag)-1;            //determine com port number
    cls=com_linestat[x];
    cms=com_modemstat[x];
    CLEAR(com_flag,1L<<x)
    ENABLE
    pline=comlines[x];              //pc com port's coc - line number
    cocn=(pline>>6) and 0xf;        //coc number
    L=pline and 0x3f;               //line number on this coc
    lwi=L>>5;
    lbit=1<<(L and 31);
    coc = cocs[cocn];
    dev=coc->dev;
    switch (ptype[x]) {
        case 2:
        case 1:                            //com port
            COCBYT(dsr)=(cms >> 5) and 1;
            rdy= (cls and 0x20); break;
        case 3: rdy=1;                     //lpt port
         }
    if (rdy && (coc->run[lwi] and lbit)) begin
        SET_COCBIT(xscanhold)
        COC_O_INT
        endif
    endwhile
}


void com_in()
{   word x,pline,cocn,L;
    cocdata *coc;
    void * dev;
     DISABLE
     while(ringii ne ringor) begin
        uhword ch;
        x=ring[ringor++]; ch=ring[ringor++];
        ringor &= (sizeof(ring)-1);
        ENABLE
        pline=comlines[x and 0x7f];     //pc com port's coc - line number
        cocn=(pline>>6) and 0xf;        //coc number
        L=pline and 0x3f;               //line number on this coc
        ch=(x and 0x80) ? 0x100 : ch;   //break bit
        coc = cocs[cocn];
        dev=coc->dev;
        dev_ME_charin(dev,ch,L);
        DISABLE
        endwhile
     com_in_flag=0;
     ENABLE
}
lpt_strobe()
{   int x,p,bit;
    int bits=strobe_flag;
//  printt(" strobe entered bits = ") printxv(bits) NEWLINE
    while (bits) begin
        x=BPRIO(bits)-1;
        bit=1L<<x;
        p=comport[x];
//      printxv(x) printd(sphase[x]) NEWLINE
        switch (sphase[x]) {
            case 1:  if (inp(p+1) and 0x80) { sphase[x]++;  //wait for not busy
                        outp(p+2,inp(p+2) or 1);} break;    //set strobe
            case 2:  if ((inp(p+1) and 0xc0) eq 0x40 )  //strobe til busy
                        {sphase[x]=0;
                        outp(p+2,(inp(p+2) and 0xfe) or 0x10);
                        //set irq, reset strobe
                        CLEAR(strobe_flag,bit);} break;
            case 0:  CLEAR(strobe_flag,bit);
                     //if line off, we'll never interrupt so we clear sphase
                     //to keep output from hanging forever
                     // (and killing zaps)
                     SET(com_flag,bit);  //force interrrupt
                     break;
            endswitch
        CLEAR(bits,bit);
        endwhile
}
#endif

