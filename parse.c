#include "parse.h"

/* states */

#define IN_WHITE 0
#define IN_TOKEN 1
#define IN_QUOTE 2
#define IN_OZONE 3

int _p_state;      /* current state      */
unsigned _p_flag;  /* option flag        */
char _p_curquote;  /* current quote char */
int _p_tokpos;     /* current token pos  */

/* routine to find character in string ... used only by "parser" */

sindex(ch,string)
char ch,*string;

{
  char *cp;
  for(cp=string;*cp;++cp)
    if(ch==*cp)
      return (int)(cp-string);  /* return postion of character */
  return -1;                    /* eol ... no match found */
}
    
/* routine to store a character in a string ... used only by "parser" */

chstore(string,max,ch)
char *string,ch;
int max;

{
  char c;
  if(_p_tokpos>=0&&_p_tokpos<max-1)
  {
    if(_p_state==IN_QUOTE)
      c=ch;
    else
      switch(_p_flag&3)
      {
        case 1:             /* convert to upper */
          c=toupper(ch);
          break;
  
        case 2:             /* convert to lower */
          c=tolower(ch);
          break;
      
        default:            /* use as is */
          c=ch;
          break;
      }
    string[_p_tokpos++]=c;
  }
  return;
}
  
/* here it is! */

//parser(inflag,token,tokmax,line,white,brkchar,quote,eschar,brkused,next,quoted)
parse(unsigned inflag,char *token,int tokmax,char *line,parse_table* ptab,
    char *brkused, int *next,char * quoted)
#define white ptab->whites
#define brkchar ptab->delims
#define quote ptab->quotes
#define eschar ptab->escape
{
  int qp;
  char c,nc;
          
  *brkused=0;           /* initialize to null */  
  *quoted=0;/* assume not quoted  */

  if(!line[*next])      /* if we're at end of line, indicate such */
    return 1;

  _p_state=IN_WHITE;       /* initialize state */
  _p_curquote=0;           /* initialize previous quote char */
  _p_flag=inflag;          /* set option flag */

  for(_p_tokpos=0;c=line[*next];++(*next))      /* main loop */
  {
    if((qp=sindex(c,brkchar))>=0)  /* break */
    {
      switch(_p_state)
      {
        case IN_WHITE:          /* these are the same here ...*/
        case IN_TOKEN:          /* ... just get out*/
case IN_OZONE:/* ditto*/
          ++(*next);
          *brkused=brkchar[qp];
          goto byebye;
        
        case IN_QUOTE:           /* just keep going */
          chstore(token,tokmax,c);
          break;
      }
    }
    else if((qp=sindex(c,quote))>=0)  /* quote */
    {
      switch(_p_state)
      {
        case IN_WHITE:   /* these are identical, */
          _p_state=IN_QUOTE;        /* change states   */
          _p_curquote=quote[qp];         /* save quote char */
          *quoted=1;/* set to true as long as something is in quotes */
          break;
  
        case IN_QUOTE:
          if(quote[qp]==_p_curquote)/* same as the beginning quote? */
  {
            _p_state=IN_OZONE;
    _p_curquote=0;
  }
          else
            chstore(token,tokmax,c);/* treat as regular char */
          break;

case IN_TOKEN:
case IN_OZONE:
  *brkused=c;/* uses quote as break char */
  goto byebye;
      }
    }
    else if((qp=sindex(c,white))>=0)       /* white */
    {
      switch(_p_state)
      {
        case IN_WHITE:
case IN_OZONE:
          break;/* keep going */
          
        case IN_TOKEN:
          _p_state=IN_OZONE;
          break;
          
        case IN_QUOTE:
          chstore(token,tokmax,c);     /* it's valid here */
          break;
      }
    }
    else if(c==eschar)/* escape */
    {
      nc=line[(*next)+1];
      if(nc==0)/* end of line */
      {
*brkused=0;
chstore(token,tokmax,c);
++(*next);
goto byebye;
      }
      switch(_p_state)
      {
case IN_WHITE:
  --(*next);
  _p_state=IN_TOKEN;
  break;

case IN_TOKEN:
case IN_QUOTE:
  ++(*next);
  chstore(token,tokmax,nc);
  break;

case IN_OZONE:
  goto byebye;
      }
    }
    else        /* anything else is just a real character */
    {
      switch(_p_state)
      {
        case IN_WHITE:
          _p_state=IN_TOKEN;        /* switch states */
          
        case IN_TOKEN:           /* these 2 are     */
        case IN_QUOTE:           /*  identical here */
          chstore(token,tokmax,c);
          break;

case IN_OZONE:
  goto byebye;
      }
    }
  }             /* end of main loop */

byebye:
  token[_p_tokpos]=0;   /* make sure token ends with EOS */
  
  return 0;
  
}

