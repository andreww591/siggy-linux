#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "sigma.h"
#include "sigmaio.h"
#include "mv.h"
#define VERB_LEVEL V_CONFIG
iopdata *iophead=NULL;
extern iopdata * Piop[8];
extern int verbose;
extern devdata * userdev[];
extern i_states intr[];

extern byte e2a[256];
iopdata * find_iop(word address)
{
    iopdata *p=iophead;
    while (p) do begin
        if (address eq p->address) then return p;
        p=p->flink;
        endwhile
    return NULL;
}

iopdata * set_iop(word address)
{   word myi;
    iopdata* p=find_iop(address);
    if (p eq NULL) then begin
        p=(iopdata *) malloc(sizeof(iopdata));
        p->flink=iophead; p->address=address; p->channels=24;
        p->child=NULL;
        p->ic=0;
        {word i; for(i=0;i<31;i++) {p->PDC[i]=0;}}
        iophead=p;
        if (p->flink eq NULL) p->pbit=1; else p->pbit=((p->flink)->pbit)<<1;
        myi=BPRIO(p->pbit);
        Piop[myi-1]=p;
        endif
    return p;
}
DCdata * find_DC(iopdata * iop, word DCaddr)
{
    DCdata *p=iop->child;
    while (p) do begin
        if (DCaddr eq p->address) then return p;
        p=p->flink;
        endwhile
    return NULL;
}
DCdata * set_DC(word iopaddr,word DCaddr)
{
    DCdata * p;
    iopdata* iop=set_iop(iopaddr);
    word myi,i;
    p=find_DC(iop,DCaddr);
    if (p eq NULL) then begin
        p=(DCdata *) malloc(sizeof(DCdata));
        p->flink=iop->child; iop->child=p;
        p->parent=iop; p->child=NULL;
        p->address=DCaddr;
        p->TSH_status=0x10;          //device automatic
        p->busy=p->multi=0;
        if (p->flink eq NULL) then p->pbit=1; else p->pbit=((p->flink)->pbit)<<1;
        myi=BPRIO(p->pbit);
        for (i=0;i<16;i++) {p->Pdev[i]=NULL;}
        iop->PDC[myi-1]=p; p->ic=0;
        endif
    return p;
}
devdata * find_dev(DCdata *DC, word devaddr)
{
    devdata *p=DC->child;
    while (p) do begin
        if (devaddr eq p->address) then return p;
        p=p->flink;
        endwhile
    return NULL;
}

devdata * find_ua(word ua)
{
    iopdata * iop;
    DCdata * DC;
    devdata * dev;
    iop=find_iop(ua>>8);
    if (iop eq NULL) return 0;
    if (ua and 0x80) then DC=find_DC(iop,(ua and 0x70)>>4);
         else DC=find_DC(iop,ua and 0x7F);
    if (DC eq NULL) then return 0;
    return find_dev(DC,ua and 0x7f);
}


devdata * set_dev(word iopaddr, word DCaddr, word devaddr, char *name,char *fid)
{
    devdata *p;
    hword pnem;
    DCdata *DC =set_DC(iopaddr, DCaddr);
    p=find_dev(DC,devaddr);
    if (p eq NULL) then begin
        p=(devdata *) malloc(sizeof(devdata));
        stosb(0,(char *)p,sizeof(devdata));
        p->flink=DC->child; DC->child=p;
        p->parent=DC; p->address=devaddr;
        p->stop=-1;
        p->pcchan=1;
//      p->fileno=p->recno=p->lastblock=p->nextblock=p->written=p->fpos=p->ars=p->orp=0;
        if (strlen(name) eq 5) then begin
            if (fid ne NULL) then begin
                p->fid=(char*)malloc(strlen(fid)+1);
                strcpy(p->fid,fid);
                endif else p->fid=NULL;
            strncpy(p->name,name,5); //p->name[5]=0;
  //        p->TDV_status=0;
  //        p->ssize=0;
            p->verbose=1;
  //        p->hio_handler=0;
            pnem=*((hword *)name);
            switch (pnem) begin
              case 0x5954 : p->handler=dev_TY;
                            p->size=1000;p->verbose=0;
//                            userdev[ttyuser]=p;  //this wont handle multiple TY's yet
                            p->hio_handler=dev_TY_hio; p->pcchan=0;
                            console_init(p);
                            break;
              case 0x5243 : p->handler=dev_CR;
                            p->size=200;break;
              case 0x4344 : p->handler=dev_DC;
                            (p->parent)->multi=1;
                            p->ssize=3; p->sense= (byte *) malloc(4);
//                          p->verbose=0;
                            p->heads=512;
                            p->nspt=12;
                            {word i;for (i=0;i<4;i++) {p->sense[i]=0;}}
                            p->size=2048;break;
              case 0x5044 : p->handler=dev_DP;
                            (p->parent)->multi=1;
                            p->ssize=16; p->sense= (byte *) malloc(p->ssize);
                            p->verbose=0;
                            p->heads=19;
                            p->nspt=11;
                            {word i;for (i=0;i<16;i++) {p->sense[i]=0;}}
                            p->size=2048;break;
              case 0x544D : p->handler=dev_MT;
//                          p->verbose=0;
                            (p->parent)->multi=1;
                            p->size=65536;break;
              case 0x5442 : p->handler=dev_BT;
                            (p->parent)->multi=1; p->pcchan=2;
                            p->size=32768;break;
              case 0x504C : p->handler=dev_LP;
                            p->size=2048;break;
              case 0x454D : p->handler=dev_ME;
                            p->size=256; p->pcchan=0;
//                          p->verbose=0;
                            cocinit(p); break;
              default: printxv(pnem) printt(" No handler for ") printt(name) NEWLINE
//                  p->handler=NULL;
                    break;
                endswitch
//            p->HOST=0;
              p->buffer=(byte *) malloc(p->size);
            endif
        p->AIOstatus=(iopaddr<<8) or ((DC->multi) ? (0x80 or (DCaddr<<4) or devaddr) : DCaddr);
//      p->AIOcc=0;
        if (p->flink eq NULL) then p->pbit=1; else
            p->pbit=((p->flink)->pbit)<<1;
        DC->Pdev[BPRIO(p->pbit)-1]=p;
        endif
    return p;
}

cfginit(char * fid)
{
    FILE *cfg;
    char buf[120];
    int ars,i;
    V_CHK(printt("cfginit called with fid = ") printt(fid) NEWLINE)
//  userinit();   //must precede add_com calls
//  aspi_init();
    for (i=0;i<256;i++) {if (!e2a[i]) e2a[i]=0xff;}  //prevent truncated writes
    cfg = fopen(fid,"rt");
    if (cfg eq NULL) then begin printt("Unable to open config file '")
        printt(fid) printt("'\n")
        printt("Using default configuration\n")
        add_dev("TYA01");
        add_dev("LPA02");
        add_dev("CRA03");
        add_dev("MTA80");
        goto cfg_interrupts;
        endif else {printt("Using config file ") printt(fid);NEWLINE }
next:
    if(fgets(buf,120,cfg) ne NULL) then begin
        if ((ars=strlen(buf)) ge 2) then begin
            if (buf[ars-1] < ' ') then buf[ars-1]=0;
            if (buf[0] ge 'A' ) then begin          //allow remarks
                if (strncasecmp(buf,"com",3) eq 0) add_com(buf);
                else if (strncasecmp(buf,"boca",4) eq 0) add_boca(buf);
                else if (strncasecmp(buf,"lpt",3) eq 0) add_lpt(buf);
                else if (strncasecmp(buf,"altf",4) eq 0) add_vterm(buf);
                else if (strncasecmp(buf,"pkt",3) eq 0) add_netcom(buf);
                else if (strncasecmp(buf,"term",4) eq 0) set_termlines(buf);
                else if (strncasecmp(buf,"netport",7) eq 0) set_netport(buf);
                else add_dev(buf);
                endif
            goto next;
            endif
        endif else fclose(cfg);
cfg_interrupts:
    for (i=0;i<16;i++) do begin
        word ig=i^(i>>1);
        intr[i].armed=intr[i].enabled=intr[i].triggered=0;
        intr[i].active=intr[i].waiting=intr[i].ewaiting=0;
        intr[i].pbit=0x8000>>ig ; //just for fun
        intr[ig].grp=i;
        endfor
    kbd_init();
//  showiocfg();
    return 0;
}

add_dev(char *dev)
{
    devdata *pdev;
    word m,iopaddr,DCaddr,devaddr;
    if (strlen(dev) ge 5) then begin
        V_CHK(printt("Adding device = ") printt(dev) NEWLINE)
        iopaddr=dev[2]-'A';
        devaddr=bin(dev[4]) and 0xf;
        DCaddr=bin(dev[3]) and 7;
        if (!(bin(dev[3]) and 8)) then begin
            DCaddr=16*DCaddr+devaddr;
            devaddr=DCaddr;
            endif
        {char *devn,*fid;
            devn=strtok(dev,", \t\n\r");
            fid=strtok(NULL,"; \t\n\r");
            pdev=set_dev(iopaddr,DCaddr,devaddr,devn,fid);
        }
        endif else begin
            printt("Unrecognized device ") printt(dev) NEWLINE;
            endelse
    return 0;
}
#include "vterm.h"
#undef V_USER
#define V_USER pcpuser
showiocfg()       // was iopdata *head)
{
    iopdata *cur_iop=iophead;
    DCdata *cur_DC;
    devdata *cur_dev;
    while (cur_iop) do begin
        printt("IOP address = ") printxv(cur_iop->address) NEWLINE
        cur_DC=cur_iop->child;
        while (cur_DC) do begin
            printt(" DC address = ") printxv(cur_DC->address) NEWLINE
            cur_dev=cur_DC->child;
            while (cur_dev) do begin
//              if (cur_dev->parent ne cur_DC)
//                  {printt("broken config data\n"); return;}
                printt("  Device address = ") printxv(cur_dev->address)
                printt(" Handler= ") printxv(cur_dev->handler)
                printt("  ") printt(cur_dev->name)
                if (cur_dev->fid ne NULL) then begin
                    printt(" <=> ") printt(cur_dev->fid)
                    endif
                printt(" pcchan=") printxv(cur_dev->pcchan)
                NEWLINE
                cur_dev=cur_dev->flink;
                endwhile
            cur_DC=cur_DC->flink;
            endwhile
        cur_iop=cur_iop->flink;
        endwhile
}


