#include "srglobals.h"
extern sigma6 s6;
extern cocdata * cocs[16];
extern byte ascm;

//extern baud[5],comx[8],comxl[10],
extern comport[NP],icoms[NP],irqs[16];
//extern unsigned char com_in[5],mcr[5],lcr[10];
//extern com_modemstat[5],com_linestat[5];
extern byte sphase[NP];

extern iopdata * iophead;
extern i_states intr[];
extern ibits ewaiting, active, waiting;


extern s6pcp cp;
