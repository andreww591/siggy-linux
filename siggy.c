#include "opts.h"
#define TABLE 1
#include <stdlib.h>
#include <signal.h>
#include <stdio.h>
#include "keys.h"
//#include <pc.h>
//#include "go32.h"
//#include <string.h>
#include <assert.h>
//#include "argcheck.h"
#include "sigma.h"
#include "cpu.h"
#include "lights.h"
#include "mv.h"
//#define XSTRMOV
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#ifndef XSTRMOV
#define xmovbr(x,y,z) movbr(x,y,z)
#define xstosbr(x,y,z) stosbr(x,y,z)
#else
void xmovbr(char *a,char *b,int c);
void xstosbr(int a,char * b,int c);
#endif

//#include "coc.h"

#include "float.h"
#include "dosint.h"
//#undef BRANCHTR
#define VERB_LEVEL V_CPU
#undef V_USER
#define V_USER pcpuser
void release_clock();
void ihandler(int junk);
void exitclean();

#define CC1 (S6CC>>3)
#define IOCMD ((OPCODE(inst)>>5 and 4) or (OPCODE(inst) and 3))

#define IOINST PRIV { int t=EVA; \
  int w20=MEMW(0x20)= ((t<<24) or (IOCMD ? 0 :(REGWA(0) and M21)));       \
  V_CHK(printt(cmdname[IOCMD]) printt(" Write x'20' = ") printx(w20) NEWLINE)                             \
  S6CC=(S6CC and 3) or (t=iop(IOCMD,t>>8 and 7));                       \
  V_CHK(printt("iop returned cc=") printxv(t) printt(" and 20,21 now = ") \
      printdw(MEMD(0x10)) NEWLINE)  \
  if ((REG(inst) ne 0) && (CC1 eq 0)) then begin \
    LDl(REG(inst),MEMD(0x20>>1)) \
  V_CHK(printt("R") printd(REG(inst)) printt(" = ") printx(R)  \
  if ((REG(inst) and 1) eq 0) then printt(" Ru1 = ") printx(SLOC(reg,REG(inst)+1,word)) NEWLINE)\
  endif }




#include "rvolatiles.h"

#include "globals.h"
extern void *thead;    //head of time delayed io linked list


//void *nexti[1<<12];    // No gaps in defined bits allowed

main(int argc, char *argv[])
{   //byte S6CC;
    word *iiad,iiremx,iirem=0;
#ifdef TABLE
    void * itbl[256] = {
       &&i0x00, &&i0x01, &&i0x02, &&i0x03, &&i0x04, &&i0x05, &&i0x06, &&i0x07,
       &&i0x08, &&i0x09, &&i0x0A, &&i0x0B, &&i0x0C, &&i0x0D, &&i0x0E, &&i0x0F,
       &&i0x10, &&i0x11, &&i0x12, &&i0x13, &&i0x14, &&i0x15, &&i0x16, &&i0x17,
       &&i0x18, &&i0x19, &&i0x1A, &&i0x1B, &&i0x1C, &&i0x1D, &&i0x1E, &&i0x1F,
       &&i0x20, &&i0x21, &&i0x22, &&i0x23, &&i0x24, &&i0x25, &&i0x26, &&i0x27,
       &&i0x28, &&i0x29, &&i0x2A, &&i0x2B, &&i0x2C, &&i0x2D, &&i0x2E, &&i0x2F,       
       &&i0x30, &&i0x31, &&i0x32, &&i0x33, &&i0x34, &&i0x35, &&i0x36, &&i0x37,
       &&i0x38, &&i0x39, &&i0x3A, &&i0x3B, &&i0x3C, &&i0x3D, &&i0x3E, &&i0x3F,       
       &&i0x40, &&i0x41, &&i0x42, &&i0x43, &&i0x44, &&i0x45, &&i0x46, &&i0x47,
       &&i0x48, &&i0x49, &&i0x4A, &&i0x4B, &&i0x4C, &&i0x4D, &&i0x4E, &&i0x4F,       
       &&i0x50, &&i0x51, &&i0x52, &&i0x53, &&i0x54, &&i0x55, &&i0x56, &&i0x57,
       &&i0x58, &&i0x59, &&i0x5A, &&i0x5B, &&i0x5C, &&i0x5D, &&i0x5E, &&i0x5F,       
       &&i0x60, &&i0x61, &&i0x62, &&i0x63, &&i0x64, &&i0x65, &&i0x66, &&i0x67,
       &&i0x68, &&i0x69, &&i0x6A, &&i0x6B, &&i0x6C, &&i0x6D, &&i0x6E, &&i0x6F,       
       &&i0x70, &&i0x71, &&i0x72, &&i0x73, &&i0x74, &&i0x75, &&i0x76, &&i0x77,
       &&i0x78, &&i0x79, &&i0x7A, &&i0x7B, &&i0x7C, &&i0x7D, &&i0x7E, &&i0x7F,
       &&i0x00, &&i0x01, &&i0x02, &&i0x03, &&i0x04, &&i0x05, &&i0x06, &&i0x07,
       &&i0x08, &&i0x09, &&i0x0A, &&i0x0B, &&i0x0C, &&i0x0D, &&i0x0E, &&i0x0F,
       &&i0x10, &&i0x11, &&i0x12, &&i0x13, &&i0x14, &&i0x15, &&i0x16, &&i0x17,
       &&i0x18, &&i0x19, &&i0x1A, &&i0x1B, &&i0x1C, &&i0x1D, &&i0x1E, &&i0x1F,
       &&i0x20, &&i0x21, &&i0x22, &&i0x23, &&i0x24, &&i0x25, &&i0x26, &&i0x27,
       &&i0x28, &&i0x29, &&i0x2A, &&i0x2B, &&i0x2C, &&i0x2D, &&i0x2E, &&i0x2F,       
       &&i0x30, &&i0x31, &&i0x32, &&i0x33, &&i0x34, &&i0x35, &&i0x36, &&i0x37,
       &&i0x38, &&i0x39, &&i0x3A, &&i0x3B, &&i0x3C, &&i0x3D, &&i0x3E, &&i0x3F,       
       &&i0x40, &&i0x41, &&i0x42, &&i0x43, &&i0x44, &&i0x45, &&i0x46, &&i0x47,
       &&i0x48, &&i0x49, &&i0x4A, &&i0x4B, &&i0x4C, &&i0x4D, &&i0x4E, &&i0x4F,       
       &&i0x50, &&i0x51, &&i0x52, &&i0x53, &&i0x54, &&i0x55, &&i0x56, &&i0x57,
       &&i0x58, &&i0x59, &&i0x5A, &&i0x5B, &&i0x5C, &&i0x5D, &&i0x5E, &&i0x5F,       
       &&i0x60, &&i0x61, &&i0x62, &&i0x63, &&i0x64, &&i0x65, &&i0x66, &&i0x67,
       &&i0x68, &&i0x69, &&i0x6A, &&i0x6B, &&i0x6C, &&i0x6D, &&i0x6E, &&i0x6F,       
       &&i0x70, &&i0x71, &&i0x72, &&i0x73, &&i0x74, &&i0x75, &&i0x76, &&i0x77,
       &&i0x78, &&i0x79, &&i0x7A, &&i0x7B, &&i0x7C, &&i0x7D, &&i0x7E, &&i0x7F};

//  uword inst;
#endif
    uword inst;
    int nxt_task;
    typedef struct {word pbit; void * routine;} pj; // priority of service routines
    pj prio[]={             //arrange following tasks by priority-low to high.
                            //Defined bits can have arbitrary priorities
                            //but gaps in defined bits not allowed
        { 0, &&fetch},      //This entry always first. (Default task)
        { pcp_evt,   &&pcpcheck},
        { strobe_evt, &&strobecheck},
        { iointr_evt, &&ioicheck},
        { clock_evt, &&clocktic},
        { kbd_evt,   &&kbdcheck},
        { com_evt,   &&comcheck},
        { intr_evt,  &&intrcheck},
        { endwait_evt, &&endwait},
        { hipcp_evt, &&hipcpcheck}
        };                 //no comma on preceding entry only
    int nbits=(sizeof(prio)/sizeof(pj))-1;
    int add;      //used for decimal add=1; sub=1
    void *nexti[1<<nbits];    // No gaps in defined bits allowed
    uword curloc,t40cc,calinst;
    dword dwork;

    ACCESSES_MEMORY
    ACCESSES_REGISTERS
    {   word i;
        for (i=0;i<256;i++) {TCCizp[i]=CCizp(i); TCCizpb[i]=CCizpb(i);}
        for (i=0;i<4096;i++) {
            TCC1234[i]=((i and 1)<<3) or ((i and 0x800)>>9) or CCizp(i);}
    }
#undef CCizp
#define CCizp(x) TCCizp[(x) and 0xc1]
#undef CCizpb
#define CCizpb(x) TCCizpb[(x) and 0xc1]
#undef CC1234
#undef CC1234S
//#define CC1234 {S6CC=TCC1234[CCi and 0xfff]; if((S6CC>>2) and s6.am) TRAP43;}
#define CC1234 goto cc1234;
#define CC1234S goto cc1234s;
    if (argc lt 2) then begin
        argusage(argv[0],table);
        exit (0);
        endif
    argcheck(argc,argv,table);

    if(big) then begin
       M16B=M17;
       M19B=M20;
       M8B=0x1ff;
     endif else begin
       output(big);
       if(msz > 0x20000) msz=0x20000;
       endelse
    ALLOCATE_MEMORY(msz)
//    SEMAPHORE_INIT(KB)
    SEMAPHORE_INIT(DE)

//    output(id_KB);
//    output(id_DE);

    p_s6=&s6;
    cfginit(cfgname);
//  screen_init();  moved into cfginit
//#include "vterm.h"
    
    cp.nopcp=1;
    cp.sw.watchdog=watchdog;
    cp.sw.sense=ssw;
    cp.sw.select_address=0;          //0xf00;
    cp.data=0xf000f;
    cp.unit_address=bootaddr;
    if (addrstop ge 0) then begin
        cp.sw.select_address=addrstop; cp.sw.address_stop=1;
        SET(event,pcp_evt) SET(pcp_flag,addr_flag)
        endif else cp.sw.address_stop=0;
    PJTABLE(prio,nexti)
    (intr[0]).exists=0x38f0; (intr[2]).exists=0xf0c0;  //should be in config
    {word i;for (i=0;i<256;i++) {s6.memmap[i]=s6.memac[i]=s6.wl[i]=0;}}

    CPURESET(s6,s6psd)
    IORESET
    MEMCLEAR
    printt("sz = ") printx(sz) NEWLINE;
    LOAD(cp.unit_address,s6)
    V_CHK(SHOWREGS(s6))
//signal(SIGFPE,fpetrap);
//signal(SIGINT,ihandler);
//       signal(SIGSEGV,release_clock);
//    _go32_want_ctrl_break(1);
//    __djgpp_set_ctrl_c(0);      //treat ctrl-c as any other character
    { int buggy=0x0f3f;
    asm volatile("fldcw %0;" : : "m" (buggy): "cc");  //mask floating exceptions -8/25/09 compiler broken
        }
        coc_net_init();
        if (nclock eq 0) then begin
          activate_clock();
          atexit(release_clock);
          signal(SIGTERM,exitclean);
          endif
        main_pid=getpid();
        start_gui(&cp,&s6);

        set_tty_raw();

#ifdef TABLE
    if (xerox eq 5) begin
#define S9(h) itbl[0x##h]=itbl[0x##h+128]=&&i90x##h;
#define S5(h) itbl[0x##h]=itbl[0x##h+128]=&&i50x##h;
        S5(63) S5(76) S5(77) S5(78) S5(79) S5(7A) S5(7B) S5(7C) S5(7D)
        S5(7E) S5(7F)
        S5(28) S5(29) S5(40) S5(41) S5(60) S5(61) S5(6F)
        endif
    if (xerox eq 9) begin
        ascm=1;
        S9(10) S9(2C) // S9(63)
        S9(18) S9(1C) S9(1D) S9(1E) S9(1F) S9(40) S9(41) S9(6F)
        endif
#define NI(h) itbl[0x##h+128]=&&trap40cc1;   //no indirect
#undef NOIND
#define NOIND
    NI(40) NI(41) NI(60) NI(61) NI(02) NI(20) NI(21) NI(22) NI(23)
#endif
    V_CHK(printf("stack_init called\n");)
    stack_init(0);
    if (task_fork(1)) begin
        io_task();      //task 1 remains in io_task and
        task_kill(0);   //should never get here
        endif
    if (task_fork(2)) begin
//        io_task();      //task 2 remains in io_task and
        task_kill(0);   //should never get here
        endif
    w_task= tasksw ? 0 : -1;   // enable / disable  cpu during i/o waits
    V_CHK(printf("stack_init finished\n");)
    if(idle) goto go_pcp;
    showscreen(ttyuser);
    SET_RUN
    goto fetch;
#define RP (&R)

intrcheck:
    {
        i_states *pg = &(intr[0]);
        word bits,inh,sh,i;
        word ninh=not((s6.ii * 0x30) | (s6.ci * 0x3c0));
        word ninh1=not(s6.ei * 0xffff);
        word pw,pa,gw,ga,ninhgw;
        ibits ewait;
        word loopus=0;
        uhword ewg=ewaiting.h.g;
        ewait.w=ewaiting.w;
        gw=GROUP(ewait.h.h0);
//      if (ewait.h.h1 ne intr[gw].ewaiting) {printt("ewait error\n");}  // debug only
//      if (active.h.h1 ne intr[GROUP(active.h.h0)].active) {printt("active error\n");}
        while (ewait.w && ewait.w ge active.w) begin
            if (loopus++ gt 17) {printt("Loopus check\n") goto none_now;}
            ewait.h.h1 &= (gw ? ninh1 : ninh);
            if (ewait.h.h1 && (ewait.w gt active.w)) then begin
                    word lev=BPRIO(ewait.h.h1);
                    s6.curloc= gw ? 0x50+16*gw-lev : 0x5F-((lev-3) and 0xf);
                    goto trapint1;
                    endif
            ewg &= not(intr[gw].pbit);
            if (ewg eq 0 || ewg lt active.h.h0) break;
            gw=GROUP(ewg); ewait.h.h0=intr[gw].pbit;  //gw=GROUP(ewait.h.h0);
            ewait.h.h1=intr[gw].ewaiting;
            endwhile
        goto none_now;
    }
none_now:
        CLEAR(event,intr_evt)
        goto *nexti[event];
htrapint:
    if (h40) goto itrap;
    if (verbose and 64) then begin
        printt(" trapping - loc ") printxv(s6.curloc) printt(" s6.ia = ") printxv(s6.ia) NEWLINE
        endif
trapint:
    s6.ia--; s6.ia &= M17;    //common code now?
trapint1:
    s6.cc=S6CC;
    {word group,level;
    uword inst=MEMW(s6.curloc);
    S6CC=s6.cc;
    LAA
    switch (OPCODE(inst)) {
#include "insttrap.h"
        endswitch
    if (s6.curloc lt 0x50) goto *nexti[event]; // was trap, so exit
    group=(s6.curloc ge 0x60) ? s6.curloc/16-4: 0;
    level=0x8000 >> (group ? (s6.curloc and 0xf) : (s6.curloc-2) and 0xf);
    CLEAR((intr[group]).triggered,level)    //single inst auto arm and clear
    SET((intr[group]).armed,level and (intr[group]).exists)
    ACTIVECHG(intr[group])
    NEXTI
    goto intrcheck;     //check if any more pending
    }
trap40cc1: TRAP40CC1
clocktic:
    itic++;
    CLEAR(event,clock_evt)
    (intr[0]).triggered|=(intr[0]).armed and 0xF000;  //trigger clock1-clock4 if armed
    ACTIVECHG(intr[0])
    if (thead) chk_io();
//    if (xmittic) if(itic gt xmittic) flush_pkt_out();
    goto *nexti[event];
itrap:
    showscreen(pcpuser);
    if (s6.curloc lt 0x50) {printt("Trap ")} else {printt("Interrupt ")}
    printxv(s6.curloc)
    if (s6.curloc eq 0x40) {printt(" s6.t40cc = ") printxv(s6.t40cc)}
    NEWLINE goto halt;
watchdog:
    s6.cc=S6CC;
    if (!cp.sw.watchdog) TRAP(46);
    showscreen(pcpuser);
    printt("Watchdog Timer Trap ") goto halt;
wait:
    s6.cc=S6CC;
    showscreen(pcpuser);
    printt("Wait ") printt("  ")
    SHOWREGS(s6)
    dumpb(s6.ia);
    goto go_pcp;

halt:
    s6.ia--;
no_decr:
    s6.cc=S6CC;
    showscreen(pcpuser);
    SHOWREGS(s6)
    dump((s6.ia and 0x1fff8)-8,0x40);
go_pcp:
    showscreen(pcpuser);
    s6.cc=S6CC;
    //wind down dos io before pcp
    while (io_task_busy) begin
        vterm_printf(pcpuser,"Winding down i/o\n");
        if (event and endwait_evt) begin
            CLEAR(event,endwait_evt);
    //      task_switch(1);
            nxt_task=BPRIO(task_ready);
            if (nxt_task) begin
                BITCLEAR(nxt_task-1,task_ready)
                if (cpu_stack[nxt_task]) task_switch(nxt_task); else {vterm_printf(pcpuser,"## ");}
                endif
            endif
        goto go_pcp;
        endwhile;
    GPSD(s6psd,s6)
    cp.data=*pRxMEM3(s6.ia,word);     //get data lights
    cp.idle=1;
//  output(itic);
    SET_WAIT
    stop_clock();
    printf("Idle\n\r");
    sleep(10000000);
    printf("Woke up\n\r");
    start_clock();  
//    pcp(&cp,&s6);
    SET_RUN
    if (pcp_flag || cp.sw.address_stop) then SET(event,pcp_evt) else
        CLEAR(event,pcp_evt)
    cp.idle=0;
    GPSD(s6psd,s6); BPSD(s6,s6psd);
    LAA
    goto *nexti[0];          //force at least one instruction
hipcpcheck:
    V_CHK(printt("hi pcp") NEWLINE)
    CLEAR(event,hipcp_evt)
    goto go_pcp;
pcpcheck:
    if (cp.sw.address_stop) then begin
    //  printt("addrstp checking s6.ia, cp.sw.select_address ") printx(s6.ia) printt(" , ") printx(cp.sw.select_address) NEWLINE
        if (s6.ia eq cp.sw.select_address) then begin
            V_CHK(printt("Address Stop") NEWLINE)
            goto go_pcp;
            endif
        endif
	
    if (pcp_flag eq 0) then begin
        goto fetch;  //get out quickly (assume pcp is lowest priority)
        endif
    if (pcp_flag and cntrl_c_flag) then begin
        CLEAR(pcp_flag,cntrl_c_flag)
        goto go_pcp;
        endif
	    
    if (pcp_flag and step_flag) then begin
        if (stepuntil lt 0) then begin
            if (s6.ia eq -stepuntil) then goto fetch;
                else goto go_pcp;
           endif else if (stepuntil gt 0) then  stepuntil--;
        if (stepuntil gt 0) then goto fetch;
        if (pcp_flag eq 0) then CLEAR(event,pcp_evt)
        goto go_pcp;
        endif
    if (pcp_flag and rec_flag) then begin
        V_CHK(printt("Record Stop") NEWLINE)
        goto go_pcp;
        endif
    goto *nexti[event & not(pcp_evt)];
ioicheck:
    iop_int();
    NEXTI
kbdcheck:
    if (kbhit()) then begin
        int k=getkey();
        if (k ge 0x168 && k le 0x171) then begin
            showscreen(k-0x168);                  //13b is F1 key
            goto kbdend;
            endif
        if (k ge 0x18b && k le 0x18c) then begin
            showscreen(k-0x18b+10);                //18b is F11 key
            goto kbdend;
            endif
        if (user lt debuguser) then begin
            switch (k) {
                case 0x178 ... 0x17a:  SET(cp.sw.sense,8>>(k-0x178)) ; break;
                case 0x110 ... 0x113:  CLEAR(cp.sw.sense,8>>(k-0x110)) ;break;
                case 0x13b:            TRIGGER(0,0x10); break;    //CP interrupt
                case 0x03:             ihandler(0); break;  // control c
                default: charin(user,k);
            endswitch
            endif else charin(user,k);
        endif
kbdend:
    CLEAR(event,kbd_evt)
    goto *nexti[event];
comcheck:
    CLEAR(event,com_evt)
//    if (com_flag) coc_com();
//    if (com_in_flag) com_in();
//    pkt_in();
    goto *nexti[event];
strobecheck:
//    lpt_strobe();
    if(strobe_flag eq 0) CLEAR(event,strobe_evt);
    goto *nexti[event and (not(strobe_evt))];
endwait:
    CLEAR(event,endwait_evt);
    nxt_task=BPRIO(task_ready);
    if (nxt_task) begin
        BITCLEAR(nxt_task-1,task_ready)
        if (cpu_stack[nxt_task]) task_switch(nxt_task); else {vterm_printf(2,"## ");}
        endif
    goto *nexti[event];

#ifdef TABLE
//goto fetch;
bvect:
//  s6.cc=S6CC;
//  asm volatile("" ::: "%eax","%ebx","%ecx","%edx","%esi","%edi");
    NEXTI

fetch:
ALABEL(fetch)
//  S6CC=s6.cc;
      if (!iirem) begin
         switch(s6.admode) {
                case 7: iiad=RxMEMMS(s6.ia,word,1,1,1,1);break;
                case 6: iiad=RxMEMMS(s6.ia,word,1,1,1,0);break;
                case 5: iiad=RxMEMMS(s6.ia,word,1,1,0,1);break;
                case 4: iiad=RxMEMMS(s6.ia,word,1,1,0,0);break;
                case 3: iiad=RxMEMMS(s6.ia,word,1,0,1,1);break;
                case 2: iiad=RxMEMMS(s6.ia,word,1,0,1,0);break;
                case 1: iiad=RxMEMMS(s6.ia,word,1,0,0,1);break;
                case 0: iiad=RxMEMMS(s6.ia,word,1,0,0,0);break;
                endswitch
      //iiad=pRxMEM1(s6.ia++,word);
        iiremx=REMwRXM(iiad,s6.ia);        //RXMREMw(iiad)-1;
        iirem=iiremx-1;
        s6.ia = (s6.ia +1) and M17;
        endif else {iiad--; iirem--; s6.ia++;}
      inst=*iiad;
fetched:
    goto *itbl[inst>>24];
    {
#include "inst.h"
#include "fltinst.h"
#include "decinstw.h"
#include "sufxinst.h"
ALABEL(cc1234s)
cc1234s: {S6CC=TCC1234[(CCi eor 1) and 0x8c1]; if((S6CC and 4) && s6.am) TRAP43;} BREAK;
ALABEL(cc1234)            //was fff
cc1234: {S6CC=TCC1234[CCi and 0x8c1]; if((S6CC and 4) && s6.am) TRAP43;} BREAK;
    }

#else
  fetch:
    { byte opcodeb;
      word eadd;
      inst=*pRxMEM1(s6.ia++,word); s6.ia &= M17;
fetched:
      opcodeb=(inst>>24) and 0x7f;
//      switch (opctype[opcodeb]) {
//   //   case 0: eadd= (word) pieax
//        case 2: NOIND; eadd=IMMED(inst); break;
//#undef IMMED
//#define IMMED(x) eadd
//#undef NOIND
//#define NOIND
//        default: break;}
      switch (opcodeb) {
#include "inst.h"
#include "fltinst.h"
#include "decinstw.h"
#include "sufxinst.h"
          default:
          unimp:
              printt("Yet Unimplemented Instruction =")
              printx(inst) printt("  ")
              goto halt;
      endswitch
    goto *nexti[event]; }
#ifndef IEAINLINE
#ifndef IEASTDFUNC
#include "ie.h"
#endif
#endif
#endif

}                               //end of main
void ihandler(int junk)
{
    if ((pcp_evt and event) && (pcp_flag and cntrl_c_flag)) then begin
         SET(event,hipcp_evt) endif
         else SET(event,pcp_evt)
    SET(pcp_flag,cntrl_c_flag)
}

void wakeup()
{
kill(main_pid,SIGRTMIN);    //wakeup with a faked clock tic signal
}

void exitclean()
{
exit (0);
}

#ifdef XSTRMOV
void xmovbr(char *a,char *b,int c)
{   movbr(a,b,c); }
void xstosbr(int a,char * b,int c)
{   stosbr(a,b,c); }
#endif

void stosb(char val, char* dest, int c)
{
  int i=0;
  char * d=dest;
  for (i=c; i ;i--) { 
    *d++ = val;
    }
//  printf("stosb called\n");
}


