#include <stdlib.h>
#include <stdio.h>
#include "sigma.h"
#include "cpu.h"
#include "vterm.h"
byte * gmem;

extern sigma6 s6;
extern verbose;
#define VERB_LEVEL V_CONFIG
typedef struct {char attr; word adr;} locentry;
typedef struct {word n; locentry e[1];} ltable;
void dumpc(word addr,word size,ltable *locs,word wwid);

void dumpc(word addr,word size,ltable *locs,word wwid)
{
    ACCESSES_MEMORY
    ACCESSES_REGISTERS
    word i,j,atr;
    word norm=0;        //was LIGHTGRAY;
    word stop=addr+size;
    i=addr and 0xfffff8;
restart:
    for (; i<stop; i++) do begin
        if ((i and (wwid-1)) eq 0) then begin
            printf("\n%.5lX:",i and -wwid); //fflush(stdout);
            endif else
        if ((i and 3) eq 0) then printf(" ");
        if (i ge addr) then begin
            atr=norm;
            for (j=0; j<locs->n; j++) do begin
                if (i eq locs->e[j].adr) then begin
                    atr=locs->e[j].attr; break; endif
                endfor
//          fflush(stdout);
            norm=vterm_attr(pcpuser,atr); printf(" %.8lX",*RxMEMMS(i,word,3,s6.mm,s6.ms,0)); vterm_attr(pcpuser,norm);
//          fflush(stdout);
            endif else printf("         ");
        endfor
    printf("\n");
    return;
trapint: if(s6.t40cc eq 4) goto trap40cc2;
         if(s6.t40cc eq 2) goto trap40cc4;
trap40cc2: printt(" nonexistent memory location ") printxv(i) NEWLINE
    if (i lt 0) goto chkr; else return;
trap40cc4: printt(" cannot access location ") printxv(i) NEWLINE
chkr:
    if ((i=((i+512) and -512)) lt stop) goto restart;
}
dump(word addr,word size)
{
    ACCESSES_MEMORY
    word i;
    word stop=addr+size;
    i=addr and 0xfffff8;
    for (; i<stop; i++) do begin
        if ((i and 7) eq 0) then begin
            printf("\n%.5lX:",i and 0xffff8);
            endif else
        if ((i and 3) eq 0) then printf(" ");
        if (i ge addr) then printf(" %.8lX",MEMW(i));
            else printf("         ");
        endfor
    printf("\n");
}
dumpb(word addr)         //dump bracketing
{   word wwid=8;
    ltable ltab;
    locentry xx[4];
    ltab.n=1;
    ltab.e[0].adr=addr;
    ltab.e[0].attr=31;  //YELLOW;             //yellow
    dumpc(((addr-8) and 0x1fff8)-8,0x40,&ltab,wwid);
}

void memory(int siz)
{
    byte * mem;
    mem=(char *)(((word) calloc(siz*4L+2047,1)+2047) and 0xfffff800);  //ensure page boundary
    mem=mem+4*siz;
//    printf("rem = %ld\n",RXMREMb(mem-4*siz));
    gmem=mem;    //globally available pointer
// vterm not yet init here, crash when printf uses vterm_printf
//    V_CHK(printf("rem (0) = %ld\n",REM(&(SLOC(mem,0,word)),siz,1));)
//  printf("m=%lx\nsize=%ld\n",m,sz);
    sz=siz;
    szword=siz;
    szdword=siz>>1;
    szhword=siz<<1;
    szbyte=siz<<2;
}

dumpr(word rp, word *regs)
{
    word i;
    printt("Register Block ")
    printf("%.2X",rp);
    for (i=0;i<16;i++) do begin
        if ((i and 7) eq 0) then begin
            NEWLINE printf("%.2lX:",i);
            endif else
        if ((i and 3) eq 0) then printf(" ");
        printx(regs[15-i+16*rp])
        endfor
    NEWLINE
}

