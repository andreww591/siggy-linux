#define and &
#define or |
#define xor ^
#define eor ^
#define not ~              //ones complement     ! for (0 vs 1)
#define eq ==
#define ne !=
#define gt >
#define lt <
#define ge >=
#define le <=
#define mod %
#define begin {
#define endif }
#define endfor }
#define endelse }
#define endswitch }
#define endwhile }
#define do
#define then
#include "gspio.h"
#define nel(x) (sizeof(x)/sizeof(x[0]))
#define foreach(x,i) {int i;for (i=0;i<nel(x);i++) {
#define endforeach }}
#define BPRIO(i)  ({int _p; unsigned int _i=(i); asm("mov $-1,%0; bsr %1,%0; " : \
    "=&r" (_p) : "r" (_i) : "cc" ); _p+1;})
//#define BFPRIO(i) ({word _p; uword _i=(i); asm("mov $-1,%0; bsf %1,%0; " : \
    "=&r" (_p) : "g" (_i) : "cc" ); _p+1;})

#define BFPRIO(i) ({int _p; unsigned int _i=(i); asm("mov $-1,%0; bsf %1,%0; " : \
    "=&r" (_p) : "r" (_i) : "cc" ); _p+1;})
//#define ENABLE   /* asm volatile("sti  ;" ::: "cc" ); */
//#define DISABLE  /* asm volatile("cli  ;" ::: "cc" ); */


#define outp(p,v) ({int _p=(p); int _v=(v); \
    asm volatile("outb %%al,(%%dx);" :: "a" (_v), "d" (_p) : "cc" );})
#define inp(p) ({int _p=(p);unsigned char _i; asm volatile("inb (%%dx),%%al;" \
    :"=a" (_i):"d" (p):"cc");_i;})
#define LINK(x,t,s) if (t##tail s) (t##tail s)->t##flink=x; else t##head s=x; \
    x->t##blink=t##tail s; x->t##flink=0; t##tail s=x;
#define UNLINK(x,t,s) if (x->t##blink) (x->t##blink)->t##flink=x->t##flink; \
                                          else t##head s=x->t##flink; \
                  if (x->t##flink) (x->t##flink)->t##blink=x->t##blink; \
                                          else t##tail s=x->t##blink;

#define BITSET(bn,var) asm("bts %1,%0" : "=m" (var) : "r" (bn): "cc");
#define BITCLEAR(bn,var) asm("btr %1,%0" : "=m" (var) : "r" (bn): "cc");
#define SC(x,count)  ({unsigned _p;asm("roll %2,%1" : "=g"(_p) : "0" (x),"ci" ((char) count) : "cc" ); _p;})
// gcc optimizes following to roll for constant counts but not variable
//#define SC(x,count) ((x << count) | (x >> (sizeof(x)*8-count)))
// 486 or greater processor only for bswap
//#define htonl(w) ({unsigned _p; asm("bswap %0" : "=r" (_p) : "0" (w) : "cc");_p;})
