
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <dirent.h>
#include "sigma.h"
#include "sigmaio.h"
#include "ebcdic.h"
#include <errno.h>

#define VERB_LEVEL V_DEVICE
#undef V_USER
#define V_USER iouser
#include "vterm.h"
#define printpos(x) {printt(x) printxv(ftell(DEVICE)) printt(" last = ") printxv(dev->lastblock) printt(" next = ") printxv(dev->nextblock) NEWLINE}
#define DEVICE (dev->HOST)
#define INITIAL                       \
        DCdata * DC=dev->parent;      \
        int nrec,size;                \
    if (DEVICE eq NULL) then begin    \
        int t=HOSTdevinit(dev);        \
        if (t) then return t;         \
	endif
#define READ_SIZE(x) {word nrec=fread(&size,4,1,DEVICE); if (nrec ne 1) x;}
#define READ_PSIZE {fseek(DEVICE,-4,SEEK_CUR); READ_SIZE(return -22)}
// the "seek_cur" below is a work around for a bug which cost me a night's work!
#define NSET_NEXT {fseek(DEVICE,0,SEEK_CUR); if (size lt 0) dev->nextblock=-size+ftell(DEVICE); else dev->nextblock=0;}
#define SET_NEXT {READ_SIZE(return -22) NSET_NEXT}
#define SET_LAST {READ_SIZE(return -22) dev->lastblock=ftell(DEVICE)+size-8; \
                 fseek(DEVICE,-4,SEEK_CUR);}
#define SET_BLOCK {dev->lastblock=ftell(DEVICE); READ_SIZE(return -22) \
                 dev->nextblock=-size+dev->lastblock+4;}
#define TAPE_WRITTEN ((dev->HOSTtype eq 1) && (dev->written))
#define CLOSE_TAPE_BLOCK {word fn,lw,nbsize;byte zer[4]={0,0,0,0};     \
                         lw=ftell(DEVICE);        \
                         nbsize=dev->lastblock+4-lw;    \
                         fwrite(&nbsize,4,1,DEVICE);    \
                         lw+=4;       \
                     /*  fn=fileno(DEVICE);  */  \
                     /*  chsize(fn,lw); */       \
                         fseek(DEVICE,dev->lastblock,SEEK_SET);  \
                         fwrite(&nbsize,4,1,DEVICE);    \
                         fseek(DEVICE,lw,SEEK_SET); \
                         dev->lastblock=lw;  \
                         dev->nextblock=0;   \
                         fwrite(zer,4,1,DEVICE); \
                         dev->written=0;}
extern verbose,pcp_flag,event;

int HOSTgetbuf(devdata *dev,int size)
{
    dev->buffer=(char *)malloc(size);
    dev->size=size;
    dev->orp=0;
    return 0;
}

int HOSTsetbuf(devdata *dev,int size)
{
    if (size gt 0x10000) {PCP_BREAK; return -100;}
    dev->buffer=(char *)realloc(dev->buffer,size);
    dev->size=size;
    dev->orp=0;
    return 0;
}

HOSTdevinit(devdata *dev)
{
    int ars,nrec,size,k,nexist,which;
    char * zer[4]={0,0,0,0};
    struct stat statbuf;
    char* mode;
    // char d[MAXDRIVE],p[MAXDIR],f[MAXFILE],e[MAXEXT],t[MAXPATH];
    char *etab[5]={ ".cr" , ".mt" , ".dp" , ".lp" , ".dc" };
    char *e;
    e= strrchr(dev->fid,'.');
    //which=fnsplit(dev->fid,d,p,f,e);
//    printf("HOSTdevinit looking at ext\n"); 
//    printf("is %s\n",e);
    for (k=0;k<5;k++) begin
//        printf("comparing with %s\n",etab[k]);
//        printf("%d\n", strcasecmp(e,etab[k]));
        if (strcasecmp(e,etab[k]) eq 0) {dev->HOSTtype=k;  goto typed;}
        endfor
    dev->TSH_status|=TSH_notop;
    printf("not operational by ext\n");
    return -3;
typed:
//    printf("HOSTdevinit at label typed\n");
    if (dev->HOSTtype eq 4) dev->HOSTtype=2;   //treat .dc like .dp
    nexist=stat(dev->fid,&statbuf);
    if (nexist eq -1) then begin
       printf("Error return from stat called with ");
       outputs(dev->fid);
       outputxs(dev->fid);
       perr(); NEWLINE
       endif
    if (!nexist) then begin
        switch (dev->HOSTtype) {
  /*cr*/    case 0: mode="rb";break;             //existing .cr file always readonly
  /*mt*/    case 1: if (statbuf.st_mode and S_IWUSR) {mode="r+b";dev->TDV_status=0x40;break;}
                    if (statbuf.st_mode and S_IRUSR) {mode="rb";dev->TDV_status=0;break;}
                    dev->TSH_status=TSH_notop; return -3;
  /*dp*/    case 2: if (statbuf.st_mode and S_IWUSR) {mode="r+b";break;}
                    if (statbuf.st_mode and S_IRUSR) {mode="rb";break;}
                    dev->TSH_status|=TSH_notop; return -3;
  /*lp*/    case 3: mode="r+b";
            endswitch
        endif else begin
        mode="w+b";
        if (dev->HOSTtype eq 1) dev->TDV_status=0x40;
        endelse
    dev->fileno=dev->recno=dev->lastblock=dev->nextblock=dev->written=dev->fpos=0;
    V_CHK(outputs(mode));
    DEVICE = fopen(dev->fid,mode);
    if (DEVICE eq NULL) then begin
        V_CHK(printt("Unable to open file") NEWLINE)
        dev->TSH_status=0x20;     //device not operational
        return -3;
        endif else V_CHK(printt("opened file ") printt(dev->fid) NEWLINE)
    if (dev->HOSTtype eq 1)
        if (nexist) begin
            fwrite(zer,4,1,DEVICE); //initial "mark"
            dev->fpos=4; dev->written=0;
        endif else begin
        READ_SIZE(return -4)
        if (size le 0) then begin
            NSET_NEXT
            endif else begin
            dev->HOSTtype=0;               /*cr type file*/
            fseek(DEVICE,0,SEEK_SET);
            endelse
        endelse
    return 0;
}

int HOSTdevrewind(devdata *dev)
{
    INITIAL
    dev->recno=dev->fileno=0;
    if (TAPE_WRITTEN) CLOSE_TAPE_BLOCK
    fseek(DEVICE,0,SEEK_SET);
    if (dev->HOSTtype eq 1) then begin
        dev->lastblock=0;
        SET_NEXT
        endif
    DV_CHK(printpos(" post rewind fpos = "))
    return 0;
}
int HOSTdevspacefile(devdata *dev)
{
    INITIAL
    dev->recno=0;
    dev->fileno++;
    if (dev->HOSTtype ne 1) then return -3;
    DV_CHK(printpos(" sff pre fpos = "))
    if (TAPE_WRITTEN) return -18;
    fseek(DEVICE,dev->nextblock,SEEK_SET);
    READ_SIZE(return -4)            //read prev mirrored block size
    dev->lastblock=ftell(DEVICE);
    READ_SIZE(goto eot)
    NSET_NEXT
    DV_CHK(printpos(" sff post fpos = "))
    return -32;    //Tm without uend
eot:
    dev->nextblock=0;
    return -17;       //end of written part of tape
}

int HOSTdevspacefileb(devdata *dev)
{
//    int nrec,size;
    INITIAL
    dev->recno=0;
    dev->fileno--;
    DV_CHK(printpos(" sfb pre fpos = "))
    if (dev->HOSTtype ne 1) then return -3;
    if (TAPE_WRITTEN) CLOSE_TAPE_BLOCK;
    if (dev->lastblock lt 4) {fseek(DEVICE,dev->lastblock,SEEK_SET); return -21;}
    dev->nextblock=dev->lastblock-4;
    fseek(DEVICE,dev->nextblock,SEEK_SET);
    SET_LAST
    DV_CHK(printpos(" sfb post fpos = "))
//  return 0;
    return -32; //try this to avoid error message
}

int HOSTdevwrite(devdata *dev)
{
    INITIAL
    DV_CHK(printpos("HOSTdevwrite called fpos = ") )
    dev->recno++;
    if (dev->HOSTtype le 1) {fwrite(&dev->ars,4,1,DEVICE);
        DV_CHK(printpos(" post size write fpos = ") )}
    fwrite(dev->buffer,dev->ars,1,DEVICE);
    if (dev->HOSTtype eq 1) {fwrite(&dev->ars,4,1,DEVICE); dev->written=1;}
    if (dev->HOSTtype eq 3) fwrite("\n",1,1,DEVICE);
    return 0;
}
int HOSTdevtapemark(devdata *dev)
{
    INITIAL
    DV_CHK(printt("HOSTdevtapemark called recno = ") printd(dev->recno))
    DV_CHK(printt(" fileno = ") printd(dev->fileno) NEWLINE)
    dev->fileno++;
    if (dev->HOSTtype eq 1) CLOSE_TAPE_BLOCK
    return -32;
}

int HOSTdevseek(devdata *dev)
{   word seekadd;
    INITIAL
    if (dev->HOSTtype eq 2) begin
        if (dev->ssize gt 4) begin
            word *sense=(word *) dev->sense;
            seekadd= ((*sense >>16) * dev->heads * dev->nspt +
                ((*sense and 0xff00)>>8) * dev->nspt +
                (*sense and 0xff)) * 1024;
            endif else begin
            hword *sense=(hword *) (dev->sense+2);
            seekadd = ((((*sense )>> 4) * dev->nspt) + ((*sense and 0xf))) *1024;
            DV_CHK(printt(" sense = ") printxv(*sense) printt(" seekadd = ") printxv(seekadd) NEWLINE)
            endelse
        dev->recno=seekadd;
        fseek(DEVICE,seekadd,SEEK_SET);     //offset from bof
        if (ftell(DEVICE) ne seekadd) PCP_BREAK;
        endif
}
int HOSTdevread1(devdata *dev)
{   //int recs;
    INITIAL
    nrec=fread(dev->buffer,1024,1,DEVICE);
    if (nrec ne 1) return -1;
    return 0;
}

int HOSTdevreadrev(devdata *dev)
{   word status;
    INITIAL
    status= HOSTdevsrb(dev);
    if(status) return status;
    status= HOSTdevread(dev);
//  if(status) return status;
    HOSTdevsrb(dev);
    return status;
}


int HOSTdevsrb(devdata *dev)
{
//  DCdata *DC=dev->parent;
//    int nrec,size;
    INITIAL
    switch (dev->HOSTtype) {
/*.mt*/ case 1: if (ftell(DEVICE) le 8) return -21;     //BOT
            DV_CHK(printpos(" srb pre fpos = "))
            READ_PSIZE
            DV_CHK(printt(" prev rec size = ") printd(size) NEWLINE)
            if (size le 0) begin
                fseek(DEVICE,-8,SEEK_CUR);
                dev->nextblock=ftell(DEVICE);
                READ_SIZE(return -22)
                dev->lastblock=dev->nextblock+size-4;  //size is neg
                fseek(DEVICE,-4,SEEK_CUR);
                dev->fileno--;
                dev->recno=0;
                DV_CHK(printpos(" srb (-16) final pos "))
                return -16;
                endif
            size=size and 0xffffff;
            fseek(DEVICE,-size-8,SEEK_CUR);
            DV_CHK(printpos(" srb (0) final pos = "))
            break;
        default: return -20;
        endswitch
    dev->recno--;
    return 0;
}
int HOSTdevread(devdata *dev)
{
//    DCdata *DC=dev->parent;
//    int nrec,size;
    int errf=0;
    INITIAL
    V_CHK(printt("HOSTdevread called\n"));
    switch (dev->HOSTtype) {
/*.cr*/ case 0: nrec=fread(&size, 4, 1, DEVICE);
                 if (nrec ne 1) then begin
                    V_CHK(printt(dev->name) printt(" - EOF after record ")
                        printd(dev->recno) NEWLINE)  return -2; endif
                if (size le 0) return -5;   //set DTE here?
		dev->recno++;    CHKHOSTBUF(size)	
                nrec=fread(dev->buffer,size,1,DEVICE);
                if (nrec ne 1) then begin
                    V_CHK(printt("truncated record at record number ") 
		        printx(dev->recno) NEWLINE)  return -2; endif  //need to set uend here
                dev->ars=size; break;
/*.mt*/ case 1: READ_SIZE(return -2)
                if (size le 0) then begin
                    DV_CHK(printpos(" read - neg size (tapemark) "))
                    if (ftell(DEVICE) ne dev->nextblock+4) then begin
                        DV_CHK(printt("Bad tape block fpos = ") printxv(ftell(DEVICE)) NEWLINE)
                        return -6;
                        endif
                    READ_SIZE(return -4)
                    dev->lastblock=ftell(DEVICE)-4;
           //       if (size ne 0)
                    dev->nextblock=-size+dev->lastblock+4;    //next file mark
           //           else dev->nextblock=0;
                    dev->fileno++;
                    dev->recno=0;
                    DV_CHK(printpos(" read (tapemark -16) final "))
                    return -16;   //read tapemark
                    endif
                dev->recno++;
                errf=size>>24;
                size=size and 0xffffff;
                CHKHOSTBUF(size)
                nrec=fread(dev->buffer,size,1,DEVICE);
                if (nrec ne 1) then begin
                    V_CHK(printt("truncated record at record number ") printx(dev->recno) NEWLINE)
                    return -2;      //need to set uend here
                    endif
                dev->ars=size;
                READ_SIZE(return -22)
                size=size and 0xffffff;
                if ((size and 0xffffff) ne dev->ars) then begin
                    V_CHK(printt("Bad reverse record length") NEWLINE)
                    //report Transmission data error here
                    endif
                break;
        default:
         ;
	endswitch
     V_CHK(printt(dev->name) printt(" record ") printd(dev->recno) printt(" read - ")
     printt(dev->fid) NEWLINE)
     if ((dev->stop gt 0) && (dev->stop eq dev->recno)) then begin
        SET(event,pcp_evt) SET(pcp_flag,rec_flag)
        endif
     dev->ars=size;  dev->orp=0;      //set output removal pointer
     return errf;
}

