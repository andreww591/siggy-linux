#include <stdio.h>
#define printc(k) printf("%c",(char) k);
#define printt(y) printf(y); //cputs(y) doesnt cr for nl
#define printx(y) printf(" %.8lX",(long) y);
#define printxv(y) printf(" %lX",(long) y);
#define printd(y) printf(" %ld",(long) y);
#define printdw(y) printf(" %.16LX",(long long) y);
#define printo(x) {char *oo[2]={" off "," on "}; printf(oo[x and 1]);}
#define NEWLINE printf("\r\n");
#define printidata(b,d,n,fmt,width,bw,typ)      \
        {int _i,_j;                         \
         for (_i=0,_j=0;_i<n;_i++,_j++)     \
            {if (_j>=width) {_j=0; NEWLINE; \
                {int _jj;                   \
                 for (_jj=0;_jj<bw;_jj++) printf(" ");  \
                 }                                      \
            }                                           \
        printf(fmt,*(((typ*)(b))+d+_i));}} NEWLINE
#define outputxb(z) {printf( #z " ="); printidata(&z,0,sizeof(z)," %.2X",16,sizeof( #z " =")-1,unsigned char)}
#define outputib(z) {printf( #z " ="); printidata(&z,0,sizeof(z)," %3d",8,sizeof( #z " =")-1,unsigned char)}
#define outputiw(z) {printf( #z " ="); printidata(&z,0,sizeof(z)/4," %7d",8,sizeof( #z " =")-1,int)}
#define outputxs(z) {printf( #z " ="); printidata(z,0,strlen(z)," %.2X",16,sizeof( #z " =")-1,unsigned char)}
#define Q(x) #x
#define QB(x) Q(\)##x
#define QS(x) #x " = " QB(x\##"")
//#define QS(x) #x " = " Q(\)##x##\##""   //doesnt expand macro x
#include <stdarg.h>
#define TYPEOF(x) ({__builtin_classify_type (* (typeof(x)*) 0);})
//#define TYPEO(x) ({__builtin_classify_type ( (typeof(x)) 0);})
//#define TYPE(x) ({__builtin_classify_type (*(x));})
#define output(x) printf( #x " = %ld\n",(long) x)
#define outputx(x) printf( #x " = %.*LX\n",sizeof(x)*2,(long long) x)
#define outputvx(x) printf( #x " = %LX\n",(long long) x)
#define outputnl(x) printf( #x " = %ld ",(long) x)
#define outputxnl(x) printf( #x " = %.*LX ",sizeof(x)*2,(long long) x)
#define outputvxnl(x) printf( #x " = %LX ",(long long) x)
#define outputf(x) printf( #x " = %f\n",(float) x)
#define outputs(x) printf( #x " = %s\n", x)
#define LINE printf("at line %d in file %s\n\r",__LINE__,__FILE__);
