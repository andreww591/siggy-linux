
//-----------command line option variables----------------------------
word bootaddr=0x003;
word ssw=0;
char *cfgname="sigma.cfg";
word addrstop=-1;
word watchdog=0;
word nclock=0;
word idle=0;
word delta=0;
word verbose=0;
word h40=0;
word msz=0x20000;
word xerox=6;
word tasksw=1;
word pcpwait=0;
word limit=0;
word big=1;   // default to BIG
//---------------------

i_states intr[17];   //extra group allows .grp indexing
ibits active,waiting,ewaiting;

int itic=0;
int stepuntil=0;
int br_stack[16],br_count;
SEMAPHORE_DATA(DE);
SEMAPHORE_DATA(KB);      //semaphore for keyboard
char anlz[128]= { 9,9,9,9,8,8,8,8, [8 ... 0x1f] = 12,
                  9,9,9,9, [0x24 ... 0x3f] = 8,
                  1,1,1,1, [0x44 ... 0x4f] = 8, [0x50 ... 0x5f] = 4,
                  1,1,1,1, [0x64 ... 0x6f] = 8, [0x70 ... 0x7f] = 0 };
char opctype[128]= { 9,9,2,9,8,8,8,8, [8 ... 0x1f] = 12,
                     2,2,2,2, [0x24 ... 0x3f] = 8,
                     2,2,1,1, [0x44 ... 0x4f] = 8, [0x50 ... 0x5f] = 4,
                     2,2,1,1, [0x64 ... 0x6f] = 8, [0x70 ... 0x7f] = 0 };


byte TCCizp[256];
byte TCCizpb[256];
byte TCC1234[4096];
CMDNAMES
sigma6 s6;
psd s6psd;
s6pcp cp;
sigma6 *p_s6;
byte ascm=0;          //ascii mode option - enable for sig9
#include <string.h>
#include "argcheck.h"
argtab table[]={
    {'b',&bootaddr,Hex,   "Unit Address - boot device address (hex) default = 3"},
    {'s',&ssw,     Hex,   "Sense switch settings              (hex) default = 0"},
    {'n',&nclock,  Switch,"No clock interrupts (for debugging)"},
    {'c',&cfgname, String,"File name with Sigma Configuration default sigma.cfg"},
    {'a',&addrstop,Hex,   "Address stop enabled at address    (hex) default off"},
    {'w',&watchdog,Switch,"Watchdog timer override on               default off"},
    {'h',&h40,     Switch,"Halt on traps                            default off"},
    {'i',&idle,    Switch,"Idle at startup"},
    {'m',&msz,     Hex,   "Memory size                   (hex) default 20000"},
    {'p',&pcpwait, Switch,"PCP if wait while no interrupts enabled  default off"},
    {'t',&tasksw,  Switch,"Task switch (CPU runs while disk I/O)    default on"},
    {'d',&delta,   Switch,"Debug without pcp display"},
    {'v',&verbose, Hex,   "Verbosity level (hex), bits turn on debugging messages:\n\
              1: device  2: controllers  4:iop   8:cpu   16:config "},
    {'x',&xerox,   Hex,   "Xerox Sigma CPU type                     default 6"},
    {'g',&big,     Switch,"biG (system mods for memory > 128K       default on"},
    {'l',&limit,   Int,   "Limit console output # of characters     default 0 (unlimited)"},
    {'f',0,        File,  "File to read command line options"},
    {0,0,Last,"Usage: %s"}};



//   globals from pcp.c
int pcp_flag=0;
int showcpu=0;
//int scroll=1;
#define NH 30       //number of history command lines to keep
#define SCB 80
int hcmdii=0,hcmdor=0;
char hcmdbuf[NH*SCB];
char pcmdbuf[SCB];
int cbii=0,cbl=0,cbc=0,edited=0,insmode=0;
//  globals from screen.c
int user=0;
