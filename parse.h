typedef struct tag_parse_table {
        char *whites;
        char *delims;
        char *quotes;
        char escape;
    } parse_table;

//useage parse_table {{ " /t"},{"\n+-*/=<>"},{"'\""},{'\\'}} ptab;
int parse(unsigned inflag,char *token,int tokmax,char *line,parse_table* ptab,
    char *brkused, int *next,char * quoted);


//  char whites[]={" /t"};
//  char delims[]={"\n+-*/=<>"};
//  char quotes[]={"'\""};
//  char escape='\\';

