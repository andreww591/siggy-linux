#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>      
#include "idlop.h"
#include <pthread.h>
#include <stdlib.h>    //(exit)
#include <string.h>    //(strlen)
#include <signal.h>
#include "sigma.h"
#include "sigmaio.h"
#include "coc.h"

#define errif(x,y)   if (x) {perror(y);set_tty_raw();exit(1);}
int port = 51664;
void * talk(void * );
void pipe_handler();
struct sigaction sa;
extern devdata * cocdev[];
extern int cocln[];
extern cocdata* cocs[16];
#include "rvolatiles.h"

void * net_init();


void coc_net_init()
{
  int rc;
  pthread_t pidk;
    pthread_create(&pidk,NULL,net_init,NULL);
    id_kid[nkids++]=pidk;
}

void * net_init()
{
  struct sockaddr_in sin;
  struct sockaddr_in pin;
  int sock_id;
  int temp_sock_id;
  int address_size;

  int i, len, rc;
  pthread_t  pidk;
  char welcome[]="\n\rWelcome to siggy \n\r";
  char ttypreq[]="Terminal is ";
  char tbuf[80];

  sock_id = socket(AF_INET, SOCK_STREAM, 0);
  errif ((sock_id eq -1) ,"create socket");

  bzero(&sin, sizeof(sin));
  sin.sin_family = AF_INET;
  sin.sin_addr.s_addr = INADDR_ANY;
  sin.sin_port = htons(port);
  bzero(&pin,sizeof(pin));
  pin.sin_family = AF_INET;
  pin.sin_addr.s_addr = INADDR_ANY;
  pin.sin_port = htons(port);

  rc=bind(sock_id, (struct sockaddr *)&sin, sizeof(sin));
  errif((rc eq -1),"bind error");

  rc=listen(sock_id, 20);
  errif((rc eq -1),"listen error");
  printf("Accepting connections port %d...\r\n",port); 
  sa.sa_flags = SA_SIGINFO ;  //| SA_RESTART;
  sa.sa_sigaction = pipe_handler;
  sigemptyset(&sa.sa_mask);
  if (sigaction(SIGPIPE, &sa, NULL) == -1)
     perror("activate coc pipe sigaction");
  while(1) {
//    sigset_t mask;
    int line;
    address_size=sizeof(pin);
    temp_sock_id = accept(sock_id, (struct sockaddr*)&pin,&address_size);
    errif ((temp_sock_id eq -1),"call to accept");
    send(temp_sock_id,ttypreq,strlen(ttypreq),0);
    usleep(20000);
    rc=recv(temp_sock_id,tbuf,10,MSG_DONTWAIT);
    if (rc gt 0) {tbuf[rc]=0; send(temp_sock_id,tbuf,rc,0);}
//    output(temp_sock_id);
    line=assign_cocln(temp_sock_id,tbuf);
    if(line lt 0) {
       sprintf(tbuf,"\n\rNo coc line available for that terminal type\n\r");
       send(temp_sock_id,tbuf,strlen(tbuf),0);
       close(temp_sock_id);
       continue;
    }
//    outputx(line);
    send(temp_sock_id,welcome,strlen(welcome),0);
    sprintf(tbuf,"Connected to %s line %d \r\n",(cocs[line >>16]->dev)->name,line & 0xff);
    send(temp_sock_id,tbuf,strlen(tbuf),0);
    rc=pthread_create(&pidk,NULL,talk,&temp_sock_id);
    }
}

void pipe_handler()
{
//task_kill(
}
void socketout(char fd, int data)
{

int ifd=fd;
   if(ifd){
      send(ifd, &data, 1, 0);
   }
}


void * talk(void * arg)
{
int * psk_id= (int*) arg;
int sk_id=* psk_id;
int len,i,rc,ichar;
char buf[50];
int mycoc,myline;
cocdata * c;
devdata *dev;
//     output(sk_id);
     mycoc=cocln[sk_id]>>16;
     myline=cocln[sk_id] and 0xff;
     dev=cocdev[mycoc];
     c=cocs[mycoc];
     while(1) {
        rc=recv(sk_id, buf, 50, 0);
	if(rc le 0) {goto endthread;}           //,"recv error");

//        printf("%d : %c \n",sk_id,buf[0]);
        ichar=buf[0];
        dev_ME_charin(dev, ichar, myline);

//        len = rc;          //strlen(buf);
//        for (i=0; i<len; i++) buf[i] = toupper(buf[i]);
    
//        rc=send(sk_id, buf, len, 0);
//        if(rc eq -1) {goto endthread;}           //  "call to send");

      }
endthread:
     c->dsr[myline]=0;
     c->fd[myline]=0;
    close(sk_id);
}
