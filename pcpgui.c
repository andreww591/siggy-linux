
#include "widgets.h"
#undef MAX
#undef MIN

#include "opts.h"
#include "sigma.h"
#include "cpu.h"
#include "sigmaio.h"
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include "parse.h"
#include "coc.h"
#include "vterm.h"
#include <string.h>
//#include <stdlib.h>
#include <pthread.h>
#include <signal.h>


#include "rglobals.h"
#include "rvolatiles.h"

void printit(wtree * t)
{
if (t->name) printf("name is %s\n\r",t->name);
}

enum dataintem {NONEL,WATCHDOG,SENSE,SELADD,ADDSTOP,DATA,DATAL,
         PWRL,CPURL,IORL,LOADL,SYSRL,NORMALL,RUNL,WAITL,INTERRUPTL,
         PSD1,PSD2,INSERTPSD,INSTADD,STORESW,DISPLAYSW,DATASW,COMPUTESW};

int btable []={ SELADD, -1, WATCHDOG, -1, -1, -1, -1, -1, ADDSTOP, SENSE, RUNL};
int ptable []= {PSD1, PSD2};

wtree * lup[INSERTPSD*32];
void bval(GtkWidget *, wtree *);


void invert(wtree *t)
{
   int item=t->idata;
   int ix=item*32;
   if ((item ge WATCHDOG) and (item le PSD2)){
      while (lup[ix]) ix++;
      lup[ix]=t;
      }
}


int tree_walk(wtree * root, void (*func)(wtree *))
{  int i=0;
//   int cl=0;
   wtree * cur=root;
next:
   i=i+1;
   func(cur);
   if (cur->child_last) {
       cur=cur->child_last; //cl++;
       goto next;
       }
sibling:
   if(cur ne root) {
       if (cur->prev) {
           cur=cur->prev;
           goto next;
           }
       if (cur->parent ne root) {
           cur=cur->parent; //cl--;
           goto sibling;
           }
       }
return i;
}

void * pcp_gui(void *);


typedef struct tag_system6 {
   sigma6  * ps6;
   s6pcp * ps6pcp;
   } system6;

typedef struct tag_ugui {
   system6 * live;
   system6 * disp;
} ugui;

psd d6psd;           //fix me to not global


//guint update_display(ugui * ug)
gboolean update_display(gpointer ug)
{
   system6 * disp = ((ugui *)ug)->disp;     //our copy - has display state
   system6 * live = ((ugui *)ug)->live;     //running system 
   sigma6 * ls6 = live->ps6;      // pointer to live s6
   s6pcp * ls6pcp = live->ps6pcp; // pointer to live pcp 
   sigma6 * ds6 = disp->ps6;      // pointer to our displayed copy of s6
   s6pcp * ds6pcp=disp->ps6pcp;   // pointer to our displayed copy of pcp
   union {
   s6pcp  diff;                   // differences in live vs disp (to be updated)
   char   b[0];
   unsigned int w[0];             // allows access to diff pcp as word array
   } udiff;
   int i;
   psd s6psd,pdiff;
//   union {
//   psd  diff;
//   unsigned int w[0];
//   } pdiff;

   for (i=0;i<sizeof(btable)/4;i++) {
      udiff.w[i] = ((unsigned int *)ls6pcp)[i] eor ((unsigned int *)ds6pcp)[i];
      if(udiff.w[i] ne 0) {                          //need updating?
         unsigned int dd=udiff.w[i];                 //yes - these bits
         int val=(((unsigned int *) ls6pcp)[i]);     // to these new values
         if (btable[i] ge 0) {                       // if we really care
             while (dd) {
               int bit=BPRIO(dd);                     //bit to update now
               int j=32*btable[i]+bit-1;              //index to widget table
               int val1=(val>>(bit-1)) and 1;         //new value for this widget
               GtkWidget * wg = lup[j]->me;           //widget for this bit
	       g_signal_handlers_block_matched (G_OBJECT (wg),  //no signals
                    G_SIGNAL_MATCH_FUNC, 0, 0, NULL, bval, wg); // while updating
               gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(wg), val1);	
	       g_signal_handlers_unblock_matched (G_OBJECT (wg), //resume signals
                    G_SIGNAL_MATCH_FUNC, 0, 0, NULL, bval, wg);
               dd ^= 1<<(bit-1);
                 }
         }
         ((unsigned int *) ds6pcp)[i] ^= udiff.w[i];  //update our copy
      }
    }
    GPSDP(s6psd,ls6);
    for (i=0;i<2;i++)  {
       pdiff.wd[i] = s6psd.wd[i] eor d6psd.wd[i];
       if(pdiff.wd[i] ne 0) {
           unsigned int dd=pdiff.wd[i];
           int val= s6psd.wd[i];
           while (dd) {
               int bit=BPRIO(dd);
               int j=32*ptable[i]+bit-1;
               int val1=(val>>(bit-1)) and 1;
               GtkWidget * wg  = lup[j]->me;
	       g_signal_handlers_block_matched (G_OBJECT (wg),  //no signals
                    G_SIGNAL_MATCH_FUNC, 0, 0, NULL, bval, wg); // while updating
               gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(wg), val1);	
	       g_signal_handlers_unblock_matched (G_OBJECT (wg), //resume signals
                    G_SIGNAL_MATCH_FUNC, 0, 0, NULL, bval, wg);
               dd ^= 1<<(bit-1);
                 }
        d6psd.wd[i] ^=  pdiff.wd[i];
       }
     }
return TRUE;
}

#define IDLECHK {if (cpcp->idle ne 1) {printf("Running: switch %s not active\n", t->name); break;}}
#define NOSW  {printf("Not a switch: %s\n",t->name); break;}
#define NI  {printf("Not yet implemented: %s\n", t->name); break;}


ACTION(bval)           //binary value toggle switches
{  wtree * p=t->parent;
   int val=0;
   ACCESSES_MEMORY;
   system6 * d6= t->pdata;
   s6pcp * cpcp = ((system6 *)(t->pdata))->ps6pcp;
   sigma6 *ps6 = d6->ps6;
   sigma6 s6;     //kluge for LOAD
   p=p->child_first;
   while (p) {
         val=val+val;
         if( GTK_TOGGLE_BUTTON(p->me)->active) val=val+1;
          p=p->next;
          }
   switch(t->idata) {
      case WATCHDOG:    cpcp->sw.watchdog=val;break;
      case SENSE:       cpcp->sw.sense=val;break;
      case SELADD:      cpcp->sw.select_address=val;break;
      case ADDSTOP:     cpcp->sw.address_stop=val;break;
      case DATA:        break;
      case PWRL:        printf("Powering off\n");kill(main_pid,SIGTERM);break;
      case CPURL:       IDLECHK; NI; break;
      case IORL:        IDLECHK; IORESET; break;
      case LOADL:       IDLECHK;LOAD(cpcp->unit_address,s6);ps6->ia=s6.ia;break;
      case SYSRL:       IDLECHK; NI; break;
      case NORMALL:     NOSW;
      case RUNL:        NOSW;
      case WAITL:       NOSW;
      case INTERRUPTL:  NI; break;
      case INSERTPSD:   NI; break;
      case COMPUTESW:   if (val eq 1) wakeup(); else ihandler(0); break;
      default:          break;
     }
}
#define pb bval


//int main(int argc, char *argv[])

start_gui(s6pcp *cp, sigma6 *s6)
{ int computer;
   system6 * d_sys = malloc(sizeof(system6));
   d_sys->ps6= s6;
   d_sys->ps6pcp=cp;
   pthread_t gui_pid=0;
   int rc=pthread_create(&gui_pid, NULL, pcp_gui, d_sys);  //was cp
   id_kid[nkids++]=gui_pid;
}

void * pcp_gui(void * d6)
{
//  int xsz=640;   //main window width
//  int ysz=472;   //main window height
//  int hss=19;    //height of switch rows
//  int ybrow=209;   // y of button row (power switch etc)
#include "pcpgui.h"
  wtree *top=malloc(sizeof(wtree));   //main window
  wtree * vnext;          //main multi item container
  wtree * runst;
  system6 disp;
  ugui ugg= { d6, &disp};

  disp.ps6= malloc(sizeof(sigma6));
  disp.ps6pcp= malloc(sizeof(s6pcp));
  bzero(disp.ps6,sizeof(sigma6));
  bzero(disp.ps6pcp,sizeof(s6pcp));

  gtk_rc_parse( RCFILE );
  gtk_init(NULL,NULL);           //&argc, &argv);
  WTop(top,"SIGMA 6"); WnSize(top,640,472);
     vnext=WNew(top,gtk_fixed);   //need a multi-item container
        SMFTIButton(vnext,xyzt,bval,4,SENSE,d6);
        SMFTIButton(vnext,xyzt,bval,1,WATCHDOG,d6);
        SFTIButton(vnext,xyzt,pb,PWRL,d6);
        SFTIButton(vnext,xyzt,pb,CPURL,d6);
        SFTIButton(vnext,xyzt,pb,IORL,d6);   // io reset
        SFTIButton(vnext,xyzt,pb,LOADL,d6);

        SFTIButton(vnext,xyzt,pb,SYSRL,d6);   //sys reset
        SFTIButton(vnext,xyzt,pb,NORMALL,d6);   //normal mode
      runst=WNew(vnext,gtk_fixed);
        SFTIButton(runst,xyzt,pb,RUNL,d6);   //run
        FTIButton(runst,xyzt[WAITL].x,xyzt[WAITL].y,29,22,pb,RUNL,d6);   //wait

        SFTIButton(vnext,xyzt,pb,INTERRUPTL,d6);      //interrupt
        SMFTIButton(vnext,xyzt,bval,32,PSD1,d6);      //psd1 lamps
        SMFTIButton(vnext,xyzt,bval,32,PSD2,d6);      //psd2 lamps
        SMFTIButton(vnext,xyzt,bval,1,ADDSTOP,d6);
        SMFTIButton(vnext,xyzt,bval,17,SELADD,d6);
        SMFTIButton(vnext,xyzt,bval,32,DATA,d6);   //data val switches
        SMFTIButton(vnext,xyzt,bval,32,DATAL,d6);  //data lamps
        SFTIButton(vnext,xyzt,bval,INSERTPSD,d6);    //insert psd
        SFTIButton(vnext,xyzt,bval,INSTADD,d6);      //inst add hold/inc
        SFTIButton(vnext,xyzt,bval,STORESW,d6);      //store ia/seladd
        SFTIButton(vnext,xyzt,bval,DISPLAYSW,d6);    //display ia/seladd
        SFTIButton(vnext,xyzt,bval,DATASW,d6);       //data enter/clear
        SFTIButton(vnext,xyzt,bval,COMPUTESW,d6);    //compute switch


  bzero(lup,sizeof(lup));
  tree_walk(top,invert);

  update_display(&ugg);

  gtk_widget_show_all(top->me);
  g_timeout_add( 15,update_display, &ugg);
  gtk_main();
  return 0;
}
