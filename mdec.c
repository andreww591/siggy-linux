#include "opts.h"
#include "sigma.h"
#include "cpu.h"
extern sigma6 s6;
extern event, pcp_flag;
extern xerox;
#define mem gmem
#define reg greg

void decadd(dec *deca, byte sa, dec *decb, byte sb, int add, dec *decs, byte *pss, byte *pSC0)
{   word i;  byte SC0=0;
    byte * pdecb, * pdeca, * pdecs=&decs->b[0];
    if (add eq 0) sb=1+(sb&1);
            if (sa eq sb) {           /* assumes sa,sb preset +=2; -=1 (0=0) */
              pdecb=&decb->b[0]; pdeca=&deca->b[0];
/* s=a+b */  asm volatile("subl %%edi,%%ebx; clc; cld; \
             0: lodsb ; adcb (%%edi,%%ebx),%%al; daa; stosb; loop 0b; \
             lahf; movb %%ah,%0 "  :
                  "=m" (SC0) :
                  "S" (pdeca), "b" (pdecb),"D" (pdecs), "c" (sizeof(dec)):
                  "%eax","cc");
             *pss=sa;
             endif else {
              if ((decb->d[1] gt deca->d[1]) ||
              ((decb->d[1] eq deca->d[1]) && (decb->d[0] gt deca->d[0]))) begin
                *pss=sb;
/* s=b-a */     pdecb=&deca->b[0]; pdeca=&decb->b[0];  //swap em
                endif else begin
                *pss=sa;
/* s=a-b */     pdeca=&deca->b[0]; pdecb=&decb->b[0];  //unswapped
                endelse
                  asm volatile("subl %%edi,%%ebx; clc; cld; \
                    0: lodsb ; sbbb (%%edi,%%ebx),%%al; das; stosb; loop 0b; \
                    lahf; movb %%ah,%0 "  :
                        "=m" (SC0) :
                        "S" (pdeca), "b" (pdecb),"D" (pdecs), "c" (sizeof(dec)):
                        "%eax","cc");
                endelse
                *pSC0=SC0 and 1;  //return carry flag value
}
int decinst( uword inst)
{ //byte S6CC=s6.cc;
switch (OPCODE(inst)) {
int add;
#include "decinst.h"
endswitch
//s6.cc=S6CC;
return 0;
trapint: return -1;
}
