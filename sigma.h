//#define USE_NCURSES
//#define RXINLINE
//#define IEASTDFUNC
#include "idlop.h"
#include <sys/sem.h>
#include <sys/ipc.h>
typedef int word;
typedef unsigned int uword;
typedef short int hword;
typedef unsigned short int uhword;
typedef unsigned char byte;
typedef long long dword;
typedef unsigned long long udword;

#define ACCESSES_MEMORY  byte *mem= gmem;   //local copy of global memory pointer
#define ALLOCATE_MEMORY(x) memory(x); mem=gmem;
#define ACCESSES_REGISTERS byte *reg= greg;  //local copy of global current register block pointer
#define ALLREGS "%eax","%ebx","%ecx","%edx","%esi","%edi"
#define CALL(f,typ) ({typ _x; asm volatile("call *%1 " : "=a" (_x) :"m" (f) : ALLREGS);_x;})
#define FUNCTION(f) {void *retur; word _x;asm volatile("popl %0" : "=mr" (retur) :: ALLREGS);
#define RETURN(x) asm volatile(" " : "=a" (_x) : "a" (x) : ALLREGS);goto *retur;}
//#define NOP { asm volatile(" " : ::"%eax","%ebx","%ecx","%edx","%esi","%edi");}
#define NOP

typedef union tag_udw { dword dw; unsigned long long ud; struct {word w1; word w0;} w;} udw;

#define bin(x) ((x ge 'A') ? ((x and 7)+9) : (x and 0xf))

typedef union tag_semun {
        int val;
        struct semid_ds buf;
        unsigned short int *array;
        struct seminfo *__buf;
        } semun;


typedef union tag_ibits {
        uword w;
        struct {
          uhword h1;
          uhword h0;
          uhword g;
        } h;
} ibits;

typedef struct tag_interrupts {
        uhword exists;
        uhword armed;
        uhword enabled;
        uhword triggered;
        uhword active;      //duplicated info for speed = triggered and not armed
        uhword waiting;     //  ditto                     triggered and armed
        uhword ewaiting;
        uhword signal;      // external signal level, will immediately retrigger interrupt if it remains high
        uhword pbit;        // bit defines priority of this group
        uhword grp;         // table for group=grp[priority-1]
} i_states;


byte *gmem,*greg;
//#define sz 0x10000              //128 k (WORDS, of course!) of memory
word sz;
word szword;
word szhword;
word szbyte;
word szdword;

//Build a priority table, bitlist is an array of bit values, nbits long
//table built is 2**n entries, value is index into bitlist (1 indexing, so
//0 means none) of the highest priority bit that is set. The bitlist
//array defines the priorities, ordered lowest to highest;
#define PRIOTABLE(nbit,pbitlist,elem,table)                       \
    {int i,j;                                                               \
     int _tsz=1<<nbit;                                                       \
     pj * pbl;                                                \
     for (j=0; j<_tsz; j++) do begin                                         \
         for (i=nbit,pbl=pbitlist+nbit; i>0; i--,pbl--) do begin          \
            if (j and pbl->elem) then begin                              \
                table[j]=(void *)i;                                                 \
                goto nxtj;                                                  \
                endif;                                                      \
            endfor                                                          \
         table[j]=0;                                                        \
      nxtj: ;                                                                \
         endfor; }
#define PJTABLE(prio,jtable)        \
    {   int j;                      \
        int _nbit=(sizeof(prio)/sizeof(pj))-1;                            \
        PRIOTABLE(_nbit,prio,pbit,jtable)                                \
        for (j=0; j < (1<<_nbit); j++)                                    \
                jtable[j]=(void*)(prio[(int)jtable[j]]).routine;         \
    }

#define M8 0xff
#define M16 0xffff
#define M17 0x1ffff
#define M18 0x3ffff
#define M19 0x7ffff
#define M20 0xfffff
#define M21 0x1fffff



#define CMDNAMES char * cmdname[]={"SIO","TIO","TDV","HIO","RIO","POLP","AIO","POLR"};
#define SIO 0
#define TIO 1
#define TDV 2
#define HIO 3
#define AIO 6


void memory(word size);
byte iop(word cmd, word iopad);


long long mult(long a, long b);


#define TBS(buf,sz,tbl)  {int i;int _s=(sz);byte * _x=buf; \
                 for (i=0;i<_s;i++) do begin    \
                      *_x=tbl[(int)(*_x)];            \
                      _x++;                      \
                      endfor }

//clock_evt = 1 , kbd_evt = 2 is hardcoded in dosint.c , so don't change it here
enum task_evt  { //clock_evt=1, kbd_evt=2, com_evt=4, endwait_evt=8,
        pcp_evt=16, intr_evt=32,
        hipcp_evt=64, strobe_evt=128,
        iointr_evt=256};
enum pcp_flag { cntrl_c_flag=1, idle_flag=2, step_flag=4, addr_flag=8, rec_flag=16};
enum users { cpuuser=0, pcpuser=1, ttyuser=2 ,iouser=3, debuguser=4};

//#define ENABLE  /* asm volatile("sti  ;" ::: "cc" ); */ in idlop.h
//#define DISABLE /* asm volatile("cli  ;" ::: "cc" ); */
#define outp(p,v) ({int _p=(p); int _v=(v); \
    asm volatile("outb %%al,(%%dx);" :: "a" (_v), "d" (_p) : "cc" );})
#define inp(p) ({int _p=(p);unsigned char _i; asm volatile("inb (%%dx),%%al;" \
    :"=a" (_i):"d" (p):"cc");_i;})
    
//#define CLEAR(x,b) x &= not(b);
#define CLEAR(x,b) {int _b=~(b); asm("andl %1,%0; " : "=m" (x) : "ir" (_b) : "cc");}
//#define SET(x,b) x |= b;
#define SET(x,b) {int _b=(b); asm("orl %1,%0; " : "=m" (x) : "ir" (_b) : "cc");}
#define STS(x,b,m) {typeof (x) _b=(b); typeof(x) _m=(m);x &= not(_m) or _b ; x |= _b and _m;} // "or b" is for interruptability considerations
#define ANYALL(x) (-(x ne 0))
#define ACTIVECHG(x) {    \
      DISABLE              \
      x.active=x.triggered and not(x.armed);      \
      x.waiting=x.triggered and x.armed;          \
      x.ewaiting=x.waiting and x.enabled;         \
      STS(active.h.g,ANYALL(x.active), x.pbit)     \
      active.h.h0=HIBIT(active.h.g);               \
      active.h.h1=GROUPBITS(active.h.g,active);    \
      STS(ewaiting.h.g,ANYALL(x.ewaiting), x.pbit) \
      ewaiting.h.h0=HIBIT(ewaiting.h.g);            \
      ewaiting.h.h1=GROUPBITS(ewaiting.h.g, ewaiting); \
      if (ewaiting.w gt active.w) SET(event,intr_evt) \
      ENABLE}
#define GROUP(x) intr[16-BPRIO(x)].grp
#define HIBIT(x) (0x8000L>>(16-BPRIO(x)))
#define GROUPBITS(x,fld) (x ? intr[GROUP(x)].fld : 0)

// #define PRIORITY(x) (*priority+x)

#define TRIGGER(g,bit) {SET(intr[g].triggered,((bit) and intr[g].armed)) ACTIVECHG(intr[g])}

// note ACTIVECHG must be called seperately
#define ARM(g,bit) {SET(intr[g].armed,bit and intr[g].exists) STS(intr[g].triggered,intr[g].signal,bit and intr[g].exists)}

enum verbosities { V_DEVICE=1, V_DEVC=2, V_IOP=4, V_CPU=8, V_CONFIG=16, V_DEBUG=32};
#define V_CHK(x) {if (verbose and VERB_LEVEL) then begin  \
        /* showscreen(V_USER) */; x; endif}
#define V_USER pcpuser  //default if not specied in individual modules

#define SLOC(pe,x,item) (((item*)((pe)-sizeof(item)))[-(x)])
#define MEM(x,typ) SLOC(mem,x,typ)
#define MEMW(x) SLOC(mem,x,word)
#define MEMB(x) SLOC(mem,x,byte)
#define MEMD(x) SLOC(mem,x,dword)


#define REM(p,segsize,typ) ((((segsize-1) and (word) p)/sizeof(typ))+1)
#define RXMREMb(p) (REM(p,64,byte))
#define RXMREMw(p) (REM(p,64,word))
#define REMwRXM(p,sp) (REM(p, (( sp and 0x1fff0)? 2048:64) , word))

#define MIN(a,b) ((a gt b) ? b : a)
#define MIN3(a,b,c) (MIN(MIN(a,b),c))
#define MAX(a,b) ((a gt b) ? a : b)
#define MAX3(a,b,c) (MAX(MAX(a,b),c))
#define PCP_BREAK {SET(event,pcp_evt) SET(pcp_flag,cntrl_c_flag)}




#define SEMAPHORE_DATA(x)  int id_##x;  
#define SEMAPHORE_INIT(x)       \
{semun options;           \
key_t uniq_key=ftok(".",'s');   \
id_##x = semget(uniq_key,1,IPC_CREAT |IPC_EXCL |0666);   \
options.val = 1;          \
semctl(id_##x , 0, SETVAL, options);   \
assert(semctl(id_##x, 0, GETVAL, 0) ne 0);  \
}

#define SEMAPHORE_CLOSE(x)  {   \
semctl(id_##x, 0, IPC_RMID,0 ); \
}
#define S_LOCK(x) {  \
struct sembuf lock; \
lock.sem_num = 0;   \
lock.sem_op = -1;   \
lock.sem_flg = 0 ; \
semop(id_##x, &lock, 1);  \
}
#define S_UNLOCK(x) {   \
struct sembuf lock; \
lock.sem_num = 0;   \
lock.sem_op = 1;    \
lock.sem_flg =0;   \
semop(id_##x, &lock, 1);  \
}

#define ENABLE S_LOCK(DE)

#define DISABLE S_UNLOCK(DE)
