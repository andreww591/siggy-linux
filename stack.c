#define NSTKS 4
#include <stdlib.h>
#include "sigma.h"
#include <stdarg.h>
#include "vterm.h"
#undef V_USER
#define V_USER pcpuser
int cpu_stack[NSTKS]={[0 ... NSTKS-1]=0};
int bot_stack[NSTKS]={[0 ... NSTKS-1]=0};

extern SEMAPHORE_DATA(DE);

#if 0
int cur_task=0;
main()
{   int k=1;
    stack_init(0);
    k=task_fork(1);
    printf("hello task %d\n",cur_task);
    output(k);
//    if (stack_no-1 ge 0) k=set_stack(stack_no-1,0); else k=set_stack(0,2);
    task_kill(0);
    output(k);
    printt("done\n")
}
#else
extern cur_task;
#endif
int stack_init (int size)
{
    int i;
    if (size eq 0) size=32768;
    foreach(cpu_stack,i)
        bot_stack[i]= (int) malloc(size);
        bot_stack[i]=bot_stack[i]+size;
        cpu_stack[i]=0;       //fix me:  dont allocate stack 0
        endforeach
    return 0;
}
#define SAVE_REGS \
     asm volatile( "pushf; push %%eax; push %%ebx; push %%ecx; push %%edx; "   \
        "push %%ebp; push %%esi; push %%edi; push %%ds; push %%es; push %%fs;" \
        "push %%gs;"::);
#define SAVE_SP \
     asm volatile( "mov %%esp,%0;": "=a" (cpu_stack[cur_task]) :: "cc");
#define SET_SP \
     asm volatile( "mov %0,%%esp;" :: "a" (cpu_stack[ix]) : "cc");
#define SET_REGS \
    asm volatile( " pop %%gs; pop %%fs; pop %%es; pop %%ds; pop %%edi;"        \
            "pop %%esi; pop %%ebp; pop %%edx; pop %%ecx; pop %%ebx; pop %%eax;" \
            "popf;":: );

int task_switch(int ix)    //switch to task ix
{
    DISABLE
    if( ix lt 0 || ix ge NSTKS || ix eq cur_task)
        {ENABLE
         printt("invalid task switch request  ")
         outputnl(ix); output(cur_task); abort(); return -1;}
//  vterm_printf(2,"ts%d ",ix);
    SAVE_REGS SAVE_SP
    cur_task=ix;
    SET_SP  SET_REGS
    ENABLE
    return 0;   //returns to point of last usage of this stack
}
int task_kill(int ix)
{
    cpu_stack[cur_task]=bot_stack[cur_task];
    DISABLE
    if( ix lt 0 || ix ge NSTKS || ix eq cur_task)
        {ENABLE
         printt("invalid kill request  ")
         outputnl(ix); output(cur_task); return -1;}
//  vterm_printf(2,"k%d ",ix);
//  SAVE_REGS SAVE_SP
    cpu_stack[cur_task]=0;
    cur_task=ix;
    SET_SP  SET_REGS
    ENABLE
    return 0;   //returns to point of last usage of this stack
}

int task_fork(int ix)
{
    DISABLE
    if( ix lt 0 || ix ge NSTKS || ix eq cur_task)
        {ENABLE
         printt("invalid fork request  ")
         outputnl(ix); output(cur_task); return -1;}
//  vterm_printf(2,"f%d ",ix);
    asm ("mov $0,%%eax" ::);   //save old task with eax=0
    SAVE_REGS SAVE_SP
    cur_task=ix;
    cpu_stack[ix]=bot_stack[ix];
    SET_SP
    asm volatile(" mov -4(%%ebp),%%ebx\n push 4(%%ebp)\n"
         " mov (%%ebp),%%ebp\n  mov $1,%%eax\n  ret\n " ::);
//  ENABLE
//  return 1;   //returns to caller twice; first with new task and eax=1
                //then again with old task and eax=0
}
#if 0
int sw_task(int ix,int fork)
{   int stksz;
    DISABLE
//  output(ix); output(fork); output(stack_no);
    if( ix lt 0 || ix ge NSTKS || ix eq stack_no)
        {ENABLE
         printt("invalid stack request\n")
         output(ix); output(fork); output(stack_no); return -1;}
    if (fork eq 2) begin
       cpu_stack[stack_no]=bot_stack[stack_no];
       endif else begin
       asm volatile( "pushf; push %%eax; push %%ebx; push %%ecx; push %%edx; "
          "push %%ebp; push %%esi; push %%edi; push %%ds; push %%es; push %%fs;"
          "push %%gs;"::);
           asm volatile( "mov %%esp,%0;": "=a" (cpu_stack[stack_no]) :: "cc");
       endelse
    stack_no=ix;
    stksz=bot_stack[ix]-cpu_stack[ix];
    if ((stksz lt 0 || stksz gt 47) && (fork ne 1)) {
    asm volatile( "mov %0,%%esp;" :: "a" (cpu_stack[ix]) : "cc");
    asm volatile( " pop %%gs; pop %%fs; pop %%es; pop %%ds; pop %%edi;"
            "pop %%esi; pop %%ebp; pop %%edx; pop %%ecx; pop %%ebx; pop %%eax;"
            "popf;":: );
            } else {
            cpu_stack[ix]=bot_stack[ix];
            asm volatile( "mov %0,%%esp;" :: "a" (cpu_stack[ix]) : "cc");
            asm volatile(" mov -4(%%ebp),%%ebx\n push 4(%%ebp)\n"
            " mov (%%ebp),%%ebp\n  mov $1,%%eax\n  ret\n " ::);}
    ENABLE
    return stack_no;   //returns to point of last usage of this stack
          //if its a new stack, returns to caller
}
#endif
