#include <stdio.h>
#include "sigma.h"
#include "sigmaio.h"
#include "coc.h"
//#include <sys/farptr.h>
//#include <go32.h>
#define VERB_LEVEL V_DEVC
#include "vterm.h"
#include <stdlib.h>

#undef V_USER
#define V_USER iouser
#define ORD_WRITE 1
#define ORD_READ 2
#define ORD_SEEK 3
#define ORD_SENSE 4
#define ORD_CHKWRITE 5
#define SHOW DV_CHK({CMDNAMES printt("cmd ") printt(cmdname[cmd]) printt(" to device ") printt(dev->name) NEWLINE})
//#define SHOW
#define SHOW_ORD DV_CHK(({printt(dev->name) printt(" Order Out = ") printxv(ord) NEWLINE}))
devdata *thead=0;
devdata *ttail=0;
extern byte e2a[];
extern byte a2e[];
extern verbose;
extern event;
extern itic;
extern pcp_flag;
extern i_states intr[];
extern ibits active,waiting,ewaiting;
extern uword ic;
extern iopdata* Piop[8];
extern cocdata* cocs[16];
extern vidtty,ttylight;
extern user;
extern w_task;
extern SEMAPHORE_DATA(DE);
#define HIOCHK {if (dev->hio) return;}   //get out if hio was received
#define NOTOP dev->
#define BUSY dev->busy
#define MBS(s,d,c) {word i=(c);byte * src=(s); byte * dst=(d); for (;i;i--){*dst++=*src++;}}
#define MBSX(s,d,c) {word i=(c),j;byte *src=(s); byte * dst=(d); for (j=0;j<i;j++){dst[j eor 3]=src[j];}}
#define MBS0(d,c) {word i=(c);byte * dst=(d); for (;i;i--){*dst++=0;}}
#define MBS0X(d,c) {word i=(c),j;byte * dst=(d); for (j=0;j<i;j++){*dst[j eor 3]=0;}}
#define printdev(x) printf("%s %s ",dev->name,x);
#define UORD  printt("Unimplemented order to ") printt(dev->name) printt(" = ") printxv(ord) outputx(DC->command); //exit(1);
#define SETPHASE(n,pt) dev->phase=&&phase##n; dev->itime=itic+pt; DISABLE LINK(dev,t,) ENABLE
#define DEVICE(x) void x(devdata *dev,word cmd) {
#define SUBCONTROLLER                          \
    word ord,status,endserv,es ;           \
    DCdata * DC=dev->parent;                   \
    iopdata * iop=DC->parent;                  \
    if (cmd) goto *dev->phase;                 \
    SHOW                                       \
    dev->TSH_status=DC->TSH_status=TSH_AUTO;   \
    D_BUSY;                                \
    dev->busy&=7; DC->busy&=7;             \
get_cmd:                                   \
    dev->ars=dev->orp=0;                   \
    ord=DC->command=order_out(dev);        \
    SHOW_ORD                               \


#define CHAINR {dev->ars=dev->orp=0; ord=DC->command=order_out(dev); return;}
#define CHAIN  goto get_cmd;

#define T_ORDER(chainx)                        \
    {   word tord;                             \
        tord=t_order(dev,0);                   \
    HIOCHK                                     \
    DV_CHK(printt(" tord = ") printxv(tord) NEWLINE)   \
    if (tord and TO_INT) then D_INTERRUPT      \
    if (tord and TO_IOPH) then begin           \
         DC->TSH_status|=TSH_UE;               \
         order_in(dev,OI_UE | OI_CE);          \
         tord=t_order(dev,0);                  \
         endif else begin                      \
    if (tord and TO_INT) then D_INTERRUPT      \
/*  if (tord and TO_CD) then begin */          \
        es=order_in(dev,OI_CE);                \
        if (!es) then tord=t_order(dev,0);     \
        if (tord and TO_INT) then D_INTERRUPT  \
        if (tord and TO_CC) then chainx        \
/*      endif       */                         \
    endelse   \
    N_BUSY }
// report UEND
#define UEND {word es;DC->TSH_status|=TSH_UE;dev->TSH_status|=TSH_UE;es=order_in(dev,OI_UE | OI_CE); if (!es) T_ORDER(break;);}
#define UENDx uend=1;
// report CHEND
#define CHEND {word es=order_in(dev,OI_CE); if (!es) T_ORDER(CHAIN);}
#define CHEND_D {word es=order_in(dev,OI_CE); if (!es) T_ORDER(UEND);}
#define AUTO

#define CHENDD {SETPHASE(2,2)}  //DELAY 4 MS(2 TICS) BEFORE CHEND AT phase2

#define CURSEEKADDR (((hword *) (dev->sense))[1])
#define DATAIN {data_in(dev,1); CHEND}
#define HEADS dev->heads
#define NSPT dev->nspt
#define CHKSEEK {if (*(dev->sense+1) ge HEADS) {dev->TDV_status|=0x20; *(dev->sense+11)|=8;UEND}}
#define INCSEEK {if (++(*dev->sense) ge NSPT) {*dev->sense=0; (*(dev->sense+1))++;}}

#define INCSEEKR {if (((CURSEEKADDR++) and 0xf) ge NSPT) CURSEEKADDR+=16-NSPT;}
#define CHKSEEKR {if (CURSEEKADDR ge HEADS*16) {dev->TDV_status|=0x20; dev->AIOstatus|=0x20000000; UEND}}

#define AIO_EOF 0x10000000
#define TAPESTATUS   \
            {word uend=0; \
            DV_CHK(printt("Status = ") printd(status) NEWLINE)     \
            if (dev->HOST)    \
            {STS(dev->TDV_status,ANYALL((ftell((FILE*)dev->HOST) le 4)),4);}   \
            else if (status ge 0) STS(dev->TDV_status,status>>4,4);        \
            STS(dev->TDV_status,8,0xbb)   \
        switch (status) begin                                      \
            case 00:  break;  /* set automatic ?*/   \
            case 1: order_in(dev,OI_TE);UENDx;break; \
/*eot?*/    case -2:                                   /*set EOF  */   \
            case -16: UENDx                           /*TM on read (UEND) */  \
            case -32: STS(dev->TDV_status,0x10,0xb3);  /*TM on spacefile ops*/ \
                break;              \
            case -21: STS(dev->TDV_status,0x14,0xb3);  \
                UENDx break;        \
            default: order_in(dev,OI_TE);                 \
                printf(" %s: ",dev->name);   \
                printt(" ord ") printxv(ord) printt(" status = ") printd(status) \
                if(dev->HOST) {printt(" fpos = ") printxv(ftell((FILE *)dev->HOST)) NEWLINE}      \
                   UENDx /* uncorr read err for now*/ \
        endswitch            \
        STS(dev->AIOstatus,((word) dev->TDV_status)<<24,0xB8000000)   \
        if (uend) UEND  \
        if (dev->TSH_status and TSH_UE) break;}


#define BTSTATUS   \
            {word uend=0; \
            DV_CHK(printt("Status = ") printd(status) NEWLINE)              \
            if ((ord and 8) && (status and 0x40)) /* reverse op and EOM */  \
                STS(dev->TDV_status,status>>4,4) /*load point*/ ;           \
            STS(dev->TDV_status,8,0xbb)  /*Noncorrectable read error*/      \
            if (status and 0xC0) then begin   /*TM or EOM */                \
                SET(dev->TDV_status,0x10) /*set End of FIle*/               \
                if ((ord eq 2) || (ord eq 0xC)) UENDx;  /*on read or readreverse*/  \
                endif                                                       \
        switch (status and 15) begin                                         \
            case 0:  break;                                                 \
            case 1:  break;  /*recovered error would be nice to log*/       \
            case 2:  STS(dev->TSH_status,2,0x60) UENDx; /*not operational*/ \
            case 4: STS(dev->TSH_status,2,0x60) /*hard hardware error-set not op*/ \
            case 3: SET(dev->TDV_status,8) /*noncorrectable read err*/      \
                    order_in(dev,OI_TE); break;                             \
            case 5: UENDx; break;      /*illegal request for this device*/  \
            case 6: status=ASPImodesense(dev);           /*unit attention*/ \
                      if (status) {printt("ASPImodesense returned ")        \
                          printxv(status) NEWLINE}                          \
                    STS(dev->TDV_status,not(status>>1),0x40) /*write permitted*/ \
                    goto retry;  /* UENDx; break;    */                     \
            case 7: SET(dev->TDV_status,0x20)   /*write protect violation */\
            case 8:                             /*blank block (EOM) */      \
            case 9:                   /*vendor unique*/                     \
            case 0xA:                 /*copy aborted*/                      \
            case 0xB:                 /* aborted command*/                  \
            case 0xC:                 /*equal*/                             \
            case 0xD:                 /*volume overflow*/                   \
            case 0xE:                 /*miscompare*/                        \
            case 0xF:                 /*reserved*/                          \
                    UENDx; break;                                           \
        endswitch                                                           \
        STS(dev->AIOstatus,((word) dev->TDV_status)<<24,0xB0000000)         \
        if (uend) UEND                                                      \
        if (dev->TSH_status and TSH_UE) break;}
#ifdef _aspi
DEVICE(dev_BT)
    SUBCONTROLLER
    CLEAR(dev->TDV_status,4);   //clear load point bit
retry:
    dev->TSH_status=0x10;
    switch (ord) begin
            case ORD_WRITE:
                    endserv=data_out(dev,dev->size);
                    status=ASPIdevwrite(dev);
                    BTSTATUS
                    CHEND break;
            case ORD_READ:
//                   printt("dev->buffer = ") printxv(dev->buffer) NEWLINE
//                   printt(" dev = ") printxv(dev) NEWLINE
                     status=ASPIdevread(dev);
            //         TAPESTATUS
                     endserv=data_in(dev,1);
                     if (dev->ars ne dev->orp) order_in(dev,OI_IL);
                     BTSTATUS
                     CHEND break;
            case 0xC:           // read reverse
                    status=ASPIdevreadrev(dev);
            //        TAPESTATUS
                    dev->orp = dev->ars -1;
                    endserv=data_in(dev,1);
                    if (dev->orp ne -1) order_in(dev,OI_IL);
                    BTSTATUS
                    CHEND break;
            case 0x43:          // space record forward
                    status=ASPIdevread(dev);
                    BTSTATUS CHEND break;
            case 0x4B:          // space record backward
                    status=ASPIdevsrb(dev);
//                  printdev(" space backwards status = ") printd(status) NEWLINE
                    BTSTATUS CHEND break;
            case 0x13:          // rewind and interrupt
                    DEV_INTERRUPT
                    dev->AIOstatus|=0x40000000;
            case 0x23:          // rewind offline
            case 0x33:          // rewind online
                    status=ASPIdevrewind(dev);
                    if (status eq 0) SET(dev->TDV_status,4)  //Load point
                    BTSTATUS CHEND break;
            case 0x53:          // space file forward
                    status=ASPIdevspacefile(dev);
                    if (status eq 0) status=0x80;
                    BTSTATUS CHEND break;
            case 0x5B:          // space file backward
                    status=ASPIdevspacefileb(dev);
                    if (status eq 0) status=0x80;
                    BTSTATUS CHEND break;
            case 0x63:          // set erase
                    status=0;
                    BTSTATUS CHEND break;
            case 0x73:          // write tape mark
                    status=ASPIdevtapemark(dev);
                    if (status eq 0) status=0x80;
                    BTSTATUS CHEND break;
            default:  UORD  UEND break;
            endswitch    

}
#endif
DEVICE(dev_MT)
    SUBCONTROLLER
//fix: include AUTO vs MANUAL test somewhere
    dev->TSH_status=0x10;
    dev->ars=0;
        switch (ord) begin
            case ORD_WRITE:
                    endserv=data_out(dev,dev->size);
                    status=HOSTdevwrite(dev);
                    TAPESTATUS
                    CHEND break;
            case ORD_READ:
                     status=HOSTdevread(dev);
              //       TAPESTATUS
                     endserv=data_in(dev,1);
 //                    if(dev->ars ne dev->orp) order_in(dev,OI_IL);
                     TAPESTATUS
                     CHEND break;
            case 0xC:           // read reverse
                    status=HOSTdevreadrev(dev);
              //      TAPESTATUS
                    dev->orp = dev->ars-1;
                    endserv=data_in(dev,1);
                    DV_CHK(printt(" read reverse dev->orp after data_in = ") printxv(dev->orp) NEWLINE)
 //                   if (dev->orp ne -1) order_in(dev,OI_IL);
                    TAPESTATUS
                    CHEND break;
            case 0x43:          // space record forward
                    status=HOSTdevread(dev);
                    TAPESTATUS CHEND break;
            case 0x4B:          // space record backward
                    status=HOSTdevsrb(dev);
//                  printdev(" space backwards status = ") printd(status) NEWLINE
                    TAPESTATUS CHEND break;
            case 0x13:          // rewind and interrupt
                    dev->AIOstatus|=0x40000000;
                    status=HOSTdevrewind(dev);
                    DEV_INTERRUPT
                    TAPESTATUS CHEND break;
            case 0x23:          // rewind offline
            case 0x33:          // rewind online
                    status=HOSTdevrewind(dev);
                    TAPESTATUS CHEND break;
            case 0x53:          // space file forward
                    status=HOSTdevspacefile(dev);
                    TAPESTATUS CHEND break;
            case 0x5B:          // space file backward
                    status=HOSTdevspacefileb(dev);
                    TAPESTATUS CHEND break;
            case 0x63:          // set erase
                    status=0;
                    TAPESTATUS CHEND break;
            case 0x73:          // write tape mark
                    status=HOSTdevtapemark(dev);
                    TAPESTATUS CHEND break;
            default:  UORD  UEND break;
            endswitch
}

DEVICE(dev_CR)
    SUBCONTROLLER
//fix: include AUTO vs MANUAL test somewhere
// MANUAL or NOT READY if at eof?
        switch (ord) begin
            case ORD_READ:
                     V_CHK(printt("CR read\n");)
                     HOSTdevread(dev);
                     data_in(dev,1);
                     CHEND break;
            case 0x43: UEND break;  //implemented because mtlboot tries this to see if tape
            default:
                    UORD N_BUSY; break;
                    exit(1);
            endswitch
}

//#define TTYLIGHT _farpokew(_dos_ds,vidtty,ttylight);
#define TTYLIGHT   set_led(ttylight);

void console_writeln( devdata * dev, const char *s,int len)
{   int c,i;  const unsigned char *ss = (const unsigned char *) s;
    for (i=len;i;i--) begin
        c = *ss++;
        console_write(dev,c);
        if (c eq '\n') console_write(dev,'\r');
        endfor
}

void dev_TY_hio(devdata *dev)
{
    if ((dev->busy and 1) eq 0) {ttylight=0;
        TTYLIGHT}
}

char endk[256]= { [0x05]=1,  [0x08]=1, [0x15]=1 };
dev_TY_charin(devdata * dev,word k)
{
    word es,ord,ek,tord;
    DCdata * DC=dev->parent;
    iopdata * iop=DC->parent;
    if (BUSY) then begin
        if (((ord=DC->command) and 1) eq 0) then begin    //if ord=read
            if (k lt 0x100) then begin
                if (k ge 0x61 && k le 0x7a) then k&=0xdf;    //force alpha upper case
//              printc(k); if (k eq 0x0d) then printc(0x0a);
//              fflush(stdout);
//                vterm_write(user,k); if (k eq 0x0d) then vterm_write(user,0x0a);
                console_write(dev,k); 
                if (k eq 0x0d) then console_write(dev,0x0a);
                ek=(dev->buffer)[dev->ars++]=a2e[k];
                es=data_in(dev,0);
                if(((ord and 4) && (ek eq 8)) ||
                    ((ord eq 0x86) && endk[ek])) then es=order_in(dev,OI_CE);
                if (!es) T_ORDER(CHAINR)
                if ((dev->busy and 1) eq 0) {ttylight=0;
                    TTYLIGHT}
              endif
            endif
        endif
}

DEVICE(dev_TY)
    SUBCONTROLLER
        switch (ord) begin
            case 6:                    //read keyboard to eom or count done
            case 0x0E:
            case 0x86:
            case ORD_READ:          //read from console paper tape
                showscreen(ttyuser);
                ttylight=1;  //0x0600+'T';
                TTYLIGHT
                break;
            case ORD_WRITE:         //write to TY
            case 5:
                endserv=data_out(dev,dev->size);
                TBS(dev->buffer,dev->ars,e2a); *(dev->buffer+dev->ars)=0;
      //        showscreen(ttyuser);
      //        printt(dev->buffer)  fflush(stdout);
//                vterm_outl(ttyuser,dev->buffer,dev->ars);
                console_writeln(dev,dev->buffer,dev->ars);
     //!        if (strlen(dev->buffer) ne dev->ars) printt("Truncated write ")
//              if(!endserv) then T_ORDER(CHAIN)
                CHEND break;
            default:                //other orders?
                UORD
            endswitch
}


DEVICE(dev_LP)
    SUBCONTROLLER
    switch (ord) begin
        case 1:
        case 0x41:
        case 3:
        case 0x43:
        case 5:
        case 0x45:
        case 7:                             //?
                es=data_out(dev,dev->size);
                TBS(dev->buffer,dev->ars,e2a);
                HOSTdevwrite(dev);
                SETPHASE(2,25)    //wait 25 tics and reenter at phase2
                                  //25 tics = 50ms/line = 1200 line/minute
                break;
        default: UORD
        if (!(ord and 1)) UEND
        break;
        endswitch
    return;
phase2:
    DISABLE
    UNLINK(dev,t,)
    ENABLE
    CHEND
}

#define printsect  printx(*((word*) dev->sense))
DEVICE(dev_DP)
    SUBCONTROLLER
        switch (ord) begin
            case ORD_WRITE:
//              printdev(" write order ")
                es=1;
                while (es) begin
                    CHKSEEK
                    dev->orp=dev->ars=0;
                    es=data_out(dev,1024);
                    DV_CHK(printt(" write sector = ") printsect  NEWLINE)
                    if (dev->ars lt 1024) begin
                        int i;
                        DV_CHK(printt ("short write = ") printd(dev->ars) NEWLINE)
                        for (i=dev->ars;i<1024;i++) dev->buffer[i]=0;
                        dev->ars=1024;
                        endif
                    HOSTdevwrite(dev);
                    HIOCHK
                    INCSEEK
                    endwhile
//              PCP_BREAK
                CHENDD  break;
            case ORD_CHKWRITE:
//              printdev(" checkwrite order ")
                es=1;
                while (es) begin
                    word st;
                    CHKSEEK
                    dev->orp=dev->ars=0;
                    es=data_out(dev,1024);
                    DV_CHK(printt(" chkwrite sector = ") printsect NEWLINE)
                    st=HOSTdevread1(dev);   //no compare here yet
                    INCSEEK
                    endwhile
                CHENDD  break;
            case ORD_READ:
   //           printdev(" read order")
                es=1;
                while (es) begin
                    word st;
                    dev->orp=0;
                    CHKSEEK
                    DV_CHK(printt(" read sector = ") printsect printt(" es = ") printd(es) NEWLINE)
                    st=HOSTdevread1(dev);
                    dev->ars=1024;
                    INCSEEK
                    HIOCHK
                    es=data_in(dev,1);
                    endwhile
//                  PCP_BREAK
                CHENDD  break;
            case 0x83: //DEV_INTERRUPT        //fall through to seek
                dev->AIOstatus |= 0x08000000;
                DV_CHK(printdev (" Seek 83 from ") printsect NEWLINE)
                es=data_out(dev,4);
                MBSX(dev->buffer,dev->sense,4);
                MBS0(dev->sense+4,12);
                DV_CHK(printdev(" seek to ") printsect NEWLINE)
//              if (*((word *) dev->sense) eq 0x8) PCP_BREAK
                CHEND_D         //uend if command chain??
                HOSTdevseek(dev);
                HIOCHK
                DEV_INTERRUPT
                break;
            case ORD_SEEK:
                es=data_out(dev,4);
                MBSX(dev->buffer,dev->sense,4);
                MBS0(dev->sense+4,12);
                DV_CHK(printdev(" seek to ") printsect NEWLINE)
//              if (*((word *) dev->sense) eq 0x8) PCP_BREAK
                HOSTdevseek(dev);
                HIOCHK
                CHENDD break;
            case ORD_SENSE:
                DV_CHK(printdev(" sense order "))
                *(dev->sense+7)=itic mod NSPT;  //make it spin!
                MBSX(dev->sense,dev->buffer,dev->ssize);
                dev->ars=dev->ssize;
                data_in(dev,1);
                CHENDD break;
            case 0xA:
                DV_CHK(printdev(" Read headers "))
                MBS0(dev->buffer,8);
                es=1;
                while (es) begin
                    dev->orp=0; dev->ars=8;
                    MBSX(dev->sense,dev->buffer+1,4);
                    INCSEEK
                    es=data_in(dev,1);
                    HIOCHK
                    endwhile
                CHENDD break;
            case 0x17:                //release
            case 0x07:                //reserve
                    es=data_out(dev,1024);
                    CHEND break;
            default:   PCP_BREAK
                UORD UEND break;
            endswitch
    return;
phase2:
    DISABLE
    UNLINK(dev,t,)
    ENABLE
    CHEND
}
dev_ME_charin(devdata * dev,word k,word L)
{
    word es,ord,ek,tord;
    DCdata * DC=dev->parent;
    iopdata * iop=DC->parent;
    if (BUSY) then begin
        if (k le 0x100) then begin
            dev->ars=2; dev->orp=0;
            (dev->buffer)[0]=k; (dev->buffer)[1]=(L & 63) or ((k>>1) and 0x80);
            es=data_in(dev,0);
//            TRIGGER(2,0x8000);       //hardcode interrupt loc 0x60 for now
            TRIGGER(dev->ig0,dev->ib0);
            if (!es) T_ORDER(CHAINR)
            endif
        endif
}

DEVICE(dev_ME)
    SUBCONTROLLER
    DV_CHK(printdev("dev_ME sio ord = ") printxv(ord) NEWLINE)
}



DEVICE(dev_CP)
}
DEVICE(dev_DC)
    SUBCONTROLLER
        switch (ord) begin
            case ORD_WRITE:
//              printdev(" write order ")
                es=1;
                while (es) begin
                    CHKSEEKR
                    dev->orp=dev->ars=0;
                    es=data_out(dev,1024);
                    DV_CHK(printt(" write sector = ") printsect  NEWLINE)
                    if (dev->ars lt 1024) begin
                        int i;
                        DV_CHK(printt ("short write = ") printd(dev->ars) NEWLINE)
                        for (i=dev->ars;i<1024;i++) dev->buffer[i]=0;
                        dev->ars=1024;
                        endif
                    HOSTdevwrite(dev);
                    HIOCHK
                    INCSEEKR
                    endwhile
//              PCP_BREAK
                if (w_task ge 0) {CHEND} else
                    {SETPHASE(2,3)}   //interrupt 6ms clock tic
                break;
            case ORD_CHKWRITE:
//              printdev(" checkwrite order ")
                es=1;
                while (es) begin
                    word st;
                    CHKSEEKR
                    dev->orp=dev->ars=0;
                    es=data_out(dev,1024);
                    DV_CHK(printt(" chkwrite sector = ") printsect NEWLINE)
                    st=HOSTdevread1(dev);   //no compare here yet
                    INCSEEKR
                    endwhile
                if (w_task ge 0) {CHEND} else
                    {SETPHASE(2,3)}   // 6ms
                break;
            case 0x12:
            case ORD_READ:
   //           printdev(" read order")
                es=1;
                while (es) begin
                    word st;
                    dev->orp=0;
                    CHKSEEKR
                    DV_CHK(printt(" read sector = ") printsect printt(" es = ") printd(es) NEWLINE)
                    st=HOSTdevread1(dev);
                    dev->ars=1024;
                    INCSEEKR
                    HIOCHK
                    es=data_in(dev,1);
                    endwhile
                if (w_task ge 0) {CHEND} else
                    {SETPHASE(2,3)}   //interrupt 6ms
                break;
            case ORD_SEEK:
                CLEAR(dev->AIOstatus,0x20000000);
                CLEAR(dev->TDV_status,0x20);
                es=data_out(dev,2);
                if (dev->ars lt 2) {order_in(dev,OI_IL);}
                if ((((*(dev->buffer+1)) and 0xf) ge NSPT) ||
                      (((*(dev->buffer+1))>>4) + (16L * (*dev->buffer))) ge HEADS) begin
                    DV_CHK(printt("seek uend data - ") printxv(*dev->buffer) printt(" ") printxv(*(dev->buffer+1)) NEWLINE)
//                    dev->TDV_status |=0x20;
//                    dev->AIOstatus |=0x20000000;
//                    UEND break;
                    endif
                MBSX(dev->buffer,dev->sense,4);
                MBS0(dev->sense,2);
                DV_CHK(printdev(" seek to ") printsect NEWLINE)
//                if (((*(dev->sense+3) and 0xf) gt NSPT)|| CURSEEKADDR ge ((HEADS) << 4)) {UEND break;}
                CHKSEEKR
                HOSTdevseek(dev);
                if (w_task ge 0) {CHEND} else
                    {SETPHASE(2,3)}
                break;
            case ORD_SENSE:
                DV_CHK(printdev(" sense order "))
                *(dev->sense+1)=itic mod NSPT;  //make it spin!
                CHKSEEKR
                MBSX(dev->sense,dev->buffer,(dev->ssize+3)and 0xfc );
                dev->ars=dev->ssize;
                data_in(dev,1);
                CHKSEEKR
                CHEND break;
            case 0x80:
                CHEND break;
            default:
                UORD UEND break;
            endswitch
    return;
phase2:
    DISABLE UNLINK(dev,t,) ENABLE
    CHEND
}
//     moved this to device.c from iop.c

byte reset_dev(devdata *dev)
{
    DCdata * DC=dev->parent;
    iopdata *iop=DC->parent;
    int ret=dev->TDV_status;
    V_CHK(printt("Reset device ") printt(dev->name) NEWLINE)
    dev->busy=0;
    N_BUSY NDEV_BUSY N_INTERRUPT NDEV_INTERRUPT
    dev->TSH_status=0x10;
    if (dev->ssize gt 0) begin
        MBS0(dev->sense,4);  //zero seek address
        HOSTdevseek(dev);
        endif
    if (dev->hio_handler) (*dev->hio_handler)(dev);
    return ret;
}

