#include <stdlib.h>
#include "sigma.h"
#include "sigmaio.h"
#include "lights.h"
#define VERB_LEVEL V_IOP
#undef V_USER
#define V_USER pcpuser //iouser
#define SHOWCMD  DV_CHK({CMDNAMES printt(cmdname[cmd]) printt(" to IOP number ") printxv(iopad) NEWLINE})
#define SHOWDEV  DV_CHK(printt(" Device Address= ") printxv(devadd) NEWLINE)
#define SHOWCHAN DV_CHK(printt(" Channel =") printxv(chan) printt(" Device=") printxv(deva) NEWLINE)
#define SHOWCDW  DV_CHK(printt("IO Command Doubleword Address= ") printx(tmp and M21) \
        NEWLINE  printt("Command Doubleword=") printx(MEMW(tmp*2)) printt(" ") \
        printx(MEMW(tmp*2+1)) NEWLINE)
#define COUNT (iop->count[chan] and M16)       //was M16 BIG
#include "vterm.h"
#include "rvolatiles.h"

iopdata *iophead, Piop[8]={0,0,0,0,0,0,0,0};
uword ic=0;          //master iop ic pbits
devdata *chead[8]={0,0,0,0,0,0,0,0};
devdata *ctail[8]={0,0,0,0,0,0,0,0};
int io_task_busy=0;
extern cur_task,task_ready;
extern cpu_stack[],bot_stack[];
extern verbose;
extern viddos,light_on,light_off;
extern i_states intr[];
extern ibits active,ewaiting;
extern event,itic;
extern SEMAPHORE_DATA(DE);
word iop_int()
{
    iopdata * p=iophead;
    while (p) do begin
        if (p->ic) {TRIGGER(0,0x20) break;}
        p=p->flink;
        endwhile
//  if (intr[0].signal and 0x20) {TRIGGER(0,0x20)}
//  ACTIVECHG(intr[0])
    CLEAR(event,iointr_evt)
}

byte reset_DC(DCdata *DC)       //reset a given device controller
{
    devdata *p=DC->child;
    int ret=0;
    DC->TSH_status=0x10;
    while (p) do begin
        ret|=reset_dev(p);
        p=p->flink;
        endwhile
    return ret;
}

void reset_iop(iopdata *iop)   //reset each controller on this iop
{
    DCdata *p=iop->child;
    while (p) do begin
        reset_DC(p);
        p=p->flink;
        endwhile
}

void io_reset()             //reset each iop on the system
{
    iopdata *p=iophead;
    while (p) do begin
    reset_iop(p);
    p=p->flink;
    endwhile
}

//devdata *dev,*ohead;  // must be global -
byte iop(word cmd, word iopad)
{
    ACCESSES_MEMORY
    byte cc;
    word tmp;
    word devadd, chan, deva;
    DCdata *DC;
    iopdata *iop;
    devdata *dev;     //why does this need to be global?
    if (cmd eq AIO) then begin   //AIO
//      V_CHK(printt(" AIO ") NEWLINE)
        cc=0xc;     //cc for no interrupt recognition
        iop=iophead;
        while (iop) begin
            if (iop->ic) then begin
                DC=iop->PDC[BPRIO(iop->ic)-1];
                if (DC->multi) then begin
                    dev=DC->Pdev[BPRIO(DC->ic)-1];
                    if (DC->ic eq 0) CLEAR(iop->ic,DC->pbit)
                    endif else begin
                    dev=DC->child;
                    endelse
                    dev->busy &= 1;
                MEMW(0x20)=dev->AIOstatus;
                DV_CHK(printt(" AIO device = ") printxv(dev->AIOstatus) NEWLINE)
                cc=dev->AIOcc and 0x4;
                DV_CHK(printt(" AIO cc = ") printxv(cc) NEWLINE)
                if ((DC->TSH_status and TSH_IP) && (dev->AIOstatus and 0xf) eq (iop->mdeva[DC->address])) begin
                    N_INTERRUPT
                    if (!dev->busy) N_BUSY  //make controller and device ready
                    endif else begin
                    NDEV_INTERRUPT
                    if (!dev->busy) NDEV_BUSY  //make device ready
                    endelse
                DV_CHK(printt(" DC TSH after N_INTERRUPT = ") printxv(DC->TSH_status) NEWLINE)
                break;
                endif
            iop=iop->flink;
            endwhile
        return cc;
        endif
    if ((iop=find_iop(iopad)) eq NULL) then return 0xc;   // Addressed iop doesn't exist
    tmp=MEMW(0x20); devadd=tmp>>24 and M8; tmp &=M21;
    if (devadd and 0x80) then {chan=(devadd>>4 and 7);deva=devadd and 0xf;}
         else chan=deva=devadd and 0x7f;
    if (chan ge iop->channels) then return 0xc;      //IOP needs more fam
    if ((DC=find_DC(iop, chan)) eq NULL) then return 0xc;  //No device address recognition
    if ((dev=find_dev(DC,deva)) eq NULL) then return 4;  // Is this right?
    switch (cmd) {
        case SIO:                
            iop->cda[chan]=tmp and M17;           //was M16
            SHOWCMD  SHOWDEV  SHOWCHAN  SHOWCDW
            if (dev->busy or DC->busy) cc=4; else begin
                MEMW(0x21)=(TSH<<24)|(iop->IOPstatus[chan]<<16) | COUNT;
                MEMW(0x20)=iop->cda[chan]-1;
                dev->AIOstatus&=0xffff;
                dev->hio=0;
//                (*dev->handler)(dev,cmd);
                iop->IOPstatus[chan] = 0;  //clear bits 8-14
                iop->oi_status[chan] = 0;
                iop->mdeva[chan] = deva and 0xf;  //save 4 lsb of dev address
                if (dev->pcchan) begin
                    DISABLE; LINK(dev,c,[dev->pcchan])
                    dev->busy |=8; DC->busy |=8;
                    V_CHK(printf("io_task check here\n");)
                    if ((io_task_busy and (1<< ((dev->pcchan)-1))) eq 0) {task_switch(dev->pcchan);ENABLE;}
                        else {ENABLE; vterm_printf(ttyuser,"Defered i/o on %s\n",dev->name);}
                    endif else (*dev->handler) (dev,cmd);  //chan 0 for non dos using io
                cc=0; //sio should return status, but io is already complete here!
                endif
            return cc;
            break;
        case TIO:  //SHOWCMD here cripples mtl tty input
            MEMW(0x21)=(TSH<<24) | (iop->IOPstatus[chan]<<16) | COUNT;
            MEMW(0x20)=iop->cda[chan]-1;       //if s9 iop, subchannel status reported in high byte TTSH 3.3.1.1
            if (dev->busy) then cc=4; else cc=0;
            return cc;
        case TDV:
            SHOWCMD SHOWDEV
            MEMW(0x21)=(dev->TDV_status<<24) | (iop->IOPstatus[chan]<<16) | COUNT;
            MEMW(0x20)=iop->cda[chan]-1;
            if (dev->busy) then cc=4; else cc=0;
            return cc;
        case HIO:
            dev->hio=1;
            SHOWCMD SHOWDEV
            MEMW(0x21)=(TSH<<24) | (iop->IOPstatus[chan]<<16) | COUNT;
            MEMW(0x20)=iop->cda[chan]-1;
            DISABLE
            if (dev->busy and 9) then cc=4; else cc=0;
            if (dev->busy and 8) then begin
                UNLINK(dev,c,[dev->pcchan])
                endif
            ENABLE
            NDEV_BUSY
            NDEV_INTERRUPT
            N_BUSY
            N_INTERRUPT
            if (dev->hio_handler) (*dev->hio_handler)(dev);
            //need to reset UE - at least mt9 wants it
            STS(dev->TSH_status,0,8)
            STS(DC->TSH_status,0,8)
            return cc;
        default:
            SHOWCMD
            exit(0);
            return 0;
        endswitch
}

#define ORDER_IO               \
    ACCESSES_MEMORY            \
    DCdata *DC=dev->parent;    \
    iopdata *iop=DC->parent;   \
    int chan=DC->address;

#define FAM(x) ((iop->x)[chan])
#define SHOW_CHAN DV_CHK(printt("IOP ") printxv(iop->address) \
     printt(" fam, chan ") printd(chan)                      \
     printt(": flags=") printxv(FAM(flags))                  \
     printt(" count=") printxv(FAM(count))                   \
     printt(" byteaddr=") printxv(FAM(byteaddr)) NEWLINE)

byte order_out(devdata *dev)
{   udw cdw;
    word ticc=1,cd0,cd1;
    ORDER_IO
nextord:
    if (iop->cda[chan] gt szdword) {iop->IOPstatus[chan]|=0x12; return 255;}
    cdw.dw=MEMD((iop->cda)[chan]);
    (iop->flags)[chan]=cdw.w.w1>>24;
    (iop->count)[chan]=cdw.w.w1 and M16;       //was M20 ???
    if (iop->count[chan] eq 0) iop->count[chan]=0x10000;
    iop->IOPstatus[chan]&=0x7f;  //clear incorrect length (recover writes il on seek)
    (iop->byteaddr)[chan]=cdw.w.w0 and M19B;   //was M19 - BIG M20
    SHOW_CHAN
    iop->cda[chan]++;
    if ((cdw.w.w0 and 0x0f000000) eq 0x08000000) then begin  //Transfer in Channel
        iop->cda[chan]=cdw.w.w0 and M16B;      //was M16 BIG
        if (ticc--) goto nextord;
        iop->IOPstatus[chan]|= 6;  //IOP control error and iop halt
        endif
    return cdw.w.w0>>24;
}
byte order_in(devdata *dev,byte oi)    //order in byte is in DC->oi_status
{
    ORDER_IO
    DV_CHK(printt("Order In for device ") printt(dev->name) printt(" oi = ") printxv(oi) NEWLINE)
//                               IL                      TE                  CE UE
//  iop->IOPstatus[chan]|= ((oi<<1) and 0x80) or ((oi>>1) and 0x40);
    if (oi and OI_TE) SET(iop->IOPstatus[chan],0x40)
    if (oi and OI_IL) SET(iop->IOPstatus[chan],0x80)
    iop->oi_status[chan] |= oi;
    if (oi and OI_CM) then iop->cda[chan]++;    //chaining modifier
    if ((oi and OI_CE) && (iop->count[chan] ne 0)) SET(iop->IOPstatus[chan],0x80)
    if ((iop->IOPstatus[chan] and 0x80) && !(iop->flags[chan] and 2) &&
        (iop->flags[chan] and 0x08))
        SET(iop->IOPstatus[chan],0x2);   //iop halt if IL and not SIL and HTE
//    if (iop->IOPstatus[chan] and 0x40) {printt(dev->name) printt(" tde detected at oi \n")}
    return 0;        //xfer status to iop
}

byte t_order(devdata *dev)
{
    ORDER_IO
    byte aflags=0;
    byte flags=iop->flags[chan];
    byte dchain=(flags and 0x80)>>7 ;
    word count=iop->count[chan];
    word iopstat=iop->IOPstatus[chan];
    word oistat=iop->oi_status[chan];
    byte uend=(oistat>>3) and 1;
//    byte chend=(dev->orp ge dev->ars) or ((count eq 0) and not(dchain)) or uend
//        or ((iopstat and 0x10)>>4);      //   or (dev->ars gt dev->mxrl);
    byte chend=(oistat>>4) and 1;
    byte hte=((flags and 8)>>3);
    byte cchain=chend and ((flags and 0x20)>>5)  and
        not(dchain) and not(uend and hte);
    byte izc=(count eq 0 ) and ((flags and 0x40)>>6);
    byte ice=chend and ((flags and 0x10)>>4);
    byte iue=uend and ((flags and 4)>>2);
    byte iop_halt=((iopstat and 2)>>1);  //or   //IL and no SIL
     //   (((iopstat and 0x80)>>7) and ((not(flags) and 0x02)>>1));
    byte t_ord=((izc or ice or iue)<<7) or ((count eq 0)<<6) or (cchain<<5) or
        (iop_halt<<4);
//  printt(" iop halt = ") printxv(iop_halt) printt(" iopstat = ") printxv(iopstat) printt(" flags = ") printxv(flags) NEWLINE
 	
    aflags= (iopstat and 0xC0) or (izc<<5) or (ice<<4) or (iue<<3);
    STS(dev->AIOstatus, aflags<<16, 0x00ff0000);
    DV_CHK(printt("Terminal Order device ") printt(dev->name) printt("  ") printxv(t_ord) NEWLINE)
    return t_ord;
}

#define DATA_IO                  \
    ACCESSES_MEMORY              \
    int xsize,i,rem;             \
    byte *mema,*bufa;            \
    DCdata *DC=dev->parent;      \
    iopdata *iop=DC->parent;     \
    int chan=DC->address;        \
    int ba=iop->byteaddr[chan];  \
    int count=iop->count[chan];  \
    int remm=szbyte-ba;

#define SHOW_XFER(x) DV_CHK(printt(" Data xfer ") printt(x) printt(" ba = ") printxv(ba) \
    printt(" bc = ") printxv(count)        \
    printt(" ars = ") printxv(dev->ars))
    
#define SHOW_MEMA  DV_CHK(printt(" MEMB = ") printxv(mema) NEWLINE)
#define XFER_ADDR mema=&MEMB(ba);
#define NOSKIP !(iop->flags[chan] and 0x01)
byte data_in(devdata *dev,word enddata)
{   word direction=1;
    DATA_IO
    SHOW_XFER("in")
    if (remm le 0) {iop->IOPstatus[chan]|=0x12; return 0;}
    if (DC->command eq 0xC) direction=-1;
nextin:
    XFER_ADDR
    bufa=dev->buffer+dev->orp;
    if (direction gt 0) then begin
        rem=dev->ars - dev->orp;
        xsize = MIN(rem,count);
        if(remm lt xsize) {xsize=remm; iop->IOPstatus[chan]|=0x12;}
        SHOW_MEMA
        if (NOSKIP)   //check skip flag
            {for (i=0;i<xsize;i++) do begin
                *mema--=*bufa++;
                endfor}
        dev->orp+=xsize;
        endif else begin          //=============
        rem=dev->orp+1;
        xsize= MIN(rem,count);
        if (xsize gt ba+1) {xsize=ba+1; iop->IOPstatus[chan]|=0x12;}
        SHOW_MEMA
        if (NOSKIP)
            {for (i=0;i<xsize;i++) do begin
                *mema++=*bufa--;
                endfor}
        dev->orp-=xsize;
        endelse
    iop->count[chan]-=xsize;
    iop->byteaddr[chan]+=xsize*direction;
    if ((iop->count[chan] eq 0) && (iop->flags[chan] and 0x80)) then begin  //data chain logic
        order_out(dev);
        ba=iop->byteaddr[chan];
        count=iop->count[chan];
        goto nextin;
        endif
    if (iop->IOPstatus[chan] and 2) return 0;
    return (iop->count[chan] ne 0);  // return endserv: if count ne 0 then ok to endservice
    }
byte data_out(devdata *dev,word endat)
{
    DATA_IO
//    CHKHOSTBUF(dev->ars+count)
    SHOW_XFER("out")
nextout:
    rem= endat - dev->ars;
    xsize=MIN(rem,count);
    CHKHOSTBUF(dev->ars+xsize)
    XFER_ADDR
    bufa=dev->buffer+dev->ars;
    SHOW_MEMA
    {int i;
        if (NOSKIP) for (i=xsize;i;i--)  *bufa++=*mema--;
        else for (i=xsize;i;i--) *bufa++=0; // send zeroes out if skip
    }
    dev->ars+=xsize;
    iop->count[chan]-=xsize;
    iop->byteaddr[chan]+=xsize;   //should check direction here
    if (iop->count[chan] eq 0 && iop->flags[chan] and 0x80) then begin
        order_out(dev);
        ba=iop->byteaddr[chan];
        count=iop->count[chan];
        goto nextout;
        endif
        return (iop->count[chan] || (iop->flags[chan] and 0x80));  // return endserv: if count ne 0 then ok to endservice
//        return 0;         //endservice = 0 => DC must request a TO
}
extern devdata *thead;
io_task()
{
    devdata * ohead,*dev;
    int t=cur_task,nxt_task;
    while (1) begin
        dev=thead;
        SET_IO_ON
        while (dev) begin
            devdata * nxt=dev->tflink;             //in case handler unlinks us
            if (dev->pcchan eq t)
               if (itic ge dev->itime) (*dev->handler)(dev,1);  //resume parked handler (it might be command chained)
            dev=nxt;
            endwhile
        DISABLE
        while (chead[t]) begin
            ohead=chead[t];
            UNLINK(ohead,c,[t])
            ENABLE
            (*ohead->handler)(ohead,0);
            DISABLE
            endwhile
        SET_IO_OFF
     // io_task_busy=0;
        BITCLEAR(cur_task-1,io_task_busy)
        nxt_task=BPRIO(task_ready);
        if (nxt_task) BITCLEAR(nxt_task-1,task_ready)
        V_CHK(printf("switch to task"); printd(nxt_task); NEWLINE)
        task_switch(nxt_task);
        ENABLE
     // io_task_busy=1;
        BITSET(cur_task-1,io_task_busy)
        endwhile
}
void chk_io()
{              //assumes all time wait devices on same pcchan
    int chan=thead->pcchan;
    if ((io_task_busy and (1<<(chan-1))) eq 0)
        task_switch(chan);
}

