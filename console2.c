//#include "sigma.h"
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include <stdio.h>   /* for fprintf(stderr,... */
#include <termios.h>
#include <string.h>
#include <stdlib.h>
        main(int argc,char *argv[])
//        console(fdin, fdout, flag, bit);
{
   int keyboard,screen,pipei,pipeo; 
   char c,rc;
   struct termios tio;
   char * pipeifid = "pipei";
   char pipeofid[120]="pipeo";
      if (argc > 1) {
           pipeifid = argv[1];
           strcpy(pipeofid,pipeifid);
           strcat(pipeofid,"o");
           }

      fd_set readfds,writefds,exceptfds;
      struct timeval tv;
      int max_fd;

      /* inbound and outbound keep track of whether we have a character */
      /* already read which needs to be sent in that direction */
      /* the _char variables are the data buffer */
      int outbound,inbound;
      char outbound_char,inbound_char;

   keyboard = open("/dev/tty",O_RDONLY| O_NONBLOCK);
   assert(keyboard>=0);
   screen   = open("/dev/tty",O_WRONLY| O_NONBLOCK);
   assert(screen>=0);
   pipei   = open(pipeofid,O_RDONLY| O_NONBLOCK);
   assert(pipei>=0);
   pipeo = open(pipeifid,O_WRONLY  );
   assert(pipeo>=0);
   set_tty_raw();
//   if(debug) {
//     fprintf(stderr, "keyboard=%d\n",keyboard);
//     fprintf(stderr, "screen=%d\n",screen);
//     fprintf(stderr, "serial=%d\n",serial);
//   }

      outbound = inbound = 0;
      while(1) {
	 FD_ZERO(&writefds);
	 if(inbound) FD_SET(screen, &writefds);
	 if(outbound) FD_SET(pipeo, &writefds);
	 FD_ZERO(&readfds);
	 if(!outbound) FD_SET(keyboard, &readfds);
	 if(!inbound) FD_SET(pipei, &readfds);

	 max_fd = 0;
	 if(pipei > max_fd)      max_fd=pipei;
         if(pipeo > max_fd)      max_fd=pipeo;
	 if(keyboard > max_fd)   max_fd=keyboard;
	 if(screen > max_fd)     max_fd=screen;
         max_fd++;
//         if(debug) fprintf(stderr, "max_fd=%d\n",max_fd);

	 tv.tv_sec = 10;
	 tv.tv_usec = 0;

//         dump_fds("read in", &readfds, max_fd);
//         dump_fds("write in", &writefds, max_fd);

	 rc= select(max_fd, &readfds, &writefds, NULL, &tv);
         if(rc==0) continue;  /* timeout */

//         dump_fds("read out", &readfds, max_fd);
//         dump_fds("write out", &writefds, max_fd);


	 if(FD_ISSET(keyboard, &readfds)) {
	    rc=read(keyboard,&outbound_char,1);
//            if(debug) fprintf(stderr, "\nreading outbound=%d\n",outbound_char);
	    if(rc==1) outbound=1;
            if(outbound == 3) exit(0);
	 }

	 if(FD_ISSET(pipei, &readfds)) {
	    rc=read(pipei,&inbound_char,1);
//            if(debug) fprintf(stderr, "\nreading inbound=%d\n",inbound_char);
            if((inbound_char >= 32) || 
               (inbound_char == '\r') || 
               (inbound_char == '\n') ||
               (inbound_char == '\t'))  if(rc==1) inbound=1;
	 }

	 if(FD_ISSET(screen, &writefds)) {
//            if(debug) fprintf(stderr, "\nwriting inbound\n");
	    rc=write(screen,&inbound_char,1);
	    if(rc==1) inbound=0;
	 }

	 if(FD_ISSET(pipeo, &writefds)) {
//            if(debug) fprintf(stderr, "\nwriting outbound\n");
	    rc=write(pipeo,&outbound_char,1);
	    if(rc==1) outbound=0;
	 }

      }

}





