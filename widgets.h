#include <gtk/gtk.h>

typedef struct tag_wtree{
       GtkWidget * me;
       struct tag_wtree  * child_first;
       struct tag_wtree  * child_last;
       struct tag_wtree  * prev;
       struct tag_wtree  * next;
       struct tag_wtree  * parent;
       struct tag_wtree  * root;
       void * pdata;
       char * name;
       int  idata;
       } wtree;

#define ADDNODE(i,p) \
{ typeof(i)  t1,t2; \
i->root=p->root;     \
i->parent = p;       \
i->next = 0;         \
i->prev = 0;         \
i->child_first = 0;  \
i->child_last = 0;   \
t1=p->child_first;   \
t2=p->child_last;    \
if (t2) {t2->next=i;  \
    i->prev=t2;       \
    p->child_last=i;} \
  else p->child_first=p->child_last=i; \
i->name=i->pdata=NULL;   \
i->idata=0;    \
}

#define WTop(wtree,title) { \
 wtree->me =gtk_window_new(GTK_WINDOW_TOPLEVEL); \
 gtk_window_set_title(GTK_WINDOW (wtree->me), title);  \
 g_signal_connect ( G_OBJECT (wtree->me), "destroy", G_CALLBACK (gtk_main_quit), NULL); \
 {GtkWidget *ME = wtree->me; \
 wtree->root=wtree; \
 gtk_container_set_border_width (GTK_CONTAINER (ME), 10);  \
wtree->child_first=wtree->child_last=wtree->parent=wtree->next=wtree->prev=0; \
wtree->name=0; \
}}

#define NewNode(p,x) ({ \
wtree * m = (wtree *)g_malloc(sizeof(wtree));  \
ADDNODE(m,p)  \
m->me=x;     \
m;})

#define WNew(parnt,type, args... ) ({ \
GtkWidget ** t = (GtkWidget **)g_malloc(sizeof(GtkWidget *)); \
wtree * mytree;           \
*t = type ## _new (args) ; \
mytree=NewNode(parnt,*t);    \
gtk_container_add (GTK_CONTAINER (parnt->me) , *t); \
gtk_widget_show (*t); \
/*if (rout) g_signal_connect ( G_OBJECT(*t), #type, G_CALLBACK (rout), mytree);*/ \
mytree;})


#define WPut(parnt,type,x,y, args... ) ({ \
GtkWidget ** t = (GtkWidget **)g_malloc(sizeof(GtkWidget *)); \
wtree * mytree;           \
*t = type ## _new (args) ; \
mytree=NewNode(parnt,*t);    \
gtk_fixed_put (GTK_FIXED (parnt->me) , *t, x, y); \
gtk_widget_show (*t); \
mytree;})

#define SigCon(t,s,r)  g_signal_connect (G_OBJECT(t->me), s, G_CALLBACK(r), t);

#define WSize(wt,x,y) gtk_widget_set_size_request(wt->me,x,y);

#define WName(wt,n) wt->name = n;

#define WnSize(wt,x,y) gtk_window_set_default_size (GTK_WINDOW(wt->me),x,y);

#define TButton(wt,xsz,ysz,rt) ({wtree * t=WNew(wt,gtk_toggle_button); \
  WSize(t,xsz,ysz);   \
  gtk_button_set_relief(GTK_BUTTON(t->me), GTK_RELIEF_NONE); \
  if (rt) SigCon(t,"clicked",rt);t;})

#define Button(wt,xsz,ysz,rt) ({wtree * t=WNew(wt,gtk_button); \
  WSize(t,xsz,ysz);   \
  if (rt) SigCon(t,"clicked",rt);t;})

#define FTButton(wt,x,y,xsz,ysz,rt) ({   \
  wtree * tt = WPut(wt,gtk_fixed,x,y);  \
  wtree * t = TButton(tt,xsz,ysz,rt); *t;})

#define FButton(wt,x,y,xsz,ysz,rt) ({   \
  wtree * tt = WPut(wt,gtk_fixed,x,y);  \
  wtree * t = Button(tt,xsz,ysz,rt); *t;})

#if 0
#define MFTButton(wt,x,y,xsz,ysz,rt,n) ({ \
  int i;     \
  wtree * tn = WPut(wt,gtk_fixed,x,y);  \
  wtree * tt  = WNew(tn,gtk_hbox,FALSE,0)  ;  \
  WSize(tt,xsz,ysz); \
  for (i=0; i<n ;i++) {TButton(tt,xsz/n,ysz,rt);} })
#endif

#define MFTIButton(wt,x,y,xsz,ysz,rt,n,id,pd) ({ \
  int i,j,d,i1,i2,dy;  \
  int w0= xsz/n;  \
  i1=2 * (xsz mod n); d=i1-n; i2=d-n; \
  wtree * tn = WPut(wt,gtk_fixed,x,y);  \
  wtree * tt  = WNew(tn,gtk_hbox,FALSE,0)  ;  \
  wtree * b;       \
  WSize(tt,xsz,ysz); \
  for (i=0; i<n ;i++) {  \
     if (d < 0) {d=d+i1; j=0;}  \
       else     {d=d+i2; j=1;}  \
     b=TButton(tt,w0+j,ysz,rt); \
     b->idata=id;         \
     b->name = #id;   \
     b->pdata = pd; } })


#define FTIButton(wt,x,y,xsz,ysz,rt,id,pd) ({   \
  wtree * tt = WPut(wt,gtk_fixed,x,y);  \
  wtree * t = TButton(tt,xsz,ysz,rt);  \
  t->idata = id; \
  t->name = #id; \
  t->pdata = pd; t;})

#define ACTION(z) void z (GtkWidget *w, wtree *t)

#define SMFTIButton(a1,a2,a3,a4,a5,a6)  \
({ MFTIButton(a1,a2[a5].x,a2[a5].y,a2[a5].xsz,a2[a5].ysz,a3,a4,a5,a6);})

#define SFTIButton(a1,a2,a3,a4,a5) \
({ FTIButton(a1,a2[a4].x,a2[a4].y,a2[a4].xsz,a2[a4].ysz,a3,a4,a5);})

