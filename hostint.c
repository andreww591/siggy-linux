#define fast 1
#include "sigma.h"
#include "idlop.h"
#include "dosint.h"
#include "coccom.h"
#include <stdio.h>

#include <signal.h>
#include <time.h>
#include <sched.h>
#include <unistd.h>
#include <stdlib.h>

#define freq 500
int interrupts_active=0;
extern SEMAPHORE_DATA(DE);
//extern SEMAPHORE_DATA(KB);
extern verbose;
#define VERB_LEVEL V_DEBUG
void fastclock();
void normclock();
#define errExit(msg)    { perror(msg); exit(EXIT_FAILURE); }


//----------------BEGIN KEEP TOGETHER IN ORDER SECTION-------------
#include "volatiles.h"

void tic_handler()
{
    event|=clock_evt;
    tics++;
//    if ((tics & 0xf) == 0 ) event|=kbd_evt;    //kludge for keyboard checking
}
//void kbd_handler()
//{
//    event|=kbd_evt;
//    keys_pressed++;
//}

void lock_my_handler()
{}
//-------------END LOCKED CODE REGION-----------------------------
//----------------END KEEP TOGETHER IN ORDER SECTION--------------

void activate_clock()
{
          V_CHK(printf("activate clock called\n\r");)
          V_CHK(printf("Establishing handler for signal %d\n\r", SIGRTMIN);)
          {
           struct sigaction sa;
           sa.sa_flags = SA_SIGINFO ;  //| SA_RESTART;
           sa.sa_sigaction = tic_handler;
//           sa.sa_handler = tic_handler;
           sigemptyset(&sa.sa_mask);
           if (sigaction(SIGRTMIN, &sa, NULL) == -1)
               errExit("activate clock sigaction");
           }
           /* Create the timer */
          {
           struct sigevent sev;
           sev.sigev_notify = SIGEV_SIGNAL;
           sev.sigev_signo = SIGRTMIN;
//           sev.sigev_value.sival_ptr = &timerid;
           if (timer_create(CLOCK_REALTIME, &sev, &timerid) == -1)
               errExit("activate clock timer_create");
           }

          V_CHK(printf("timer ID is 0x%lx\n", (long) timerid);)

           /* Start the timer */
           {
             if (start_clock() == -1)
                errExit("activate clock timer_settime");
           }


}
int start_clock()

{
           struct itimerspec its;
           long long freq_nanosecs = 1000000000 / freq;
           its.it_value.tv_sec = freq_nanosecs / 1000000000;
           its.it_value.tv_nsec = freq_nanosecs % 1000000000;
           its.it_interval.tv_sec = its.it_value.tv_sec;
           its.it_interval.tv_nsec = its.it_value.tv_nsec;
//           set_tty_raw();
           return (timer_settime(timerid,0,&its,NULL));
}
void stop_clock()
{
           struct itimerspec stop;
           stop.it_value.tv_sec=stop.it_value.tv_nsec=stop.it_interval.tv_sec=stop.it_interval.tv_nsec=0;
           timer_settime(timerid,0,&stop,NULL);
//           set_tty_cooked();
}

void release_clock()
{
     int i;
     stop_clock();
     V_CHK(printf("release clock called\n\r");)
     set_tty_cooked();
     V_CHK(printf("set_tty_cooked called\n");)
     output(tics);
     SEMAPHORE_CLOSE(DE);
//     SEMAPHORE_CLOSE(KB);
     set_led(0);    //turn off ttylight
     for (i=9; i>=0; i--) {
          if (id_kid[i]) {V_CHK( printf("killing pid #%d = %d\n",i,id_kid[i]);)
                        kill(id_kid[i],SIGKILL);
                       }
          }


}

