int tics=0;      //first data item
int dt=0;
int com_flag=0;
int com_in_flag=0;
int defined_coms=0;
//int comport[5]={0, 0x3f8, 0x2f8, 0x3e8, 0x2e8};
       //   com1 3f8   com2 2f8   com3 3e8   com4 2e8
         //     IRQ4       IRQ3      IRQ4        IRQ3
         // int 0x0C       0x0B      0x0C        0x0B
int comport[NP]={0};
int ptype[NP]={0};
int baud[NP];  //={0,9600,9600,9600,9600};
//unsigned char com_in[5];
unsigned char ring[256];    //make size a power of two
int ringii=0,ringor=0;
int com_modemstat[NP];
int com_linestat[NP];
unsigned char sv21,sv3[NP],sv1[NP],sv4[NP],svb0[NP],svb1[NP];
unsigned char mcr[NP];
unsigned char lcr[NP];
int comx[8];   //={0,0,0,0,0,0,0,0};  //comx port head indexed by irqn
int comxl[NP]; //={0,0,0,0,0,0,0,0,0,0}; //comx flink
//int comx[8]={0,0,0,2,1,3,0,0};  //comx port head indexed by irqn
//int comxl[5]={0,0,0,4,0};    //this links com4 to com2 on irq3
                             // and com1 to com3 on irq4
int icoms[NP]={0,0,0,0,0,0,0,0,0,0}; //com interrupt counters


int irqs[16]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; //irq interrupt counters
int i15c[256]={ [0 ... 255] = 0};    //count int 15 function calls
int i1590c[256]={ [0 ... 255] = 0};    //count int 15 90 function calls by device
int i1591c[256]={ [0 ... 255] = 0};    //count int 15 91 function calls by device
volatile int waitdev[256]={ [0 ... 255] =0};  //this device is waiting
int task_wd[256]={ [0 ... 255] = 0};   //task waiting for this device to complete
int task_ready;                        //tasks ready to run (i.e. wait completed)
int waitderr[256]={ [0 ... 255] = 0};  //device int complete when not waiting
int idlec=0,lto=0;
int i76c=0;
//_go32_dpmi_registers r15,r_hd;


static char* cb_stack;
int cur_task=0;

int w_task=-1;    //no task change on wait interrupts
int event=0;      //last data item
//int keys_pressed=0;   //SIGINT counter
timer_t timerid;
//pid_t pidk=0;
int nkids=0;
pid_t id_kid[10];
pid_t main_pid;
int M16B=0xffff;    //use where value is dependent on BIG
int M19B=0x7ffff;    //use where value is dependent on BIG
int M8B=0xff;        //use where value is dependent on BIG
