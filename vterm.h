#define printf(f, a...) \
     vterm_printf(V_USER,f, ## a)
#undef NEWLINE
#define NEWLINE vterm_printf(V_USER,"\n");
