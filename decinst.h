#define DEC_XFER(src,dst) dst.d[0]=src.d[0]; dst.d[1]=src.d[1];
#define DEFADDER
void decadd(dec *deca, byte sa, dec *decb, byte sb, int add, dec *decs, byte *ss, byte *SC0);
#define dmadder() decadd(&deca,sa,&decb,sb,add,&decs,&ss,&SC0)
#define DECINIT word i,j,k,r=((REG(inst)-1)&15)+1,rem,drem,dadd=IEAX(inst,byte,2); \
                byte * idadd, * r15b3=&SLOC(reg,(15<<2)+3,byte),sa,sb,sl,ss,SC0,S4;  \
                dword * r12=&SLOC(reg,12>>1,dword); dec deca,decb,decs; S6CC&=3;

#define DECCHECK(DEC)    for (i=15;i;i--)                  \
    {if ((DEC.b[i]>>4 gt 9) or ((DEC.b[i] and 0xf) gt 9))  \
        {S6CC|=8; if (s6.dm) TRAP45; BREAK;}}             \
    if (((DEC.b[0]>>4 gt 9)) or ((DEC.b[0] and 0xf) le 9)) \
        {S6CC|=8; if (s6.dm) TRAP45; BREAK;} 

//#define DECSIGNADJNZ(DEC,S)                                                   \
//             if ((DEC.d[1] ne 0) or (DEC.d[0] and 0xfffffffffffffff0LL) ne 0) \
//             DECSIGNADJZ(DEC,S);
#define DECSIGNADJNZ(DEC,S) DECSIGNADJZ(DEC,S)

#define DECREMSIGN(DEC) DEC.b[0]&=0xf0;
#define DECSIGNADJZ(DEC,S) {DECSAVESIGN(DEC,S); DECPUTSIGN(DEC,S)}
#define DECPUTSIGN(DEC,S) DEC.b[0]=(DEC.b[0] and 0xf0)|12+(S&1);

#define DECSAVESIGN(DEC,S) S=DEC.b[0] and 0xf;              \
                     S=((S eq 0xb) or (S eq 0xd)) ? 1:2;    \
                     if((DEC.d[1] eq 0) and ((DEC.d[0] and 0xfffffffffffffff0LL) eq 0)) S=0;

#define DECFRMEM(DEC) rem=r; k=r-1;                                         \
                  while(rem) {idadd=pRxMEM2(dadd,byte);drem=RXMREMb(idadd); \
                     i=MIN(drem,rem); for (j=i;j;j--) DEC.b[k--]=*idadd--;  \
                     dadd+=i; rem-=i;}

#define DECTOMEM(DEC) rem=r; k=r-1;                                             \
                  while(rem) {idadd=pRxMEM0(dadd,byte);drem=RXMREMb(idadd); \
                     i=MIN(drem,rem); for (j=i;j;j--) *idadd--=DEC.b[k--]; \
                     dadd+=i; rem-=i;}
                
#define DECFRREGS(DEC) DEC.d[1]=*r12; DEC.d[0]=*(r12-1);
#define DECTOREGS(DEC) *r12=DEC.d[1]; *(r12-1)=DEC.d[0];
#define DECZERO(DEC)   DEC.d[1]=DEC.d[0]=0LL; //was 0x0c into DEC.d[0]
//#define dec_adder(a,b,s) decadd(&dec##a,s##a,&dec##b,s##b,add,&dec##s,&s##s,&SC0)

#define ADD1C addc=&decmc;
#define ADD2C addc=&dec2mc;
#define RPMS decadd(addc,sa,&decr,sr,1,&decr,&sr,&SC0);

#define DECSHIFTL(decmc)   decmc.d[1]=(decmc.d[1]<<4)|(decmc.b[7]>>4);  \
                  decmc.d[0]=decmc.d[0]<<4;

#define DS 0x20
#define SS 0x21
#define FS 0x22
#define SI 0x23
#define PUTEBSD {bsr.w.w1=(count<<24)|dadd; bsr.w.w0=(fill<<24)|(sadd-disp); LDl(r,bsr.dw);}
#define EBSINIT udw bsr; word count,disp,r,sadd,dadd,fill,*r1; byte *isadd,*idadd,kk; \
                bsr.dw=RRu1; count=((uword) bsr.w.w1)>>24; disp=IMMED(inst);  \
                sadd=(bsr.w.w0 and M19vx)+disp; dadd=(bsr.w.w1&=M19vx);            \
                r=REG(inst); fill=bsr.w.w0>>24;r1=&SLOC(reg,1,word); 
        INST(0x63) if (xerox eq 9) {NODDR; if (!(inst and 0x00f00000)) TRAP4DCC4;}
/*EBS*/            NOIND; {byte zone=0xf0-0xc0*s6.asc;byte space=0x40>>s6.asc; EBSINIT
/*1*/              while (count) {isadd=pRxMEM2(sadd,byte); idadd=pRxMEM0(dadd,byte); 
                   kk=*isadd; //could minimize recalculation of isadd and idadd
                      switch (*idadd) {
/*2a*/                   case DS: case SS: case SI:
                            if ((S6CC&4) eq 0) kk=kk>>4; else kk&=0xf;
/*2b*/                      if (kk gt 9) {PUTEBSD; TRAP45;}
/*2c*/                      if (kk) S6CC|=2;
/*2d*/                      switch (*idadd) {
     /*f*/                     case SI: *idadd=kk|zone; S6CC|=1; *r1=dadd; break;
                               case SS: *idadd=(((S6CC&1) eq 0) and (kk eq 0)) ? fill:(kk|zone);
     /*f*/                        if ((S6CC&1) eq 0) *r1=dadd+(kk eq 0); S6CC|=1; break;
                               case DS: *idadd=(((S6CC&1) eq 0) and (kk eq 0)) ? fill:(kk|zone);
     /*f*/                        if (((S6CC&1) eq 0) and (kk ne 0)) {S6CC|=1;*r1=dadd;} break;}
/*2e*/                      if (((S6CC&4) eq 0) and ((kk=*isadd&0xf) gt 9)) {
                               if ((kk eq 0xb) or (kk eq 0xd))
                                  {S6CC|=9; sadd++;} else {S6CC|=8; S6CC&=0xe; sadd++;}} 
                                  else {sadd+=(S6CC>>2)&1; S6CC=(S6CC&0xb)|(4-(S6CC&4));} 
                            break;
/*3*/                    case FS: *idadd=fill; S6CC&=4; break;
/*4*/                    default: 
                            if ((S6CC & 9) eq 0) *idadd=fill;
                            if ((S6CC & 9) eq 8) *idadd=space; break;}
/*5*/                 idadd--; dadd++; count--; PUTEBSD;}} BREAK;

/*PACK*/INST(0x76)  {DECINIT; DECZERO(deca); rem=r-1; k=r-1;
                  while(rem) {idadd=pRxMEM2(dadd,byte);drem=RXMREMb(idadd); 
                     i=MIN(drem,rem);
                     for (j=i;j>0;j--)
                        {deca.b[k]=(*idadd--<<4); deca.b[k]|=(*idadd--)&0xf; k--;}
                     dadd+=i+i; rem-=i;}
                  idadd=pRxMEM2(dadd,byte); //esp. L=1=>no while!
                  deca.b[0]=(*idadd>>4)|(*idadd<<4);
                  DECCHECK(deca); DECTOREGS(deca); DECSIGNADJZ(deca,sa);
                  S6CC=sa; *r15b3=deca.b[0];} BREAK;

/*UNPK*/INST(0x77)  {byte zone=0xf0-0xc0*s6.asc;
                     DECINIT; DECFRREGS(deca); DECCHECK(deca); //for Sigma 9 add clean trap
                     DECSIGNADJNZ(deca,sa); rem=r-1; k=r-1;
                  while(rem) {idadd=pRxMEM0(dadd,byte);drem=RXMREMb(idadd); // divide by 2!
                     i=MIN(drem,rem);
                     for (j=i;j;j--)
                        {*idadd--=(deca.b[k]>>4)|zone; *idadd--=(deca.b[k]&0xf)|zone; k--;}
                     dadd+=i+i; rem-=i;}
                  idadd=pRxMEM0(dadd,byte); //esp. L=1=>no while!
                  *idadd=(deca.b[0]>>4)|(deca.b[0]<<4);
                  i=16; while(i-- gt r) {if (deca.b[i]) S6CC|=4;}} BREAK;

/*DS*/  INST(0x78)  add=0; goto decadd;              
/*DA*/  INST(0x79)  add=1;
decadd:          {DECINIT; DEFADDER;
                  DECFRREGS(deca); DECCHECK(deca);
                  DECSAVESIGN(deca,sa);
                  DECZERO(decb); DECFRMEM(decb); DECCHECK(decb);
                  DECSAVESIGN(decb,sb);
                  DECREMSIGN(deca); DECREMSIGN(decb); dmadder();
                  if (SC0) {S6CC=4; if (s6.dm) TRAP45; BREAK;}
                  DECPUTSIGN(decs,ss); DECTOREGS(decs); S6CC=ss;} BREAK;

/*DD*/  INST(0x7A) {dec decdd,decdr,decr; byte sd,sr; word kk,ii,kint=0; long long decq;
                  DECINIT; if (r gt 8) {S6CC|=8; if (s6.dm) TRAP45; BREAK;}
                  DEFADDER; DECFRREGS(decdd); 
                  for (i=15;i>7;i--) {
                     if (decdd.b[i]>>4 gt 9) goto intddl;
                     if ((decdd.b[i] and 0xf) gt 9) goto intddr;}
                  DECCHECK(decdd); DECZERO(decdr); DECFRMEM(decdr); DECCHECK(decdr);
                  DECSAVESIGN(decdd,sd); DECSAVESIGN(decdr,sr); DECREMSIGN(decdd); DECREMSIGN(decdr);
                  if ((decdr.d[1] ne 0) or (decdr.d[0] eq 0) or
                      ((decdd.d[1] ne 0) and (decdd.d[1] ge (decdr.d[0]>>4)))) 
                        {S6CC|=4; if (s6.dm) TRAP45; BREAK;}
                  kk=0;for (i=15;i ge 0;i--) {
                     if (decdd.b[i]>>4 ne 0) {kk=i+i+1; break;}
                     if ((decdd.b[i] and 0xf) ne 0) {kk=i+i; break;}}
                  ii=0;for (i=7;i ge 0;i--) {
                     if (decdr.b[i]>>4 ne 0) {ii=i+i+1; break;}
                     if ((decdr.b[i] and 0xf) ne 0) {ii=i+i;break;}}  
                  if (kk < ii) {DECPUTSIGN(decdd,sd); decdd.d[1]=decdd.d[0]; decdd.d[0]=0xcLL;
                                DECTOREGS(decdd); S6CC=0; BREAK;}
                  if (kk ne ii) {decdr.d[1]=decdr.d[0]>>(64-4*(kk-ii)); decdr.d[0]=decdr.d[0]<<(4*(kk-ii));} //64==0 shift!
                  decq=0LL; kint=0;
intddcont:        for (ii=1+kk-ii;ii;ii--) {
                     for (kk=kint;kk<10;kk++) {sa=2; sb=2; add=0; //s=dd-dr
                        decadd(&decdd,2,&decdr,2,0,&decs,&ss,&SC0);
                        if (ss le 1) {decq=(decq<<4)|kk; 
                           decdr.d[0]=(decdr.d[0]>>4)|(decdr.d[1]<<60); decdr.d[1]=decdr.d[1]>>4; break;}
                        else {decdd.d[1]=decs.d[1]; decdd.d[0]=decs.d[0];}} kint=0;}
                  sl=2; if(sd ne sr) sl=1; if (decq eq 0) sl=0; decq=(decq<<4)|(12+(sl&1)); 
                  DECPUTSIGN(decdd,sd); decdd.d[1]=decdd.d[0]; decdd.d[0]=decq;
                  DECTOREGS(decdd); S6CC=sl; BREAK;
// do I worry about signs other than C and D here? intddr is untest by DEC.
intddl: sd=14-(decdd.b[i]>>4);  decdd.b[i]=decdd.b[0]<<4; sa=0; goto intddgo;
intddr: sd=14-(decdd.b[i]&0xf); decdd.b[i]=(decdd.b[i]&0xf0)|(decdd.b[0]&0xf); sa=1;
intddgo: decq=decdd.d[1]>>((i-8)*8+4*(1-sa)); kint=decq&0xf; decq=decq>>4;  ii=0;
        decdd.b[0]&=0xf0; for (k=15;k ge i;k--) decdd.b[k]=0; sa=(i+i-16-sa)*4; kk=sa>>2;
        DECZERO(decdr); DECFRMEM(decdr); DECSAVESIGN(decdr,sr); DECREMSIGN(decdr); 
        if (sa ne 0) {decdr.d[1]=(decdr.d[1]<<sa)|(decdr.d[0]>>(64-sa)); decdr.d[0]=decdr.d[0]<<sa;}
        goto intddcont; }

/*DM*/  INST(0x7B) {dec decmr,decmc,decr,dec2mc,*addc; byte sr,isgn; word kk,ii,kint=0;
                  signed long long mask;
                  DECINIT; DEFADDER; DECFRREGS(decmr);
                  DECZERO(decr); sr=0;
                  DECZERO(decmc); DECFRMEM(decmc); DECCHECK(decmc);
                  for (i=15;i>=0;i--) begin    //check for interrupted
                     int ib=(i and 1) * 4;
                     int ix=8+i/2;
                     int nib=(decmr.b[ix]>>ib) and 0xf;
                     if (nib gt 9) begin       //sign in this nibble
                        byte ppneg=((nib >> 2) and 1) eor 1; //sign a or b
                        int curdig=(decmr.b[0] and 0xf)+ppneg;
//                      printt("curdig = ") printd(curdig) NEWLINE
                        STS(decmr.b[ix],curdig<<ib,0xf<<ib);
                        decmr.d[0]=decmr.d[1]>>(i*4-4);
                        decmr.d[1]=0LL;
                        STS(decmr.b[0],nib,0xf);
//                      printt("decmr.d[0] = ") printdw(decmr.d[0]) NEWLINE
                        DECFRREGS(decr);
                        DECREMSIGN(decr);
                        mask=0x8000000000000000LL;
                        mask=~(mask>>((16-i)*4-1));
                        decr.d[1]=decr.d[1] & mask;
                        kint=i;
                        if (ppneg) begin
                            for (i=15;i;i--) if(decr.b[i]) break; else decr.b[i]=0x99;
                            endif
//                      printt("decr.d[1] = ") printdw(decr.d[1]) NEWLINE
//                      printt("decr.d[0] = ") printdw(decr.d[0]) NEWLINE
                        sr=2;
                        break;
                        endif
                     endfor
                  if ((r gt 8) or (decmr.d[1] ne 0)) {S6CC|=8; BREAK;}
                  DECSAVESIGN(decmr,sa); DECSAVESIGN(decmc,sb);
                  S6CC=2-(sa ne sb); DECREMSIGN(decmr); DECREMSIGN(decmc);
                  if ((decmr.d[0] eq 0) or (decmc.d[0] eq 0)) 
                     {decmc.d[0]=0xcLL; DECTOREGS(decmc); S6CC=0; BREAK;}
                  decadd(&decmc,2,&decmc,2,1,&dec2mc,&ss,&SC0);
               for(kk=1;kk<=16;kk++) begin
                 if (kk ge kint) begin
                  decmr.d[0]=decmr.d[0]>>4; ii=decmr.b[0]&0xf;
                  switch (ii) {
                     case 3: 
                     case 1:  ADD1C;sa=2;RPMS; if (ii eq 3) goto case2; else break;
                     case 5:  ADD1C;sa=2;RPMS;
                     case 4:  ADD2C;sa=2;RPMS;
                     case 2:  case2: ADD2C;sa=2;RPMS;
                     case 0:  break;
                     case 9:  
                     case 7:  ADD1C;sa=1;RPMS; if (ii eq 7) goto case8; else goto casea;
                     case 6:  ADD2C;sa=1;RPMS;
                     case 8:  case8: ADD2C;sa=1;RPMS;
                     case 10: casea: decmr.b[0]+=0x10; break;
                     endswitch //note k=0-15 for last time DM03&DM16
                  endif
                 DECSHIFTL(decmc)
                 DECSHIFTL(dec2mc)
                 endfor
//               if (decmr.b[0] ne 0) {outputx(decmr.b[0]);  PCP_BREAK;}
               sr=S6CC; DECPUTSIGN(decr,sr); DECTOREGS(decr); BREAK;}

/*DSA*/ INST(0x7C)  {word iea=IEA(inst,2), irx=field(inst,rx), ci=iea;
                    DECINIT; DECFRREGS(deca); DECCHECK(deca); S6CC=0;
                    if ((deca.d[1] eq 0) and (deca.d[0] and 0xfffffffffffffff0LL) eq 0) {S6CC=0; BREAK;}
                    DECSAVESIGN(deca,sa); DECREMSIGN(deca);
                    if (irx) ci+=(SLOC(reg,irx,word)<<13)>>15; ci=(ci<<16)>>16;
 if (ci gt 0) {if (ci gt 31) ci=31;
/*left*/         if(ci&1 eq 1) {ci--; if ((deca.b[15]&0xf0) ne 0) S6CC|=4;
      /*odd*/       for(i=15;i>0;i--) deca.b[i]=(deca.b[i]<<4)|(deca.b[i-1]>>4);deca.b[0]=0;}
      /*even*/   if(ci) {for(i=15;i>15-(ci>>1);i--) {if (deca.b[i] ne 0) {S6CC|=4; break;}}
                       for(i=15;i ge ci>>1;i--) deca.b[i]=deca.b[i-(ci>>1)];
                       for(i=(ci>>1)-1;i ge 0;i--) deca.b[i]=0;}}
/*right*/     else {if (ci lt -31) ci=-31; S6CC&=0xb;
      /*odd*/    if(ci&1 eq 1) {ci++; for(i=1;i<16;i++) deca.b[i-1]=(deca.b[i-1]>>4)|(deca.b[i]<<4); 
                        deca.b[15]=deca.b[15]>>4;}
      /*even*/   if(ci) {ci=-ci; for(i=0;i le 15-(ci>>1);i++) deca.b[i]=deca.b[i+(ci>>1)]; 
                         for(i=15;i>15-(ci>>1);i--) deca.b[i]=0;}}
              deca.b[0]=deca.b[0]&0xf0|(14-sa); DECTOREGS(deca);
              if ((deca.d[1] ne 0) or (deca.d[0] and 0xfffffffffffffff0LL) ne 0) S6CC|=sa;} BREAK;

/*DC*/  INST(0x7D) {DECINIT; DECFRREGS(deca); DECCHECK(deca);
                  DECZERO(decb); DECFRMEM(decb); DECCHECK(decb);
                  DECSAVESIGN(deca,sa); DECSAVESIGN(decb,sb);
                  DECREMSIGN(deca); DECREMSIGN(decb);   
               if ((decb.d[1] eq deca.d[1]) and (decb.d[0] eq deca.d[0]) and 
                   ((sa eq sb) or ((sa eq 0) and (sb eq 0)))) {S6CC=0; BREAK;}
               if (sa ne sb) {S6CC=sa; BREAK;}
         S4=2; if ((decb.d[1] gt deca.d[1]) or 
                  ((decb.d[1] eq deca.d[1]) and decb.d[0] gt deca.d[1])) S4=1;
               if ((sa eq sb) and (sa eq 1)) S4=3-S4; S6CC=S4;} BREAK;

/*DL*/  INST(0x7E) {DECINIT; DECZERO(deca); DECFRMEM(deca); DECCHECK(deca); DECTOREGS(deca);
                    DECSIGNADJNZ(deca,sa); S6CC=sa; *r15b3=deca.b[0];} BREAK;

/*DST*/ INST(0x7F) {DECINIT; DECFRREGS(deca); DECCHECK(deca); //for Sigma 9 add clean trap
                    DECSIGNADJNZ(deca,sa); DECTOMEM(deca);
                    i=16; while(i-- gt r) {if (deca.b[i]) S6CC|=4;}} BREAK;
