
#define FORKSYS(cmd) ({pid_t cpid; \
    cpid = fork();                    \
    if (cpid == -1) { perror("fork"); exit(EXIT_FAILURE); }   \
    if (cpid == 0) {                                      \
        system("konsole -e ./console2");                  \
        _exit(EXIT_SUCCESS);}      \
     cpid;})

#define CLONE(stksize,func,a...)  ( {            \
    int *stack_bot=(int*)malloc(stksize)+stksize;    \
    clone(func,stack_bot,CLONE_VM | CLONE_FILES, ## a);})

#define CLONEF(stksize,func,a...)  ( {            \
    int *stack_bot;                             \
    int x;                                      \
    stack_bot=(int*)malloc(stksize)+stksize;    \
    /*assert(x > 0); */                         \
    clone(func,stack_bot,CLONE_VM | CLONE_FILES, ## a);})
