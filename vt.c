#include "sigma.h"
#include "idlop.h"
#include "sigmaio.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
 
extern user;
extern devdata *userdev[];

int ttylight=0;      

//vterm_init()
//{
//printf("vterm_init called\n");
//kbd_init();
//}

int last_attr=0;
int vterm_attr(int usr, int atr)
{
int last=last_attr;
if (atr ne last_attr) printf("\x1b[%dm",atr); 
last_attr=atr;
return last;
}


void vterm_write(int usr,int d)
{
      short int k=d and 0x7f;
//      putch(k);fflush(stdout);
      printf("%c",k); fflush(stdout);
      return;
}

void vterm_printf(int usr, const char *s, ...)
{   char buf[2048];
    int c,cnt;
    va_list ap;
    const unsigned char *ss = buf;  //(const unsigned char *) s;
    va_start(ap,s);
    cnt=vsprintf(buf,s,ap);
    va_end(ap);
    while ((c = *ss++)) begin
        vterm_write(usr, c);
        if (c eq '\n') vterm_write(usr,'\r');
        endwhile
    //return cnt;
}

