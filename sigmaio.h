#include <stdio.h>
typedef void (*HANDLER)();
typedef struct tag_dev {
    struct tag_ctlr *parent;        //pointer to this device's controller
    struct tag_dev *flink;          //link to next device on this controller
    byte address;                   //device address
    byte TDV_status;                //TDV status
    byte TSH_status;                //device portion of tsh status
    byte AIOcc;                     //AIO cc 4=> unusual interrupt recognition
    word AIOstatus;                 //AIO staus
    HANDLER handler;                //pointer to device's handler function
    HANDLER hio_handler;            //pointer to device's special hio handler
    uword pbit;                     //this devices priority bit to set in controllers ic (multi device controllers)
    byte * fid;                     //HOST file or device to connect to      
    byte * buffer;                  //address of a data buffer allocated for HOST R/W
    word size;                      //size of above data buffer
    byte * sense;                   //address of a buffer for sense info
    word ssize;                     //size of sense buffer
    word heads;                     //number of heads
    word nspt;                      //number sectors per track
    word ars;                       //actual record size now in buffer
    word orp;                       //output removal pointer
    FILE*  HOST;                    //file handle for HOST open
    word filesize;                  //size of dos file in bytes when opened
    word recno;                     //record number (info only, wont track tape space file operations)
    word fileno;                    //mag tape block number
    word pipeo;                     //TY output fd
    word stop;                      //debugging magic - stop at record number
    word HOSTtype;                   //record types for HOST connection
    byte HOSTmode[4];                //fopen modes (e.g. "rb" "wb" etc)
    word fpos;                      //current position in file;
    word lastblock;                 //file pos of previous tape mark
    word nextblock;                 //file pos of next tape mark (if reading)
    void * phase;                   //goto label to resume processing
    int itime;                      //deferred interrupt time
    int pcchan;                     //channel of this device on pc
    struct tag_dev *tflink;         //flink to next time delayed device
    struct tag_dev *tblink;         //blink to prev time delayed device
    struct tag_dev *cflink;         //flink to next defered device on this pc channel
    struct tag_dev *cblink;         //blink to previous defered device on this pc channel
    word hio;                       //hio received flag
    word ig0;                       //coc input interrupt group
    word ib0;                       //coc input interrupt level bit
    byte verbose;                   //nonzero to enable DV_CHK messages for this device
    byte busy;                      //1 bit =>busy, 2 bit =>interrupt pending
    byte written;                   //flag that a write has occurred
    byte name[6];                   //Sigma Name of this device, e.g. CRA03

} devdata;

typedef struct tag_ctlr {
    struct tag_miop *parent;        //pointer to iop
    struct tag_ctlr *flink;         //link to next controller on this IOP
    devdata *child;                 //link to (first) device on this controller
    devdata *Pdev[16];              //list of devices on this controller ordered by priority low to high
    uword pbit;                     //this controllers bit to set in iop's ic
    uhword ic;                      //multi devices interrupt calls
    byte address;                   //device address switch settings of this device controller
    byte TSH_status;                //device controller portion of status returned by SIO,TIO,HIO
    byte command;                   //device's current order byte from CDW
    byte multi;
    byte busy;

} DCdata;

typedef struct tag_miop {
    struct tag_miop *flink;                 //link to next iop in chain
    DCdata *child;                  //link to first controller on this iop
    word address;                   //iop's address
    word channels;                  //number of implemented subchannels
    word count[24];                 //remaining byte count
    word cda[24];                   //command doubleword address
    word byteaddr[24];              //byte address for data transfer
    byte flags[24];                 //flags DC,IZC,CC,ICE,HTE,IUE,SIL,SK see 3.3.2.1
    byte IOPstatus[24];             //IOP status byte   see 3.3.1.1
                                    //    IL,TDE,TME,MAE,IOPME,IOPCE,IOPHALT,0
    byte subchannelstatus[24];      //    0,BCF,CCF,0,0,0,0,0 (bus check fault,control check fault)
    byte oi_status[24];             // IOP needs to save this
    byte mdeva[24];                 // save 4 lsb of dev addr (to see if interrupt is from active device)
    uword pbit;                     //this iops priority bit
    uword ic;                       //attached subcontroller's ic state
    word hpic;
    DCdata *PDC[32];                //Prioritized list of attached subcontrollers
} iopdata;


#define DEVNAMES char * devname[]={"TY","LP","CR","CP","ME","DP","DC","MT", \
        "BT","9T","PP","PL"};

#define CHKHOSTBUF(siz)  \
    if (dev->buffer eq NULL) then HOSTgetbuf(dev,siz);   \
    if (dev->size lt siz) then HOSTsetbuf(dev,siz);          \
    if (dev->size lt siz) then begin                         \
        printt("Unable to allocate a buffer for HOST (size =") \
        printd(siz) printt(")\n")                            \
        STATE(notop)  /*also need to back up 4 bytes in file*/     \
        return -1;                                                 \
        endif
 
iopdata * find_iop(word address);
DCdata * find_DC(iopdata *iop, word DCaddr);
devdata * find_dev(DCdata *DC, word devaddr);

byte order_in(devdata *dev,byte oi);
byte order_out(devdata *dev);
byte data_in(devdata *dev,word enddata);
byte data_out(devdata *dev,word enddata);

void (dev_TY)(), (dev_LP)(), (dev_CR)(), (dev_DP)(), (dev_MT)(),
        (dev_BT)(), (dev_ME)(), (dev_DC)();
void (dev_TY_hio)(devdata* dev);
int HOSTdevinit(devdata* dev);

int HOSTdevread(devdata* dev);
int HOSTdevwrite(devdata* dev);
int HOSTgetbuf(devdata *dev,int size);
int HOSTsetbuf(devdata *dev,int size);
int ASPIdevread(devdata* dev);
int ASPIdevwrite(devdata* dev);
int ASPIdevrewind(devdata* dev);

#define TSH (DC->TSH_status or dev->TSH_status)
#define DEV_STATE(state) STS(DC->TSH_status,TSH_##state,TSH_DCMASK)
#define DC_STATE(state) STS(dev->TSH_status,TSH_##state,TSH_DMASK)
#define STATE(state) {DEV_STATE(state) DC_STATE(state)}
#define D_BUSY {dev->busy |=1; STATE(busy)}
#define N_BUSY {dev->busy &=0xfe; STATE(ready)}
#define NDEV_BUSY {dev->busy &=0xfe; DEV_STATE(ready)}
#define SET_INTR \
    iop->ic |=DC->pbit;       \
    DC->ic |=dev->pbit;       \
    ic |= iop->pbit;          \
    SET(intr[0].signal,0x20)  \
    SET(event,iointr_evt)
#define CLR_INTR \
    CLEAR(DC->ic,dev->pbit);             \
    if (!DC->ic) {CLEAR(iop->ic,DC->pbit);}  \
    if (!iop->ic) {CLEAR(ic,iop->pbit);}     \
    if (!ic) {CLEAR(intr[0].signal,0x20)}    \
    SET(event,iointr_evt)    
    
#define DEV_INTERRUPT {dev->busy |=2; dev->TSH_status |=TSH_IP; SET_INTR}
#define NDEV_INTERRUPT {dev->busy &=0xfd; CLEAR(dev->TSH_status,TSH_IP); CLR_INTR} 

#define D_INTERRUPT {dev->busy |=2; DC->TSH_status |=TSH_IP; SET_INTR}
#define N_INTERRUPT {dev->busy &=0xfd; CLEAR(DC->TSH_status,TSH_IP); CLR_INTR}
 
//                interrupt  count done   cmd chain    iop halt
enum t_orders {TO_INT= 0x80, TO_CD=0x40, TO_CC=0x20, TO_IOPH=0x10};
//       transmission err   incorrect len  chain modifier  chan end  unusual end
enum ord_in {OI_TE = 0x80, OI_IL=0x40,     OI_CM=0x20, OI_CE=0x10, OI_UE=8};

enum tsh_bits {TSH_IP=0x80, TSH_AUTO=0x10, TSH_UE=8,
        TSH_DMASK=0x60, TSH_DCMASK=0x06, TSH_busy=0x66, TSH_ready=0x00,
        TSH_notop=0x22, TSH_unav=0x44};
#define DV_CHK(x) {if (verbose and VERB_LEVEL && dev->verbose) then begin \
        showscreen(V_USER); x; endif}











