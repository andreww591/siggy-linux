#include <errno.h>
#include "idlop.h"
#include <stdio.h>
void perr()
{
switch (errno) {
   case EACCES: printt("Search permission is denied for one of the directories in the path"); break;
   case EBADF: printt("filedes is bad. "); break;
   case EFAULT: printt("Bad address. "); break;
   case ELOOP: printt("Too many symbolic links encountered while traversing the path."); break;
   case ENAMETOOLONG: printt("File name too long."); break;
   case ENOENT : printt("A component of the path does not exist, or the path is an empty string."); break;
   case ENOMEM : printt("Out of memory (i.e. kernel memory)."); break;
   case ENOTDIR : printt("A component of the path is not a directory."); break;
   default: output(errno); break;
   endswitch
}

