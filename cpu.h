#define S6CC s6.cc
#define LAA iirem=0;   //look ahead abort
#define ALABEL(x) {asm volatile( #x ":" ::: "cc");}
#ifdef TABLE
#define INST(x) } i##x: NOP   { {asm volatile("    inst7_" #x ":" : : :"cc" );}
#define INST5(x) } i5##x: NOP { {asm volatile("    inst5_" #x ":" : :"i" (x/16), "i" (x%16)  :"cc" );}
#define INST9(x) } i9##x: NOP { {asm volatile("    inst9_" #x ":" : :"i" (x/16), "i" (x%16)  :"cc" );}
#define BREAK NOP goto bvect
//#define BREAK NEXTI
#else
#define INST(x) case x:  {asm volatile("  // instruction %0,%1 " : :"i" (x/16), "i" (x%16)  :"cc" );}
#define BREAK break
#endif

#define NODDR if (inst and 0x00100000) TRAP4DCC4;

#define PARITY(x) ({byte _r;word _x=(x); asm("movb $0,%0;\
 orl %1,%1 ; jpe 0f; xorb $8,%0;\
 0: roll $8,%1;orl %1,%1; jpe 1f; xorb $8,%0;\
 1: roll $8,%1;orl %1,%1; jpe 2f; xorb $8,%0;\
 2: roll $8,%1;orl %1,%1; jpe 3f; xorb $8,%0;\
 3: roll $8,%1 " : "=&r" (_r) : "r" (_x) : "cc" ); _r;})
#undef PARITY
#define PARITY(x) ({ uword __x=(x); uhword __y; byte __z; __y=__x^(__x>>16); \
        __z=__y^(__y>>8); __z^=__z>>4; __z^=__z<<2; __z^=__z<<1; __z and 8;})

//#define SCS(x,count)  asm("andb $31,%%cl ; roll %%cl,%0" : "=mabd"(x) : "mabd0" (x),"c" (count) : "cc","%ecx" );
#define SCS(x,count)  asm("roll %%cl,%0" : "=mabd"(x) : "0" (x),"c" (count) : "cc" );
//#define SCS(x,count) {word _c=count and 31; x=(x<<_c) or (x>>(32-_c));}
#define NZMO(x) ({typeof (x) _x=(x); (_x ne 0) && (_x ne -1);})
#define ZMO(x) ({typeof (x) _x=(x); (_x eq 0) || (_x eq -1);})
#define NEXTI goto *nexti[event];
// Shift condition codes
//#define CCsls(x)

// Convert intel zero,sign flags to sigma CC3,CC4
#define CCizp(x) ((x and 0x40) ? 0 : ((x and 0x80) ? 1:2))
//#define CCizp(x) (((x>>7) and 1) or (((x>>5) eor (x>>6) eor 2) and 2))
// Convert intel zero flag to sigma CC3,CC4 for bytes (which are unsigned)
#define CCizpb(x) ((not(x) and 0x40)>>5)

//Capture intel flags - this is a real work saver for overflow and carry!!!
#define CCi ({word x;asm("pushf ; popl %0" : "=r" (x): ); x;})
#define CCia ({byte x;asm("lahf ; movb %%ah,%0" : "=g" (x) : : "%eax"); x;})
//Sets sigma CCs for arithmetic ops (by stealing intel flags)
#define CC1234 {word x=CCi;  \
        S6CC=((x and 1)<<3) or ((x and 0x800)>>9) or CCizp(x);  \
        if ((S6CC>>2) and s6.am) TRAP43;}
#define CC1234A(typ,op1,op2)  {typ scnt = (8*sizeof(typ))-1;  \
  typ mask = (1<<scnt)-1; \
  typ * pop1 = op1;       \
  typ lop1 = * pop1;      \
  typ res = lop1 + op2;   \
  typ cin = (((lop1 and mask) + (op2 and mask))>>scnt) and 1;  \
  typ s1 = (lop1>>scnt) and 1;   \
  typ s2 = (op2>>scnt) and 1;    \
  typ cot = (s1 + s2 + cin)>>1;  \
  *pop1 = res;                   \
  if (cot) S6CC=8; else S6CC=0;  \
  if (res lt 0) S6CC|=1;         \
    else if (res gt 0) S6CC|=2;  \
  if(cot ne cin) S6CC|=4;        \
  if ((S6CC>>2) and s6.am) TRAP43; \
  }
#define CC12d {word x=CCi;  \
        S6CC=((x and 1)<<3) or ((x and 0x800)>>9);}
// Subtract wants opposite cc1
#define CC12Sd {word x=CCi;  \
        S6CC=((not(x) and 1)<<3) or ((x and 0x800)>>9);}
// Subtract wants opposite cc1
#define CC1234S {word x=CCi;  \
        S6CC=((not(x) and 1)<<3) or ((x and 0x800)>>9) or CCizp(x); \
        if ((S6CC>>2) and s6.am) TRAP43;}

//Sets sigma CCs for byte arithmetic ops
#define CC1234b {word x=CCi; S6CC=((x and 1)<<3) or CCizpb(x);}

//Sets sigma CC3,CC4 for word (or hword) loads, the hard way
//since intel's mov instructions don't set flags
#define CCd34(x,typ){typ _temp=(x); (_temp eq 0) ? (S6CC &= 0xC): \
                ((_temp gt 0) ? (S6CC = ((S6CC and 0xC) or 2)):   \
                (S6CC = ((S6CC and 0xC) or 1)));}
//#define CCt34(x,typ) { asm(" cmp $0,%0;" : : "g" (x) : "cc"); CC34;}
#define CCt34(x,typ) CCd34(x,typ)
//same as CCt34 except for byte loads
#define CCt34b(x) {byte _temp=(x) ; (_temp eq 0) ? (S6CC &= 0xC) : \
                (S6CC=(S6CC and 0xC) or 2);}
#define CCt1234b(x) {S6CC=((x) eq 0) ? 0 : 2;}

//sets sigma CC3,CC4 from intel flags
#define CC34 {byte x=CCia; S6CC=(S6CC and 0xC) or CCizp(x);}

//intel compare bombs on subtract overflow, gcc compare is ok. sigma CCs
#define CMP(w,y) ({word x;word _xx=(w);word _yy=(y); \
/*        asm("cmpl %2,%1 ; pushf ; popl %0" : "=r" (x): "r" (_xx), "g" (_yy));*/  \
          S6CC =(S6CC and 8)                                   \
        /*      or (CCizp(x))           */                       \
                or (_xx lt _yy) or ((_xx gt _yy)<<1)             \
                or (((_xx and _yy) ne 0)<<2);  })
//note no "and" cc2 for CD
#define CMPD(w,y) ({ dword _xx=(w); dword _yy=(y); \
        S6CC = (S6CC and 0xc) or (_xx lt _yy) or ((_xx gt _yy)<<1); })

#define SHOWPSD(s6) {psd tpsd; GPSD(tpsd,s6) printt("PSD = ") printx(tpsd.w.w0) printx(tpsd.w.w1) NEWLINE}
#define SHOWREGS(s6) SHOWPSD(s6) dumpr(s6.rp,s6.regs);


#define field(x,name)  (((ufld)(x)).i.name)
#define ifield(x,name) (((ufld)(x)).im.name)
#define mfield(x,name) (((ufld)(x)).mod.name)
#define inmembyte 0x7ffc0
#define inmemhword 0x3ffe0
#define inmemword 0x1fff0
#define inmemdword 0xfff8
//#define INMEM(x,type) (x & inmem##type)
#define INMEM(x,typ) ((x) & -64/sizeof(typ))
#define RvsM(inmem) (inmem ? mem : reg)

//#define PAGE(x,typ)  ((x*sizeof(typ))/2048)
//#define MAPPED(x,typ) ((((s6.memmap[_vpg])*4)/sizeof(typ)) )
#define MAPPEDbyte (s6.memmap[_vpg]<<2)
#define MAPPEDhword (s6.memmap[_vpg]<<1)
#define MAPPEDword (s6.memmap[_vpg])
#define MAPPEDdword (s6.memmap[_vpg]>>1)
//the following works as an lvalue:
#define RXMEMU(x,typ)  ({int tt=(x)&MASKV(typ);word inmem=INMEM(tt,typ);&SLOC(RvsM(inmem),tt,typ);})
//   RXMEM is a subscript expression of the requested type (so its valid as an lvalue)
#define bytesh 11
#define hwordsh 10
#define wordsh 9
#define dwordsh 8
//#define TRAP40CC2 TRAP40CC4

#define MASKX(typ) ((0x40000*4/sizeof(typ))-1)  //was 400000
#define MASKV(typ) ((0x20000*4/sizeof(typ))-1)
#define M16vx (s6.vm ? MASKV(dword) : MASKX(dword))
#define M17vx (s6.vm ? MASKV(word) : MASKX(word))
#define M18vx (s6.vm ? MASKV(hword) : MASKX(hword))
#define M19vx (s6.vm ? MASKV(byte) : MASKX(byte))


// vRxMEM gets the value, *RxMEM.  Whereas *RxMEM works, vRxMEM is slightly more optimized
#define vRxMEMMS(x,typ,a,mm,ms,ma) ({ word _inmem,__x,_vpg;byte _wl; typ _p; \
                   __x=(x)&MASKX(typ);          \
                 if (mm || !ma) __x &= MASKV(typ);       \
                 _inmem=INMEM(__x,typ);                    \
                 if (!_inmem) _p=(SLOC(reg,__x,typ)); else {  \
                     if (mm) { _vpg=__x>>typ##sh;       \
                         if ((ms || ma) && s6.memac[_vpg] gt a) TRAP40CC4; \
                         __x^=MAPPED##typ;}                \
                     if (!a && s6.wk && (_wl=s6.wl[__x>>typ##sh]) && (s6.wk^_wl)) TRAP40CC4;\
                     if (__x ge sz##typ) TRAP40CC2;        \
                     _p=(SLOC(mem,__x,typ));}   _p;})
//this is the general "get pointer to (sigma) register /crossover/ memory"
#define RxMEMMS(x,typ,a,mm,ms,ma) ({ word _inmem,__x,_vpg;byte _wl; typ *_p; \
                   __x=(x)&MASKX(typ);       \
                 if (mm || !ma) __x &= MASKV(typ);    \
                 _inmem=INMEM(__x,typ);                    \
                 if (!_inmem) _p=&(SLOC(reg,__x,typ)); else {  \
                     if (mm) { _vpg=__x>>typ##sh;       \
                         if ((ms || ma) && s6.memac[_vpg] gt a) TRAP40CC4; \
                         __x^=MAPPED##typ;}                \
                     if (!a && s6.wk && (_wl=s6.wl[__x>>typ##sh]) && (s6.wk^_wl)) TRAP40CC4;\
                     if(__x ge sz##typ) TRAP40CC2;         \
                     _p=&(SLOC(mem,__x,typ));}   _p;})

#define RxMEM(x,typ,a) RxMEMMS(x,typ,a,s6.mm,s6.ms,s6.ma)
#define vRxMEM(x,typ,a) vRxMEMMS(x,typ,a,s6.mm,s6.ms,s6.ma)

//special version of RXMEMM for IEAX##t##M  (for counter 4 interrupt) mapped with no access checking
#define RXMEMM(x,typ) ({ word _inmem,__x,_vpg; \
                __x=(x)&MASKX(typ);     \
                if (s6.mm or s6.ms) __x&=MASKV(typ); \
                _inmem=INMEM(__x,typ); \
                if(_inmem && s6.mm) {_vpg=__x>>typ##sh; __x^=MAPPED##typ;} \
                if (__x ge sz##typ) BREAK; \
                &(SLOC(RvsM(_inmem),__x,typ));})
// pSLOCM was added for LAS instruction, mapped, access protection and
//   write locks, but without crossover to register
#define pSLOCM(x,typ,a) ({ word _inmem,__x,_vpg,_ppg,_wl;   \
                 __x=(x)&MASKX(typ);                       \
                 if (s6.mm || !s6.ma) __x &= MASKV(typ);    \
                 _vpg=__x>>typ##sh;                        \
                 if (s6.mm) { __x^= MAPPED##typ;           \
                     _ppg=__x>>typ##sh;                    \
                     if ((s6.ms||s6.ma) && s6.memac[_vpg] gt a) TRAP40CC4;}   \
                     else _ppg=_vpg;   \
                 if (!a && s6.wk && (_wl=s6.wl[_ppg]) && (s6.wk^_wl)) TRAP40CC4;\
                 if (__x ge sz##typ) TRAP40CC2; \
                 &(SLOC(mem,__x,typ));})

//sigma instructions should use:
//                 pIEAXt(a) (t=w,h,d,b).(a=0,2) to get a C pointer
//       to the sigma Indirect Effective Address with post indeXing of the
//       current instruction. Other instruction (e.g. byte string) should use:
//                 pRxMEM0(z1,z2) if they are going to store into the location
//                 pRxMEM2(z1,z2) if they are only going to read the location
//       z1 is sigma address of register/memory location desired
//       z2 is type (word,hword,dword,byte)
// uniform coding of sigma instructions facilitates changing the low level implementaion

//---------------------------Low Level Implementation Options
#ifdef RXINLINE
#define pRxMEM0(z1,z2) RxMEM(z1,z2,0)
#define pRxMEM1(z1,z2) RxMEM(z1,z2,1)
#define pRxMEM2(z1,z2) RxMEM(z1,z2,2)
#define pRxMEM3(z1,z2) RxMEM(z1,z2,3)
#define vRxMEM0(z1,z2) vRxMEM(z1,z2,0)
#define vRxMEM1(z1,z2) vRxMEM(z1,z2,1)
#define vRxMEM2(z1,z2) vRxMEM(z1,z2,2)
#define vRxMEM3(z1,z2) vRxMEM(z1,z2,3)
#else
//#define CHKT if ((word)t le 0) {if ((word) t lt 0) goto trap40cc2; else goto trap40cc4;}
#define CHKT if (!(word)t) goto trapint;
#define pRxMEM0(z1,z2) ({z2 *t=rxmem##z2##0(z1);CHKT; t;})
#define pRxMEM1(z1,z2) ({z2 *t=rxmem##z2##1(z1);CHKT; t;})
#define pRxMEM2(z1,z2) ({z2 *t=rxmem##z2##2(z1);CHKT; t;})
#define pRxMEM3(z1,z2) ({z2 *t=rxmem##z2##3(z1);CHKT; t;})
//yes, i know- need defs for vRxMEM##a stuff here if rx not inline but will i ever use it again?
#endif

#ifdef IEAINLINE
#define pIEAXw(a) (pRxMEM##a(IEAX(inst,word,2),word))
#define pIEAXh(a) (pRxMEM##a(IEAX(inst,hword,2),hword))
#define pIEAXb(a) (pRxMEM##a(IEAX(inst,byte,2),byte))
#define pIEAXd(a) (pRxMEM##a(IEAX(inst,dword,2),dword))
#else
#ifdef IEASTDFUNC
#define CHKT if (!(word)t) goto trapint;
#define pIEAXw(a) ({word * t=pieaxw##a(inst);CHKT; t;})
#define pIEAXh(a) ({hword *t=pieaxh##a(inst);CHKT; t;})
#define pIEAXb(a) ({ byte *t=pieaxb##a(inst);CHKT; t;})
#define pIEAXd(a) ({dword *t=pieaxd##a(inst);CHKT; t;})
#else
#define pIEAXw(a) ({CALL(&&pieaxw##a,word *);})
#define pIEAXh(a) ({CALL(&&pieaxh##a,hword *);})
#define pIEAXb(a) ({CALL(&&pieaxb##a,byte *);})
#define pIEAXd(a) ({CALL(&&pieaxd##a,dword *);})
#endif
#endif
//-----------end Low level Implementation Options
//------For some reason, indirecta produces better code, yet
//      can make speed worse (maybe jump bounds, different register alloc?)
//      So both are defined, and you can use whichever is faster 
//      at the moment (until something else moves around)
#define INDIRECT(x) (field(x,indirect))
#define INDIRECTA(x) (x and 0x80000000)
//---------------------------------------------------------------

#define Rfld(x) field(x,r)
#define EA(x) field(x,ea)
#define IEA(x,a) ({int _t=EA(x); (INDIRECTA(x)) ? (vRxMEM##a(_t,word))  : _t;})
#define IEAs(x,a,mm,ms,ma) ({int _t=EA(x); (INDIRECTA(x)) ? (vRxMEMMS(_t,word,a,mm,ms,ma))  : _t;})
#define IEAU(x) ({int _t=EA(x); (INDIRECTA(x)) ? (*RXMEMU(_t,word)) : _t;})
#define IEAM(x) ({int _t=EA(x); (INDIRECTA(x)) ? (*RXMEMM(_t,word)) : _t;})
//  IEAX returns sigma addresses of the requested resolution - invalid as an lvalue
#define IEAX(x,typ,a) ({int _t=field(x,rx);int _iea=IEA(x,a)*4/sizeof(typ); _t ? (_iea+SLOC(reg,_t,word)) : _iea;})
#define IEAXU(x,typ) ({int _t=field(x,rx);int _iea=IEAU(x)*4/sizeof(typ); _t ? (_iea+SLOC(reg,_t,word)) : _iea;})
#define IEAXM(x,typ) ({int _t=field(x,rx);int _iea=IEAM(x)*4/sizeof(typ); _t ? (_iea+SLOC(reg,_t,word)) : _iea;})
#define IEAXs(x,typ,a,mm,ms,ma) ({int _t=field(x,rx);int _iea=IEAs(x,a,mm,ms,ma)*4/sizeof(typ); _t ? (_iea+SLOC(reg,_t,word)) : _iea;})
// the U and M versions are for interrupt instructions
// or maybe pcp where you want to override map in the psd (U for force 
// Unmapped) or M uses the map from psd but bypasses master/slave 
// write locks and access protection
#define IEAXwU RXMEMU(IEAXU(inst,word),word)
#define IEAXhU RXMEMU(IEAXU(inst,hword),hword)
#define IEAXbU RXMEMU(IEAXU(inst,byte),byte)
#define IEAXdU RXMEMU(IEAXU(inst,dword),dword)
#define IEAXwM RXMEMM(IEAXM(inst,word),word)
#define IEAXhM RXMEMM(IEAXM(inst,hword),hword)
#define IEAXbM RXMEMM(IEAXM(inst,byte),byte)
#define IEAXdM RXMEMM(IEAXM(inst,dword),dword)

#define REG(inst) field(inst,r)
#define IMMED(inst)  (((ufld)(inst)).im.immed)
#define OPCODE(inst) ((inst>>24) and 0x7f)  //field(inst,opcode)

#define REGWA(x) (SLOC(reg,x,word))

#define EVA IEAX(inst,word,2)

#define R SLOC(reg,field(inst,r),word)
#define Ru1P (&Ru1)
#define Ru1 SLOC(reg,field(inst,r)|1,word) 
#define Rd SLOC(reg,field(inst,r)>>1,dword)
#define TWICE(x) ({udw _d;_d.w.w0=_d.w.w1=(x);_d.dw;})
// RRu1 is not an lvalue
#define RRu1  ((field(inst,r) & 1) ? TWICE(R) : Rd)
#define LD(rg,ad) {word _r=rg; dword _dtemp=(ad); \
        if (_r & 1) then {SLOC(reg,_r,word)=_dtemp>>32;} else \
             {SLOC(reg,_r>>1,dword)=_dtemp;};_dtemp;}
#define LDl(rg,ad) {word _r=rg; dword _dtemp=(ad); \
        if (_r & 1) then {SLOC(reg,_r,word)=_dtemp;} else \
             {SLOC(reg,_r>>1,dword)=_dtemp;};_dtemp;}
//#define STD(rg,ad) {word _r=rg; dword _dtemp;  \
//                if (_r & 1) then _dtemp=TWICE(R); else _dtemp=Rd; (ad)=_dtemp;}

typedef union tag_dec { unsigned long long d[2]; word w[4]; byte b[16];} dec;

typedef union tag_ufld { int w; unsigned int uw;
             struct { unsigned ea:17; unsigned rx:3; unsigned r:4;
                     unsigned opcode:7; unsigned indirect:1;} i;
             struct { signed immed:20; signed r:4;    //signed r for MTW, etc
                     unsigned opcode:7; unsigned ind:1;} im;
            }ufld;
#define TRAP(loc) {s6.curloc=0x##loc;goto trapint;}
#define TRAPCC(loc,cc) {s6.curloc=0x##loc; s6.t40cc=cc; goto trapint;}
#define TRAP40CC4 TRAPCC(40,1) //goto trap40cc4;
#define TRAP40CC3 TRAPCC(40,2)
#define TRAP40CC2 TRAPCC(40,4) //goto trap40cc2;
#define TRAP40CC1 TRAPCC(40,8) //goto trap40cc1;
#define TRAP40CC0 TRAPCC(40,0) //goto trap40cc0;
#define TRAP40CCA TRAPCC(40,8+2*s6.ms) //goto trap40cca;
#define TRAP41 TRAP(41) //goto trap41;
#define TRAP42 TRAP(42) //goto trap42;
#define TRAP43 TRAP(43)
#define TRAP44 TRAP(44) //goto trap44;
#define TRAP45 TRAP(45) //goto trap45;
#define TRAP4DCC4 TRAPCC(4d,1) //goto trap4dcc4;
#define TRAP4DCC3 TRAPCC(4d,2) //goto trap4dcc3;
#define TRAP4DCC0 TRAPCC(4d,0) //goto trap4dcc0;
#define PRIV if (s6.ms) then TRAP40CC3;
//#define BRANCHTR   // null it for optimization---branch trace table
#define BRANCHTR {br_stack[0xf & ++br_count]=(inst&0xff000000) | (s6.ia-1);}
#define NOIND if (inst and 0x80000000) then {TRAP40CC1};

typedef struct tag_sigma6 {
    word regs[512];         //  32 register blocks of 16 words each
    word memmap[256];       //  mem map in eor form
    byte memac[256];        //  virtual mem access protection bits
    byte wl[8192];          //  physical core page write locks
    byte curloc;            //  trap location
    byte t40cc;             //  trap condition code
    byte admode;            //  addressing mode
    byte vm;                //  mm || !ma use virtual (nonextended) masks
    byte cc;                //  condition code
    byte fs;                //  float significance mode control
    byte fz;                //  float zero mode control
    byte fn;                //  float normalize mode control
    byte ms;                //  master/slave
    byte mm;                //  memory map control
    byte dm;                //  decimal mask
    byte am;                //  arithmetic mask
    byte asc;               //  sigma 9 a(n)scii bit
    word ia;                //  instruction address
    byte wk;                //  write key
    byte ci;                //  counter interrupt group inhibit
    byte ii;                //  input/output interrupt group inhibit
    byte ei;                //  external interrupt group inhibit
    byte ma;                //  mode altered *** BIG 6 ***
    byte rp;                //  register pointer
} sigma6;

typedef struct tag_spsd {
    word w1;
    word w0;
} spsd;

typedef union tag_psd {
    dword d; 
    struct tag_spsd w;
    unsigned int wd[2];
    struct {               //see tag_sigma6 for descriptions
        unsigned :4;
        unsigned rp:5;
        unsigned :14;
        unsigned ma:1;     //mode altered *** BIG 6 ***
        unsigned ei:1;
        unsigned ii:1;
        unsigned ci:1;
        unsigned :1;
        unsigned wk:2;
        unsigned :2;
        unsigned ia:17;
        unsigned :2;
        unsigned asc:1;
        unsigned am:1;
        unsigned dm:1;
        unsigned mm:1;
        unsigned ms:1;
        unsigned fn:1;
        unsigned fz:1;
        unsigned fs:1;
        unsigned :1;
        unsigned cc:4;
     } n;
} psd;

typedef struct tag_s6pcp {
    struct {       
            unsigned select_address;//:17;
            unsigned audio;//:1;
            unsigned watchdog;//:1;
            unsigned interleave;//:1;
            unsigned pe_mode;//:1;
            unsigned clockmode;//:1;
            unsigned register_display;//:1;
            unsigned register_select;//:4;
            unsigned address_stop;//:1;
            unsigned sense;//:4;
        } sw;
    word run_wait;
    word nopcp;          //dont display pcp if this is set
    word alarm;
    word data;           //they are really 3 state
    word data_mask;      //so this distinguishes the cases
    hword unit_address;  //boot address
    byte idle;    //missing Insert, Instr Addr, Store,
                     // Display, Data(store-enter)     but these are actions,
                     // not really "states"   (ok, Instr Addr hold is a state-
                     // one that will kill the CPU)
    } s6pcp;


//   Gather the PSD from the struct
#define GPSD(s6psd,s6) s6psd.w.w0=(((((((((s6.cc << 2 or s6.fs) << 1 or s6.fz) << 1  \
    or s6.fn) << 1 or s6.ms) << 1 or s6.mm) << 1 or s6.dm) \
    << 1 or s6.am) <<1 or (s6.asc and ascm)) << 19 or s6.ia);   \
    s6psd.w.w1=(((((s6.wk << 2 or s6.ci) << 1 or s6.ii) << 1 or s6.ei) \
    << 1 or s6.ma ) << 19 or s6.rp) << 4;

#define GPSDP(s6psd,s6) s6psd.w.w0=(((((((((s6->cc << 2 or s6->fs) << 1 or s6->fz) << 1  \
    or s6->fn) << 1 or s6->ms) << 1 or s6->mm) << 1 or s6->dm) \
    << 1 or s6->am) <<1 or (s6->asc and ascm)) << 19 or s6->ia);   \
    s6psd.w.w1=(((((s6->wk << 2 or s6->ci) << 1 or s6->ii) << 1 or s6->ei) \
    << 1 or s6->ma ) << 19 or s6->rp) << 4;


//   Break apart the PSD into the struct
#define SETRP(s6,val) s6.rp=(val) and 0x1f; greg=reg=((byte *)(s6.regs))+64*s6.rp+64;
#define SETRP9(s6,val) if ((val and 0xf) gt 3) TRAP4DCC0; s6.rp=(val) and 0x1f; greg=reg=((byte *)(s6.regs))+64*s6.rp+64;
#define BPSD(s6,s6psd)  \
     s6.ia=s6psd.w.w0 and 0x1ffff; \
     s6.asc=(s6psd.w.w0 >> 19) and ascm; \
     s6.am=s6psd.w.w0 >> 20 and 1; \
     s6.dm=s6psd.w.w0 >> 21 and 1; \
     s6.mm=s6psd.w.w0 >> 22 and 1; \
     s6.ms=s6psd.w.w0 >> 23 and 1; \
     s6.fn=s6psd.w.w0 >> 24 and 1; \
     s6.fz=s6psd.w.w0 >> 25 and 1; \
     s6.fs=s6psd.w.w0 >> 26 and 1; \
     s6.cc=s6psd.w.w0 >> 28 and 0xf; \
     SETRP(s6,s6psd.w.w1>>4)       \
     s6.ma=s6psd.w.w1 >> 23 and 1; \
     s6.ei=s6psd.w.w1 >> 24 and 1; \
     s6.ii=s6psd.w.w1 >> 25 and 1; \
     s6.ci=s6psd.w.w1 >> 26 and 1; \
     s6.wk=s6psd.w.w1 >> 28 and 3; \
     s6.vm=s6.mm or (s6.ma eor 1); \
     s6.admode=(((s6.mm << 1) or s6.ms) << 1) or s6.ma;

#define MEMCLEAR  {word i; word *_pwi=(word *)mem; for (i=0;i<sz;i++) {*(--_pwi)=0;};}
#define MAP(x) x    //((s6.mm) ? map(x) : x)

//      These Macros evaluate to pointers to Sigma's General Registers

#define LOAD(boot,s6)  {int i=0x20; \
    MEMW(i++)=0; MEMW(i++)=0; MEMW(i++)=0x020000A8;  \
    MEMW(i++)=0x0E000058; MEMW(i++)=0x11; MEMW(i++)=boot and 0xfff; \
    MEMW(i++)=0x32000024; MEMW(i++)=0xCC000025; MEMW(i++)=0xCD000025; \
    MEMW(i++)=0x69C00028; s6.ia=0x26;}

#define IORESET  {io_reset();}
#define CPURESET(s6,s6psd) s6psd.w.w0=0x26; s6psd.w.w1=0; BPSD(s6,s6psd) \
        {int i; for (i=0;i<16;i++) {intr[i].armed=intr[i].triggered= \
        intr[i].enabled=intr[i].active=intr[i].waiting=intr[i].ewaiting= \
        intr[i].signal=0;}} active.w=waiting.w=ewaiting.w=ic=0;

byte coc_rd(word iea);
byte coc_wd(word iea,word d);

#define RP1 __attribute__ ((regparm(1)))
#ifndef RXINLINE
#define RX(typ,n) typ *rxmem##typ##n(word add) __attribute__ ((regparm(1)));
#include "rx.h"
#undef RX
#define RX(typ,n) typ * rxmem##typ##n(word add) \
{ return RxMEM(add,typ,0);       \
  trap40cc2: return -1;          \
  trap40cc4: return 0;  }
#endif
#ifndef IEAINLINE
#ifdef IEASTDFUNC
#define IE(typ,t,n) typ *pieax##t##n(word inst) RP1;
#include "ie.h"
#undef IE
#define IE(typ,t,n) typ RP1 *pieax##t##n(word instr)     \
{    ACCESSES_MEMORY ACCESSES_REGISTERS              \
 switch(s6.admode)  {            \
     case 7:  return RxMEMMS(IEAXs(instr,typ,2,1,1,1),typ,n,1,1,1); \
     case 6:  return RxMEMMS(IEAXs(instr,typ,2,1,1,0),typ,n,1,1,0); \
     case 5:  return RxMEMMS(IEAXs(instr,typ,2,1,0,1),typ,n,1,0,1); \
     case 4:  return RxMEMMS(IEAXs(instr,typ,2,1,0,0),typ,n,1,0,0); \
     case 3:  return RxMEMMS(IEAXs(instr,typ,2,0,1,1),typ,n,0,1,1); \
     case 2:  return RxMEMMS(IEAXs(instr,typ,2,0,1,0),typ,n,0,1,0); \
     case 1:  return RxMEMMS(IEAXs(instr,typ,2,0,0,1),typ,n,0,0,1); \
     case 0:  return RxMEMMS(IEAXs(instr,typ,2,0,0,0),typ,n,0,0,0); \
    endswitch                                          \
 trap40cc2: return (typ *) -1;                       \
 trapint:         \
 trap40cc4: return 0; }
#else
#define IE(typ,t,n) pieax##t##n: FUNCTION(x) RETURN(RxMEM(IEAX(inst,typ,2),typ,n))
#endif
#endif
