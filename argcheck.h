#if 0
The following is an example program that calls argcheck
Some notes:
	String type variables must be char *var (character pointers)
	The variables need to be defined before the argtab structure is
	involked.

#include "argcheck.h"

int vflag,cflag,iflag,hflag;
char *sflag="initial";

argtab table[]={
	{'v',&vflag,Switch,"Switch -verbose flag"},
	{'c',&cflag,Caseswitch,"Case sensitive switch - uppercase is off"},
	{'i',&iflag,Int,"Int - the next arg is an integer"},
	{'h',&hflag,Hex,"Hex - the next arg is hexidecimal"},
	{'s',&sflag,String,"String - the next arg is a string"},
	{'f',0,File,"File - read the next arg as a file to get options from"},
	{0,0,Last}};

main(int argc,char *argv[]){
	argcheck(argc,argv,table);
	printf("vflag=%d, cflag=%d, iflag=%d, hflag=%X, sflag=%s\n",
		vflag,cflag,iflag,hflag,sflag);
	}
#endif
#include <stdio.h>
#include <ctype.h>
enum argtabt { Switch,Caseswitch,Int,Hex,String,File,Last};
typedef struct {
	char	c;
	void	*v;
	enum argtabt	f;
	char	*desc;
	} argtab;

extern void *malloc();
static
argcheck(int argc,char **argv,argtab *table){
	register int i,c,*ip;
	register char *cp,*ap;
	register argtab *op;
	for(i=1 ; i<argc ; i++){
		if(*argv[i]!='-' && *argv[i]!='/')continue;
		ap=  &argv[i][1];
		c= *ap;
		if(isupper(c))c=tolower(c);
		for(op=table ; op->c!=c && op->f!=Last ; op++);
		ip=(int *)op->v;
		cp=(char *)op->v;
		switch(op->f){
		case Switch:
			*ip = ! * ip;	/* flip it's status */
			break;
		case Caseswitch:
			*ip=(isupper(*ap) ? 0 : 1);
			break;
		case Int:
			while(*++ap && isspace(*ap));
			if(*ap==0){
				ap=argv[++i];
				if(i>=argc)continue;
				}
			*ip = atoi(ap);
			break;
		case Hex:
			while(*++ap && isspace(*ap));
			if(*ap==0){
				ap=argv[++i];
				if(i>=argc)continue;
				}
			*ip=0;
			while(*ap && isxdigit(*ap)){
				c= *ap++;
				if(isupper(c))c=tolower(c);
				*ip<<=4;
				*ip+=c-(isdigit(c)?'0':'a'-10);
				}
			break;
		case String:
			while(*++ap && isspace(*ap));
			if(*ap==0){
				ap=argv[++i];
				if(i>=argc)continue;
				}
			if(argv[0]) *((char **)cp) = ap;
			else {
				*((char **)cp)=malloc(strlen(ap)+1);
				strcpy(*(char **)cp,ap);
				}
			break;
		case File:
			while(*++ap && isspace(*ap));
			if(*ap==0){
				ap=argv[++i];
				if(i>=argc)continue;
				}
			{ char *myargv[40],line[84];
			int myargc=0;
			FILE *fp;
			fp=fopen(ap,"r");
			if(fp<=(FILE *)0)continue;
			while((cp=fgets(line,80,fp))>(char *)0){
				myargv[0]=0;
				myargc=1;
				for(cp=line ; *cp ; cp++){
					while(*cp && isspace(*cp))cp++;
					if(*cp==0){
						myargv[myargc]=0;
						break;
						}
					myargv[myargc++]=cp;
					if(*cp=='"'){
						myargv[myargc-1]++;
						while(*++cp && *cp!='"');
						}
					else while(*cp && !isspace(*cp)) cp++;
					*cp=0;
					}
				argcheck(myargc,myargv,table);
				}
			fclose(fp);
			}
			break;
		case Last:
			continue;
			}
		}
	}
static FILE *
cleanopen(char *name,char *mode,char *errstr){
	register FILE *fp;
	fp=fopen(name,mode);
	if(fp<=(FILE *)0){
		fprintf(stderr,errstr,name);
		exit(-1);
		}
	return(fp);
	}
argusage(char *pname,argtab *table){
	register argtab *op;
	register int i;
	for(op=table ; op->f!=Last ; op++);
	fprintf(stderr,op->desc,pname);
	fprintf(stderr,"\noption\tdescription\n");
	for(op=table ; op->f!=Last ; op++)
		fprintf(stderr,"  %c\t%s\n",op->c,op->desc);
	}
